# Initialize zsh
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share
export ZDOTDIR=$HOME/.config/zsh
export PATH=$PATH:$HOME/.local/bin
export CDLMD_LICENSE_FILE=~/Programs/license.dat

source $ZDOTDIR/zshrc
