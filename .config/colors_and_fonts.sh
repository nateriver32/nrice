xrdb ~/.Xresources

# Base colors
export BG0="$(awk '/^#define base00/ {print $3}' ~/.Xresources)"
export BG1="$(awk '/^#define base01/ {print $3}' ~/.Xresources)"
export BLUE="$(awk '/^#define base0D/ {print $3}' ~/.Xresources)"
export CYAN="$(awk '/^#define base0C/ {print $3}' ~/.Xresources)"
export GRAY="$(awk '/^#define base02/ {print $3}' ~/.Xresources)"
export GREEN="$(awk '/^#define base0B/ {print $3}' ~/.Xresources)"
export MAGENTA="$(awk '/^#define base0F/ {print $3}' ~/.Xresources)"
export ORANGE="$(awk '/^#define base09/ {print $3}' ~/.Xresources)"
export PURPLE="$(awk '/^#define base0E/ {print $3}' ~/.Xresources)"
export RED="$(awk '/^#define base08/ {print $3}' ~/.Xresources)"
export YELLOW="$(awk '/^#define base0A/ {print $3}' ~/.Xresources)"
export WHITE="$(awk '/^#define base07/ {print $3}' ~/.Xresources)"

# Custom colors
export SELECTION="$(awk '/^#define selection/ {print $3}' ~/.Xresources)"
export VISUAL_MODE="$(awk '/^#define visual_mode/ {print $3}' ~/.Xresources)"
export CURSEARCH="$(awk '/^#define cursearch/ {print $3}' ~/.Xresources)"
export WHITE2="$(awk '/^#define white2/ {print $3}' ~/.Xresources)"

# Fonts
export FONT="$(awk '/\*.font/ {print}' ~/.Xresources | sed -E "s/.*: //g")"
export FONT_NAME=${FONT%%:*}
export FONT_STYLE=$(echo ${FONT} | sed -E "s/.*:style=//g" | sed -E "s/:.*//g")
export FONT_SIZE=${FONT##*=}
export BORDER_WIDTH="$(awk '/^! \*\.border_width:/ {print $3}' ~/.Xresources)"
