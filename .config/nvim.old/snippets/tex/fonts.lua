local helpers = require('nate.luasnip-helper-funcs')
local get_visual = helpers.get_visual
local math = helpers.in_mathzone

return{

s({trig = "([^%a ž č ć đ š } {])em", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "emphasis"},
  fmta([[<>\emph{<>}]],
    {
      f( function(_, snip) return snip.captures[1] end ),
      d(1, get_visual),
    }
  )
),

s({trig="([^%a{}])txt", snippetType = "autosnippet", wordTrig = false, regTrig = true, desc = "standard text for math mode"},
  fmta(
    [[\text{<>}]],
    {
      d(1, get_visual),
    }
  )
),

s({trig="mrm", snippetType = "autosnippet", wordTrig = false, regTrig = true, desc = "roman serif for math mode"},
  fmta(
    [[\mathrm{<>}]],
    {
      d(1, get_visual),
    }
  )
),


s({trig="([^%a])href", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr="href{}{} command for url links"},
  fmta(
    [[\href{<>}{<>} <>]],
    {
      i(1, "url"),
      i(2, "display name"),
      i(3) 
    }
  )
),

s({trig = "([^%a])tit", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "italicized text"},
  fmta("<>\\textit{<>} <>",
    {
      f( function(_, snip) return snip.captures[1] end ),
      d(1, get_visual),
      i(2),
    }
  )
),

s({trig = "([^%a])tbf", snippetType = "autosnippet",wordTrig = false, regTrig = true,  dscr = "bold text"},
  fmta("<>\\textbf{<>} <>",
    {
      f( function(_, snip) return snip.captures[1] end ),
      d(1, get_visual),
      i(2),
    }
  )
),

s({trig = "([^%a])tsc", snippetType = "autosnippet", wordTrig = false, regTrig = true,  dscr = "smallcaps text"},
  fmta("<>\\textsc{<>} <>",
    {
      f( function(_, snip) return snip.captures[1] end ),
      d(1, get_visual),
      i(2),
    }
  )
),

s({trig = "([^%a])ttt", snippetType = "autosnippet", wordTrig = false, regTrig = true,  dscr = "typewritter text"},
  fmta("<>\\texttt{<>} <>",
    {
      f( function(_, snip) return snip.captures[1] end ),
      d(1, get_visual),
      i(2),
    }
  )
),

}
