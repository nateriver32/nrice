local helpers = require('nate.luasnip-helper-funcs')
local line_begin = require('luasnip.extras.expand_conditions').line_begin
local get_visual = helpers.get_visual
local tikz = helpers.in_tikz
local math = helpers.in_mathzone
local text = helpers.in_text
local list = helpers.in_itemize
local nlist = helpers.in_enumerate

return{

  s({trig = "init", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "begin script"},
    fmta([[#!/usr/bin/env <>

<>]],
      {
        i(1),
        i(2),
      }
    ),
  { condition = line_begin }
  ),

  s({trig = 'lx', snippetType = 'autosnippet', regTrig = true, wordTrig = false, desc = "parentheses"},
    fmt([[<{}>]],
      {
        d(1, get_visual),
      }
    )
  ),

  s({trig = 'kx', snippetType = 'autosnippet', regTrig = true, wordTrig = false, desc = "parentheses"},
    fmta("(<>)",
      {
        d(1, get_visual),
      }
    )
  ),

  s({trig = 'hx', snippetType = 'autosnippet', regTrig = true, wordTrig = false, desc = "square brackets"},
    fmta("[<>]",
      {
        d(1, get_visual),
      }
    )
  ),

  s({trig = 'jx', snippetType = 'autosnippet', regTrig = true, wordTrig = false, desc = "curly braces"},
    fmta("{<>}",
      {
        d(1, get_visual),
      }
    )
  ),

  s({trig = 'sx', snippetType = 'autosnippet', regTrig = true, wordTrig = false, desc = "single quotes"},
    fmta("'<>'",
      {
        d(1, get_visual),
      }
    )
  ),

  s({trig = 'dx', snippetType = 'autosnippet', regTrig = true, wordTrig = false, desc = "double quotes"},
    fmta([["<>"]],
      {
        d(1, get_visual),
      }
    )
  ),

}
