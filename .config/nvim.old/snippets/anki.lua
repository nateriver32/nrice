local helpers = require('nate.luasnip-helper-funcs')
local get_visual = helpers.get_visual
local tikz = helpers.in_tikz
local math = helpers.in_mathzone
local equation = helpers.in_equation
local text = helpers.in_text
local align = helpers.in_align
local line_begin = require("luasnip.extras.expand_conditions").line_begin
local ls = require("luasnip")

return{

  s({trig = 'exa', snippetType = 'autosnippet', regTrig = true, wordTrig = false},
    fmta([[
[latex]\textbf{EXAMPLES: begin}[/latex]

<>
[latex]\textbf{EXAMPLES: end}[/latex]
    ]],
      {
        i(1)
      }
    ),
    {condition=line_begin}
  ),

  s({trig = 'ččč ', snippetType = 'autosnippet', regTrig = true, wordTrig = false},
  fmta([[^^^ADDED^^^]],
    {
    }
  ),
    {condition = line_begin}
  ),

  s({trig = 'bold', snippetType = 'autosnippet', regTrig = true, wordTrig = false},
    fmta("[latex]\\textbf{<>}[/latex]",
      {
        d(1, get_visual)
      }
    ),
    {condition=line_begin}
  ),

  s({trig = ';;; ', snippetType = 'autosnippet', regTrig = true, wordTrig = false},
  fmta([[^^^ADDED^^^]],
    {
    }
  ),
    {condition = line_begin}
),

  s({trig = 'čč ', snippetType = 'autosnippet', regTrig = true, wordTrig = false},
    fmta([[?]],
      {
      }
    )
  ),

  s({trig = ';; ', snippetType = 'autosnippet', regTrig = true, wordTrig = false},
    fmta([[?]],
      {
      }
    )
  ),

  s({trig = 'gm', snippetType = 'autosnippet', regTrig = true, wordTrig = false},
    fmt([[<img src = "{}" />
{}]],
      {
      f(function(args)
        local clipboard_content = vim.fn.system("xclip -o -selection clipboard")
        clipboard_content = clipboard_content:gsub("\n$", "")
        local snippet_text = string.format('%s', clipboard_content)
        vim.fn.system("w<cr>")
        return snippet_text
        end
        ),
        i(2),
      }
    ),
    {condition=line_begin}
  ),

  s({trig = 'igm', snippetType = 'autosnippet', regTrig = true, wordTrig = false},
    fmt([[<img src = "{}" />
{}]],
      {
      f(function(args)
        local clipboard_content = vim.fn.system("xclip -o -selection clipboard")
        clipboard_content = clipboard_content:gsub("\n$", "")
        local snippet_text = string.format('%s', clipboard_content)
        vim.fn.system("w<cr>")
        return snippet_text
        end
        ),
        i(2),
      }
    ),
    {condition=line_begin}
  ),

  s({trig = 'mg', snippetType = 'autosnippet', regTrig = true, wordTrig = false},
    fmt([[<img src = "{}" />
{}]],
      {
      f(function(args)
        local clipboard_content = vim.fn.system("xclip -o -selection clipboard")
        clipboard_content = clipboard_content:gsub("\n$", "")
        local snippet_text = string.format('%s', clipboard_content)
        vim.fn.system("w<cr>")
        return snippet_text
        end
        ),
        i(2),
      }
    ),
    {condition=line_begin}
  ),

  s({trig = 'img', snippetType = 'autosnippet', regTrig = true, wordTrig = false},
    fmt([[<img src = "{}" />
{}]],
      {
      f(function(args)
        local clipboard_content = vim.fn.system("xclip -o -selection clipboard")
        clipboard_content = clipboard_content:gsub("\n$", "")
        local snippet_text = string.format('%s', clipboard_content)
        vim.fn.system("w<cr>")
        return snippet_text
        end
        ),
        i(2),
      }
    ),
    {condition=line_begin}
  ),

  s({trig = 'latex', snippetType = 'autosnippet', regTrig = true, wordTrig = false},
    fmta("[latex]<>[/latex]",
      {
        d(1, get_visual)
      }
    )
  ),

  s({trig = 'lien', snippetType = 'autosnippet', regTrig = true, wordTrig = false},
    fmta("[$]\\rule{\\textwidth}{1pt}[/$]",
      {
      }
    ),
    {condition = line_begin}
  ),

  s({trig = 'line', snippetType = 'autosnippet', regTrig = true, wordTrig = false},
    fmta("[$]\\rule{\\textwidth}{1pt}[/$]",
      {
      }
    ),
    {condition = line_begin}
  ),

s({trig = 'cl', snippetType = 'autosnippet', regTrig = true, wordTrig = false},
  fmta([[{{c1::<>}}]],
    {
      d(1, get_visual),
    }
  ),
    {condition = line_begin}
),
}
