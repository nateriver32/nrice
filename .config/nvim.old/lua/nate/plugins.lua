local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  vim.notify("that didn't work")
  return
end

-- Have packer use a popup window
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "single" }
    end,
  },
}


-- Install your plugins here
return packer.startup(function(use)

  -- My plugins here
  use "wbthomason/packer.nvim" -- Have packer manage itself 
  use "nvim-lua/popup.nvim" -- An implementation of the Popup API from vim in Neovim
  use "nvim-lua/plenary.nvim" -- Useful lua functions used ny lots of plugins

  -- cmp plugins
  use "hrsh7th/nvim-cmp" -- the completion plugin
  use "hrsh7th/cmp-buffer" -- buffer completions
  use "hrsh7th/cmp-path" -- path completions
  use "hrsh7th/cmp-cmdline" -- cmdline completions
  use "saadparwaiz1/cmp_luasnip" -- snippet completions
  use "hrsh7th/cmp-nvim-lsp" -- lsp completions
  use "hrsh7th/cmp-nvim-lua"
  use 'hrsh7th/cmp-nvim-lsp-signature-help'

  -- snippets
  use "L3MON4D3/LuaSnip"

  -- LSP
  use { "williamboman/mason.nvim", run = ":MasonUpdate" } -- :MasonUpdate updates registry contents
  use "williamboman/mason-lspconfig.nvim"
  use "neovim/nvim-lspconfig" -- enable LSP
  use "jose-elias-alvarez/null-ls.nvim" -- LSP diagnostics and code actions 

  -- Telescope 
  use "nvim-telescope/telescope.nvim"

  -- Treesitter 
  use { "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" }
  use 'JoosepAlviste/nvim-ts-context-commentstring'
  use { "nvim-treesitter/playground" }

  -- latex plugins 
  use 'lervag/vimtex'
  use 'frabjous/knap' -- live preview

  -- for faster commenting
  use 'numToStr/Comment.nvim'

  -- for better status line
  use { 'nvim-lualine/lualine.nvim', requires = { 'nvim-tree/nvim-web-devicons', opt = true } }

  -- for multiple cursors
  use 'mg979/vim-visual-multi'

  -- for rust development
  use 'simrat39/rust-tools.nvim'

  -- for anki deck creation directly inside of neovim
  use "rareitems/anki.nvim"

  -- for switching quickly between multiple buffers
  use { "ThePrimeagen/harpoon", branch = "harpoon2", requires = { {"nvim-lua/plenary.nvim"} }}

  -- for dictionary auto completion
  use {'octaltree/cmp-look'}

  -- show colors in real time
  use 'NvChad/nvim-colorizer.lua'

  -- for lf integration
  use {"akinsho/toggleterm.nvim", tag = '*'}
  use "lmburns/lf.nvim"

  -- for better navigation
  use { 'smoka7/hop.nvim', tag = '*' }

  -- outline to show variables and data types
  use 'hedyhli/outline.nvim'

  -- top bar to see full directory path
  use { "utilyre/barbecue.nvim", tag = "*", requires = { "SmiteshP/nvim-navic", "nvim-tree/nvim-web-devicons" } }

  -- shows code block lines
  use {"lukas-reineke/indent-blankline.nvim", main = "ibl" }

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)
