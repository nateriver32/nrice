require("ts_context_commentstring").setup{}

local status_ok, configs = pcall(require, "nvim-treesitter.configs")
if not status_ok then
  return
end

vim.g.skip_ts_context_commentstring_module = true

configs.setup {
	ensure_installed = {
		"awk",
		"bash",
		"bibtex",
		"c",
		"comment",
		"cpp",
		"css",
		"haskell",
		"html",
		"javascript",
		"jsdoc",
		"jsonc",
		"julia",
		"latex",
		"lua",
		"make",
		"markdown",
		"perl",
		"python",
		"regex",
		"ruby",
		"rust",
		"scss",
		"toml",
		"typescript",
		"yaml",
  },
  sync_install = false,
  ignore_install = { "" }, -- List of parsers to ignore installing
  highlight = {
    enable = true, -- false will disable the whole extension
    disable = { "" }, -- list of language that will be disabled
    additional_vim_regex_highlighting = true,

  },
  indent = { enable = true, disable = { "yaml" } },
  context_commentstring = {
    enable = true,
    enable_autocmd = false,
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = '<c-space>',
      node_incremental = '<c-space>',
    }
  }
}

