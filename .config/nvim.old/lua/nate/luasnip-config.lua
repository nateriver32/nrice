-- shortcuts
local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local d = ls.dynamic_node
local fmt = require("luasnip.extras.fmt").fmt
local rep = require("luasnip.extras").rep
local fmta = require("luasnip.extras.fmt").fmta
local line_begin = require("luasnip.extras.expand_conditions").line_begin

-- tell LuaSnip where to look for snippets
require("luasnip.loaders.from_lua").load({ paths = "~/.config/nvim/snippets/" })

-- basic LuaSnip behaviour
ls.config.set_config({
  -- store the selected data inside a built in LuaSnip variable for later use
  store_selection_keys = "<Tab>",

  -- keep around last snippet local to jump back
	history = true,

  -- update changes as you type
	updateevents = "TextChanged,TextChangedI",

  -- snippets automatically expand upon trigger
	enable_autosnippets = true,

})

-- Key Mapping

vim.keymap.set({ "i", "s" }, "kk", function()
	if ls.jumpable(1) then
		ls.jump(1)
	end
end, { silent = true })

vim.keymap.set({ "i", "s" }, "jj", function()
	if ls.jumpable(-1) then
		ls.jump(-1)
	end
end, { silent = true })

--[[ vim.keymap.set({ "i", "s" }, "ll", function() ]]
--[[ 	if ls.choice_active() then ]]
--[[ 		ls.change_choice(1) ]]
--[[ 	end ]]
--[[ end) ]]

vim.keymap.set({ "i", "s" }, "hh", function()
	if ls.choice_active() then
		ls.change_choice(-1)
	end
end)
