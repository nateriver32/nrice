local blankline = require("ibl")

blankline.setup(
   {
       indent = { char = "▏"},
   }
)
