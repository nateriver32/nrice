-- :help options
--vim.cmd [[
--if has('anki')
  --set dictionary+=/usr/share/dict/words
--endif
--]]
--vim.cmd "set complete+=k"
--[[ vim.opt.virtualedit = "all" ]]
vim.cmd "set ttyfast"
vim.cmd "set lazyredraw"
vim.g.neovide = true
vim.opt.spell = true
vim.opt.spelllang = { "en_us" }
vim.cmd "filetype plugin on"
vim.cmd "set tw=125"
vim.opt.history = 1000
vim.opt.ruler = true
vim.opt.title = true
vim.opt.autowriteall = true
vim.opt.backup = false                          -- creates a backup file
vim.opt.clipboard = "unnamedplus"               -- allows neovim to access the system clipboard
vim.opt.cmdheight = 1                           -- more space in the neovim command line for displaying messages
vim.opt.completeopt = { "menuone", "noselect" } -- mostly just for cmp
vim.opt.conceallevel = 0                        -- so that `` is visible in markdown files
vim.opt.fileencoding = "utf-8"                  -- the encoding written to a file
vim.opt.hlsearch = false                        -- highlight all matches on previous search pattern
vim.opt.ignorecase = true                       -- ignore case in search patterns
vim.opt.mouse = "a"                             -- allow the mouse to be used in neovim
vim.opt.pumheight = 20                          -- pop up menu height                      
vim.opt.showmode = false                        -- we don't need to see things like -- INSERT -- anymore
vim.opt.showtabline = 2                         -- always show tabs
vim.opt.smartcase = true                        -- smart case
vim.opt.smartindent = true                      -- make indenting smarter again
vim.opt.splitbelow = true                       -- force all horizontal splits to go below current window
vim.opt.splitright = true                       -- force all vertical splits to go to the right of current window
vim.opt.swapfile = false                        -- creates a swapfile
vim.opt.termguicolors = true                    -- set term gui colors (most terminals support this)
vim.opt.timeoutlen = 1000                       -- time to wait for a mapped sequence to complete (in milliseconds)
vim.opt.undofile = true                         -- enable persistent undo
vim.opt.updatetime = 300                        -- faster completion (4000ms default)
vim.opt.writebackup = false                     -- if a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited
vim.opt.expandtab = true                        -- convert tabs to spaces
vim.opt.shiftwidth = 2                          -- the number of spaces inserted for each indentation
vim.opt.tabstop = 2                             -- insert 2 spaces for a tab
vim.opt.cursorline = true                       -- highlight the current line
vim.opt.cursorcolumn = false                    -- highlight the current column
vim.opt.number = true                           -- set numbered lines
vim.opt.relativenumber = true                   -- set relative numbered lines
vim.opt.numberwidth = 2                         -- set number column width to 2 {default 4}
vim.opt.signcolumn = "yes:2"                    -- always show the sign column, otherwise it would shift the text each time
vim.opt.wrap = true                             -- display lines as one long line
vim.opt.scrolloff = 10                          -- how many lines before srcolling up/down 
vim.opt.sidescrolloff = 10                      -- same thing but left and right
vim.opt.guifont = "Terminess Nerd Font:h12  "   -- the font used in graphical neovim applications
vim.opt.guicursor = "n-v-c-n-sm:block"          -- sets cursor shape in each mode
vim.opt.shortmess:append "c"
vim.opt.laststatus = 3

vim.cmd "set whichwrap+=<,>,[,],h,l"            -- allows you to run vimstring inside a of a string
-- vim.cmd [[set iskeyword+=-]]                    -- e.g. word-word is treated as one word
vim.cmd "set signcolumn=no"

local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup
local yank_group = augroup('HighlightYank', {})
autocmd('TextYankPost', {
    group = yank_group,
    pattern = '*',
    callback = function()
        vim.highlight.on_yank({
            higroup = 'IncSearch',
            timeout = 40,
        })
    end,
})
vim.cmd "set showtabline=1"
--[[ vim.opt.list = true ]]
--[[ vim.opt.listchars = { tab = '~ ' } ]]
--[[ vim.opt.listchars = { eol = '↵' } ]]
