vim.cmd "let g:tex_flavor='latex'"
vim.cmd "let g:vimtex_view_method='zathura'"
--[[ vim.cmd "let g:vimtex_view_method='zathura'" ]]
vim.cmd "let g:vimtex_quickfix_mode=0"
vim.cmd "let g:tex_conceal='abdmg'"
--[[ vim.opt.conceallevel = 1 ]]
vim.opt.conceallevel = 0
vim.cmd "set encoding=utf-8"
vim.cmd "let g:vimtex_imaps_enabled=0"
vim.cmd "let g:vimtex_indent_enabled   = 0"
vim.cmd "let g:vimtex_complete_enabled = 0"
vim.cmd "let g:vimtex_syntax_enabled   = 0"
vim.cmd "let g:vimtex_imaps_enabled  = 1"
-- vim.cmd "let g:vimtex_fold_enabled   = 1"
-- vim.cmd "let g:vimtex_fold_manual   = 1"

-- keymaps
local opts = { noremap = true, silent = true }
local term_opts = { silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap
local kmap = vim.keymap.set

keymap("n","dsm","<Plug>(vimtex-env-delete-math)", opts)
keymap("n","<leader>wc","<Cmd>w<CR><Cmd>VimtexCountWords<CR>", opts)
keymap("n","<Esc>","zx", opts)
keymap("n","<leader><Esc>","zX", opts)
keymap("n","<leader>tt","<plug>(vimtex-toc-open)", opts)
-- keymap("n","dsm","<Plug>(vimtex-env-delete-math)", opts)
-- keymap("n","dsm","<Plug>(vimtex-env-delete-math)", opts)
-- keymap("n","dsm","<Plug>(vimtex-env-delete-math)", opts)
-- keymap("n","dsm","<Plug>(vimtex-env-delete-math)", opts)
-- keymap("n","dsm","<Plug>(vimtex-env-delete-math)", opts)
-- keymap("n","dsm","<Plug>(vimtex-env-delete-math)", opts)
-- keymap("n","dsm","<Plug>(vimtex-env-delete-math)", opts)
