local opts = { noremap = true, silent = true }

local term_opts = { silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap
local kmap = vim.keymap.set

--Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Normal --
-- Better window navigation
keymap("n", "<leader>r", "<C-w>w", opts)
--[[ keymap("n", "<S-e>", "<C-w>l", opts) ]]

-- better text navigation
keymap("n", "j", "gj", opts)
keymap("n", "k", "gk", opts)

--[[ keymap("n", "<leader>e", ":Lex 30<cr>", opts) -- default vim esplorer ]]
--[[ keymap("n", "<leader>e", ":NvimTreeToggle<cr>", opts) -- default vim esplorer ]]

-- Resize with arrows
keymap("n", "<C-j>", ":resize +2<CR>", opts)
keymap("n", "<C-k>", ":resize -2<CR>", opts)
keymap("n", "<C-h>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-l>", ":vertical resize +2<CR>", opts)

-- Navigate buffers
keymap("n", "<leader>k", ":bnext<CR>", opts)
keymap("n", "<leader>j", ":bprevious<CR>", opts)

-- various options
keymap("n", "n", "nzzzv", opts)
keymap("n", "N", "Nzzzv", opts)
keymap("n", "<leader>y", '"+y', opts)

-- Insert --
-- Press jk fast to enter
-- keymap("i", "jk", "<ESC>", opts)
keymap("i", "<C-d>", "<C-d>zz", opts)
keymap("i", "<C-u>", "<C-u>zz", opts)
keymap("n", "<leader>ww", ":w<CR>", opts)
keymap("n", "<leader>wq", ":wq<CR>", opts)
keymap("n", "<leader>qq", ":q!<CR>", opts)

-- Visual --
-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)
keymap("v", "<leader>y", '"+y', opts)

-- Move text up and down
keymap("v", "<C-j>", ":m .+1<CR>==", opts)
keymap("v", "<C-k>", ":m .-2<CR>==", opts)

-- greatest remap ever
keymap("v", "<leader>p", '"_dP', opts)

-- better movement
keymap("v", "j", "gj", opts)
keymap("v", "k", "gk", opts)

-- Visual Block --
-- Move text up and down
keymap("x", "<S-j>", ":move '>+1<CR>gv-gv", opts)
keymap("x", "<S-k>", ":move '<-2<CR>gv-gv", opts)

-- Terminal --
-- Better terminal navigation
keymap("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
keymap("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
keymap("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
keymap("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)

-- Telescope
keymap("n", "<leader>f", ":Telescope find_files<cr>", opts)
keymap("n", "<leader>g", "<cmd>Telescope live_grep<cr>", opts)
--[[ keymap("n", "<leader>;", "<cmd>Telescope quickfix<cr>", opts) ]]
keymap("n", "<leader>;", "<cmd>copen<cr>", opts)

-- TeX
-- F5 processes the document once, and refreshes the view
kmap({ "n", "v", "i" }, "<F5>", function()
	require("knap").process_once()
end)

-- F6 closes the viewer application, and allows settings to be reset
kmap({ "n", "v", "i" }, "<F6>", function()
	require("knap").close_viewer()
end)

-- F7 toggles the auto-processing on and off
kmap({ "n", "v", "i" }, "<F7>", function()
	require("knap").toggle_autopreviewing()
end)

-- F8 invokes a SyncTeX forward search, or similar, where appropriate
kmap({ "n", "v", "i" }, "<F8>", function()
	require("knap").forward_jump()
end)

-- Create inkscape figures for lectures
-- keymap('n', '<C-f>', [[:silent exec '.!inkscape-figures create "'.getline('.').'" "'.expand('%:p:h').'/figures/"'<CR><CR>:w<CR>]], { noremap = true })
-- keymap('n', '<S-f>', [[:silent exec '!inkscape-figures edit "'.expand('%:p:h').'/figures/" > /dev/null 2>&1 &' <CR><CR>:redraw!<CR>]], { noremap = true })
keymap("i", ";,", "<c-g>u<Esc>[s1z=`]a<c-g>u", term_opts)

-- run code snippets directly in the editor
keymap("n", "<leader>n", "<cmd>SnipRun<cr>", term_opts)
keymap("v", "<leader>n", "<cmd>SnipRun<cr>", term_opts)

-- lf integration
keymap("n", "<leader>e", "<cmd>Lf<CR>", term_opts)

-- jupyter notebook integration
--  keymap("n", "<leader>j", "<cmd>MagmaEvaluateLine<cr>", term_opts)
-- keymap("v", "<leader>j", "<cmd>MagmaEvaluateVisual<cr>", term_opts)

-- Anki
function anki_automate()
	vim.cmd([[/rule{]])
	vim.cmd([[normal k]])
	vim.cmd([[normal A}}]])
	vim.cmd([[?\?]])
	vim.cmd([[normal j]])
	vim.cmd([[normal I{{c1::]])
	vim.cmd([[w!]])
	vim.cmd([[AnkiSend]])
	vim.cmd([[%s/{{c1:://g]])
	vim.cmd([[%s/}}//g]])
	vim.cmd([[normal }]])
end

function auto_count()
	vim.cmd([[normal gg]])
	vim.cmd([[let i=1 | g/\[\$\]/s//\=i..'111[$]'/ | let i+=1]])
	vim.cmd([[normal viwyu{]])
end

function real_count()
	vim.cmd([[let i=1 | g/\[\$\]/s//\=i..'222[$]'/ | let i+=1]])
end

function anki_automate_single()
	vim.cmd([[/rule{]])
	vim.cmd([[normal! V]])
	vim.cmd([[exec '?\?']])
	vim.cmd([[normal! "ad]])
	vim.cmd([[normal! V]])
	vim.cmd([[exec '?Cloze']])
	vim.cmd([[normal! j]])
	vim.cmd([[exec '/Cloze']])
	vim.cmd([[normal! kd]])
	vim.cmd([[normal! "aP]])
	vim.cmd([[/rule{]])
	vim.cmd([[normal! k]])
	vim.cmd([[normal! A}}]])
	vim.cmd([[?\?]])
	vim.cmd([[normal! j]])
	vim.cmd([[normal! I{{c1::]])
	vim.cmd([[w!]])
	vim.cmd([[AnkiSend]])
	vim.cmd([[normal! u]])
	vim.cmd([[normal! gg]])
	vim.cmd([[exec '/rule{']])
	vim.cmd([[normal! V]])
	vim.cmd([[exec '?\?']])
	vim.cmd([[normal! ddd]])
end

-- vim.cmd [[let @p = '<80><fc>^DP']]

keymap("n", "<C-p>", "<cmd>lua anki_automate()<CR>", term_opts)
--[[ keymap("n", "<C-o>", "<cmd>lua anki_automate_single()<CR>", term_opts) ]]
--[[ keymap("n", "<leader>c", "<cmd>lua auto_count()<CR>", term_opts) ]]
keymap("n", "<leader>v", "<cmd>lua real_count()<CR>", term_opts)

-- reading
keymap(
	"n",
	"<leader>`",
	"<cmd>!xdotool key --delay 0 ctrl+minus ctrl+minus ctrl+minus ctrl+minus ctrl+minus ctrl+minus ctrl+minus ctrl+minus ctrl+minus ctrl+minus ctrl+minus ctrl+minus ctrl+minus ctrl+minus ctrl+minus ctrl+minus <cr><cr>",
	term_opts
)
keymap("n", "<leader><Tab>", "<cmd>!xdotool key --delay 0 ctrl+0 <cr><cr>", term_opts)

-- Text correction
keymap("n", "<leader>s", "[s1z=`]", term_opts)

-- reload nvim
keymap("n", "<leader>o", "<cmd>source ~/.config/nvim/init.lua<cr>", term_opts)

-- For the hop plugin
keymap("n", "<leader>d", "<cmd>lua require'hop'.hint_words()<cr>", term_opts)

-- Command mode
keymap("n", "<leader>c", "q:i", term_opts)
