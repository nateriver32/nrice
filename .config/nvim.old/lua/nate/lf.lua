local fn = vim.fn
local lf = require("lf")

local lines = 70
local columns = 350

-- Defaults
lf.setup({
  default_action = "drop", -- default action when `Lf` opens a file
  default_actions = { -- default action keybindings
  --[[ ["o"] = "tabedit", ]]
  --[[ [ex"] = "split", ]]
  --[[ ["v"] = "vsplit", ]]
  --[[ ["o"] = "tab drop", ]]
  },
  winblend = 0, -- psuedotransparency level
  dir = "", -- directory where `lf` starts ('gwd' is git-working-directory, ""/nil is CWD)
  direction = "float", -- window type: float horizontal vertical
  border = "single", -- border kind: single double shadow curved
  height = fn.float2nr(fn.round(0.75 * lines)), -- height of the *floating* window
  width = fn.float2nr(fn.round(0.75 * columns)), -- width of the *floating* window
  escape_quit = false, -- map escape to the quit command (so it doesn't go into a meta normal mode)
  focus_on_open = false, -- focus the current file when opening Lf (experimental)
  mappings = true, -- whether terminal buffer mapping is enabled
  tmux = false, -- tmux statusline can be disabled on opening of Lf
  default_file_manager = true, -- make lf default file manager
  disable_netrw_warning = true, -- don't display a message when opening a directory with `default_file_manager` as true
  highlights = { -- highlights passed to toggleterm
    Normal = {link = "Normal"},
    NormalFloat = {link = 'Normal'},
    FloatBorder = {guifg = "NONE", guibg = "#000000"},
  },

  -- Layout configurations
  layout_mapping = "<M-u>", -- resize window with this key
  views = { -- window dimensions to rotate through
    {width = 0.800, height = 0.800},
    {width = 0.600, height = 0.600},
    {width = 0.950, height = 0.950},
    {width = 0.500, height = 0.500, col = 0, row = 0},
    {width = 0.500, height = 0.500, col = 0, row = 0.5},
    {width = 0.500, height = 0.500, col = 0.5, row = 0},
    {width = 0.500, height = 0.500, col = 0.5, row = 0.5},
}
})
