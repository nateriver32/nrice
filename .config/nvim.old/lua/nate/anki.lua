local anki = require("anki")

anki.setup ({
  tex_support = true,
      models = {
        ["My Cloze"] = "Learning",
        ["My Basic"] = "Learning",
      },
})
