local colors = {
  blue    = '#00aaff',
  cyan    = '#00ffff',
  blacka  = '#00000000',
  black   = '#000000',
  white   = '#ffffff',
  red     = '#f92672',
  magenta = '#ff00ff',
  grey    = '#707070',
  green   = "#a6e22e",
  yellow  = "#ffd242"
}

local nate_theme = {
  normal = {
    a = { fg = colors.black, bg = colors.blue, gui = 'bold' },
    b = { fg = colors.white, bg = colors.blacka, gui = 'bold' },
    c = { fg = colors.white, bg = colors.blacka, gui = 'bold' },
  },
  command = {
    a = { fg = colors.black, bg = colors.yellow, gui = 'bold' },
    b = { fg = colors.white, bg = colors.blacka, gui = 'bold' },
    c = { fg = colors.white, bg = colors.blacka, gui = 'bold' },
  },
  insert = {
    a = { fg = colors.black, bg = colors.green, gui = 'bold' },
    b = { fg = colors.white, bg = colors.blacka, gui = 'bold' },
    c = { fg = colors.white, bg = colors.blacka, gui = 'bold' },
  },
  visual = {
    a = { fg = colors.black, bg = colors.magenta, gui = 'bold' },
    b = { fg = colors.white, bg = colors.blacka, gui = 'bold' },
    c = { fg = colors.white, bg = colors.blacka, gui = 'bold' },
  },
  replace = {
    a = { fg = colors.black, bg = colors.red, gui = 'bold' },
    b = { fg = colors.white, bg = colors.blacka, gui = 'bold' },
    c = { fg = colors.white, bg = colors.blacka, gui = 'bold' },
  },
  inactive = {
    a = { fg = colors.white, bg = colors.blacka },
    b = { fg = colors.white, bg = colors.blacka },
    c = { fg = colors.black, bg = colors.blacka },
  }
}


require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = nate_theme,
    component_separators = { left = '', right = ''},
    section_separators = { left = '', right = ''},
    disabled_filetypes = {
      statusline = {},
      winbar = {},
    },
    ignore_focus = {},
    always_divide_middle = true,
    globalstatus = true,
    refresh = {
      statusline = 1000,
      tabline = 1000,
      winbar = 1000,
    }
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff', 'diagnostics'},
    lualine_c = {'filename'},
    lualine_x = {'encoding', 'fileformat', 'filetype'},
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
  },
  tabline = {},
  winbar = {},
  inactive_winbar = {},
  extensions = {}
}
