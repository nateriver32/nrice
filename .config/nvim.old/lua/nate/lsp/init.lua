local status_ok, _ = pcall(require, "lspconfig")
if not status_ok then
  return
end

require("nate.lsp.mason")
require("nate.lsp.handlers").setup()
require("nate.lsp.null-ls")
