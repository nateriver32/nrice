local colors = {

  -- Base colors
  bg              = os.getenv("BG0"),
  black           = os.getenv("BG0"),
  blue            = os.getenv("BLUE"),
  cyan            = os.getenv("CYAN"),
  fg              = os.getenv("WHITE"),
  fg2             = os.getenv("WHITE2"),
  gray            = os.getenv("GRAY"),
  green           = os.getenv("GREEN"),
  magenta         = os.getenv("MAGENTA"),
  orange          = os.getenv("ORANGE"),
  purple          = os.getenv("PURPLE"),
  red             = os.getenv("RED"),
  yellow          = os.getenv("YELLOW"),

  -- Custom colors
  cursearch   = os.getenv("YELLOW"),
  selection   = os.getenv("RED"),
  visual_mode = os.getenv("VISUAL_MODE"),

  add         = os.getenv("GREEN"),   -- git add
  bracket     = os.getenv("WHITE"),   -- Brackets and Punctuation
  btype       = os.getenv("YELLOW"),  -- Builtin type
  change      = os.getenv("YELLOW"),  -- git change
  comment     = os.getenv("GRAY"),    -- comment
  conditional = os.getenv("YELLOW"),  -- conditional and loop
  const       = os.getenv("ORANGE"),  -- constants, imports and booleans
  delete      = os.getenv("RED"),     -- git delete
  dep         = os.getenv("RED"),     -- deprecated
  error       = os.getenv("RED"),     -- Errors
  field       = os.getenv("PURPLE"),  -- field
  func        = os.getenv("MAGENTA"), -- functions and titles
  ident       = os.getenv("WHITE"),   -- identifiers
  keyword     = os.getenv("YELLOW"),  -- keywords
  namespace   = os.getenv("PURPLE"),  -- namespaces
  number      = os.getenv("ORANGE"),  -- numbers
  operator    = os.getenv("WHITE"),   -- operators
  preproc     = os.getenv("GRAY"),    -- PreProc
  regex       = os.getenv("WHITE"),   -- Regex
  special     = os.getenv("WHITE"),   -- Builtin variable
  statement   = os.getenv("PURPLE"),  -- Statements
  string      = os.getenv("GREEN"),   -- Strings
  struct      = os.getenv("PURPLE"),  -- functions and titles
  type        = os.getenv("YELLOW"),  -- Types
  variable    = os.getenv("WHITE"),   -- Variables
  warning     = os.getenv("ORANGE"),  -- Warnings

}

return colors
