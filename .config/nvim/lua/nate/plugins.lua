-- INITIALIZE --
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	local lazyrepo = "https://github.com/folke/lazy.nvim.git"
	vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
end ---@diagnostic disable-next-line: undefined-field
vim.opt.rtp:prepend(lazypath)

-- CONFIGURE AND INSTALL --
require("lazy").setup({

	{ "mbbill/undotree" },

	{ "google/vim-searchindex" },

	{ "mg979/vim-visual-multi" },

	{ "octaltree/cmp-look" },

	{ "lervag/vimtex" },

	{ "kylechui/nvim-surround", version = "*", event = "VeryLazy" },

	{ "ej-shafran/compile-mode.nvim", dependencies = "nvim-lua/plenary.nvim" },

	{ "lmburns/lf.nvim", dependencies = "akinsho/toggleterm.nvim",
		config = function()
			require("lf").setup({
			direction = "horizontal",
			border = "single",
			mappings = true,
			default_actions = { ["`"] = "vsplit" },
			default_file_manager = true,
			disable_netrw_warning = true,
		})
	end
	},

	{ "NvChad/nvim-colorizer.lua",
		config = function()
			require("colorizer").setup({
				filetypes = { "*" }, user_default_options = { RGB = true, RRGGBB = true } })
		end
	},

	{ "numToStr/Comment.nvim" },

	{ "ibhagwan/fzf-lua",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		config = function()
			require("fzf-lua").setup(
			{ winopts = { split = "belowright new", preview = { border = "single", layout = "horizontal", horizontal = "right:50%" } } })
		end
	},

	{ "rareitems/anki.nvim",
		lazy = true,
		config = function()
			require("anki").setup(
			{ tex_support = true, models = { ["My Cloze"] = "3.Learning", ["My Basic"] = "3.Learning" } })
		end
	},
			
	{ "hrsh7th/nvim-cmp",
		dependencies = {
			"L3MON4D3/LuaSnip",
			"saadparwaiz1/cmp_luasnip",
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-path",
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-cmdline",
			"hrsh7th/cmp-nvim-lsp-signature-help",
		},
	},

	{ 'neovim/nvim-lspconfig',
		dependencies = {
			{ 'williamboman/mason.nvim', config = true },
			'williamboman/mason-lspconfig.nvim',
			'WhoIsSethDaniel/mason-tool-installer.nvim',
			'hrsh7th/cmp-nvim-lsp',
		},
	},

},
	{
		ui = {
			border = "single",
			size = {
				width  = 0.8,
				height = 0.8,
			},
			icons = {
				cmd     = "",
				config  = "",
				event   = "",
				ft      = "",
				init    = "",
				keys    = "",
				plugin  = "",
				runtime = "󰜎",
				require = "",
				source  = "",
				start   = "",
				task    = "",
				lazy    = "󰒲 ",
			},
		},
	}
)
