local servers = {
	-- clangd = {},
}

vim.api.nvim_create_autocmd("LspAttach", {
	group = vim.api.nvim_create_augroup("kickstart-lsp-attach", { clear = true }),
	callback = function(event)

		local map = function(keys, func, desc)
			vim.keymap.set("n", keys, func, { buffer = event.buf, desc = "LSP: " .. desc })
		end

		map("<leader>gd", function() vim.lsp.buf.definition() end, "[G]oto [D]efinition")
		vim.keymap.set("n", "gr", function() vim.lsp.buf.references() end, opts)
		map("<leader>rn", vim.lsp.buf.rename, "[R]e[n]ame")
		map("<leader>ca", vim.lsp.buf.code_action, "[C]ode [A]ction")
		map("<leader>hd", vim.lsp.buf.hover, "Hover Documentation")
		map("<leader>gl", vim.diagnostic.open_float, "Hover Diagnostics")
		map("<leader>gD", vim.lsp.buf.declaration, "[G]oto [D]eclaration")

		local signs = {
			{ name = "DiagnosticSignError", text = "" },
			{ name = "DiagnosticSignWarn", text  = "" },
			{ name = "DiagnosticSignHint", text  = "" },
			{ name = "DiagnosticSignInfo", text  = "" },
		}

		local config = {
			virtual_text = false,
			signs = false,
			update_in_insert = true,
			underline = false,
			severity_sort = true,
			float = {
				focusable = false,
				style = "minimal",
				border = "single",
				source = "always",
				header = "",
				prefix = "",
			},
		}

		vim.diagnostic.config(config)

		vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
			border = "single",
			config,
		})

		vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
			border = "single",
		})
	end
})

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = vim.tbl_deep_extend('force', capabilities, require('cmp_nvim_lsp').default_capabilities())

require("mason").setup({
	ui = {
		border = "single",
		icons = {
			package_installed   = " ",
			package_pending     = " ",
			package_uninstalled = " ",
		},
	},
})

local ensure_installed = vim.tbl_keys(servers or {})
vim.list_extend(ensure_installed, {})
require('mason-tool-installer').setup { ensure_installed = ensure_installed }

require('mason-lspconfig').setup {
	handlers = {
		function(server_name)
			local server = servers[server_name] or {}
			server.capabilities = vim.tbl_deep_extend('force', {}, capabilities, server.capabilities or {})
			require('lspconfig')[server_name].setup(server)
		end,
	},
}

require("lspconfig.ui.windows").default_options.border = "single"
