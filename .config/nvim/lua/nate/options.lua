-- Booleans
vim.g.neovide          = true
vim.opt.spell          = false
vim.opt.hidden         = true
vim.opt.ttyfast        = true
vim.opt.lazyredraw     = true
vim.opt.modifiable     = true
vim.opt.ruler          = true
vim.opt.title          = true
vim.opt.autowriteall   = true
vim.opt.backup         = false
vim.opt.hlsearch       = true
vim.opt.ignorecase     = true
vim.opt.smartcase      = true
vim.opt.showmode       = false
vim.opt.smartindent    = false
vim.opt.autoindent     = false
vim.opt.breakindent    = false
vim.opt.cindent        = false
vim.opt.splitbelow     = true
vim.opt.splitright     = true
vim.opt.swapfile       = false
vim.opt.termguicolors  = true
vim.opt.undofile       = true
vim.opt.writebackup    = false
vim.opt.expandtab      = false
vim.opt.cursorline     = true
vim.opt.cursorcolumn   = false
vim.opt.number         = true
vim.opt.relativenumber = true
vim.opt.wrap           = true
vim.opt.list           = true
vim.g.have_nerd_font   = true


-- Numbers
vim.opt.textwidth      = 125
vim.opt.history        = 10000
vim.opt.cmdheight      = 1
vim.opt.conceallevel   = 0
vim.opt.pumheight      = 20
vim.opt.showtabline    = 0
vim.opt.timeoutlen     = 1500
vim.opt.updatetime     = 100
vim.opt.softtabstop    = 4
vim.opt.shiftwidth     = 4
vim.opt.tabstop        = 4
vim.opt.numberwidth    = 2
vim.opt.scrolloff      = 5
vim.opt.sidescrolloff  = 5
vim.opt.laststatus     = 3

-- Compound
vim.opt.signcolumn     = "yes:2"
vim.opt.spelllang      = { "en_us" }
vim.opt.clipboard      = "unnamedplus"
vim.opt.completeopt    = { "menuone", "noselect" }
vim.opt.fileencoding   = "utf-8"
vim.opt.mouse          = "a"
vim.opt.indentexpr     = ""
vim.opt.undodir        = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.guifont        = "Iosevka Nerd Font:h14"
vim.opt.guicursor      = "n-v-i-c:block-Cursor-blinkon1,v:block-Cursor-blinkon1"
vim.opt.listchars      = { tab = "  ", trail = " ", nbsp = " " }
-- vim.opt.listchars   = { tab = ' ', trail = '·', nbsp = '␣', eol = '↵' }
vim.opt.inccommand     = "split"
vim.opt.signcolumn     = "no"
vim.opt.statusline     = "%{v:lua.GetMode()} %= %f %= %l:%c:%p%%" vim.opt.fillchars = { eob = ' ' }
vim.opt.iskeyword:remove("-")
vim.opt.iskeyword:remove("_")
vim.opt.shortmess:append("c")

-- Misc
vim.cmd("runtime macros/matchit.vim")
vim.cmd("filetype plugin on")
vim.cmd("autocmd BufNewFile,BufRead * setlocal formatoptions-=cro")
vim.cmd("set whichwrap+=<,>,[,],h,l")
vim.cmd("let g:VM_maps                       = {}")
vim.cmd("let g:VM_maps['Find Under']         = '``'")
vim.cmd("let g:VM_maps['Find Subword Under'] = '``'")
vim.cmd("let g:VM_maps['Add Cursor At Pos']  = '\\\'")

-- VimTeX
vim.g.tex_flavor               = 'latex'
vim.g.vimtex_view_method       = 'zathura'
vim.g.tex_conceal              = 'abdmg'
vim.g.vimtex_quickfix_mode     = 0
vim.g.vimtex_imaps_enabled     = 0
vim.g.vimtex_indent_enabled    = 0
vim.g.vimtex_imaps_enabled     = 1
vim.g.vimtex_complete_enabled  = 0
vim.g.vimtex_syntax_enabled    = 0
vim.g.vimtex_quickfix_mode     = 0
vim.opt.conceallevel           = 0

-- Variables
local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup
local yank_group = augroup("HighlightYank", {})

-- Highlight on copy
autocmd("TextYankPost", {
	group = yank_group,
	pattern = "*",
	callback = function()
		vim.highlight.on_yank({
			higroup = "Visual",
			timeout = 50,
		})
	end,
})

-- FZF window height
vim.api.nvim_create_autocmd({ "FileType" }, {
	pattern = { "fzf*" },
	callback = function()
		vim.api.nvim_win_set_height(0, 9)
	end,
})

-- Save cursor position 
vim.api.nvim_create_autocmd("BufReadPost", {
	callback = function()
		local last_pos = vim.fn.line("'\"")
		if last_pos >= 1 and last_pos <= vim.fn.line("$") and vim.bo.filetype ~= "commit" then
			vim.cmd("normal! g`\"")
		end
	end,
})

-- Modeline
function GetMode()
	local modes = {
		['n'] = 'NORMAL',
		['i'] = 'INSERT',
		['v'] = 'VISUAL',
		['V'] = 'VISUAL',
		['c'] = 'COMMAND',
		['R'] = 'REPLACE',
		['t'] = 'TERMINAL',
	}
	return modes[vim.api.nvim_get_mode().mode] or 'FREEFORM'
end
