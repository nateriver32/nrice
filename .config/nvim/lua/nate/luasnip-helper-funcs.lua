local ls = require("luasnip")
local sn = ls.snippet_node
local i = ls.insert_node

local M = {}

function M.get_visual(args, parent)
	if #parent.snippet.env.LS_SELECT_RAW > 0 then
		return sn(nil, i(1, parent.snippet.env.LS_SELECT_RAW))
	else
		return sn(nil, i(1, ""))
	end
end

M.in_mathzone = function() -- math context detection
	local is_inside = vim.fn["vimtex#env#is_inside"]("align")
	return vim.fn["vimtex#syntax#in_mathzone"]() == 1 or (is_inside[1] > 0 and is_inside[2] > 0)
end

-- text detection
M.in_text = function()
	return not M.in_mathzone()
end

-- comment detection
M.in_comment = function()
	return vim.fn["vimtex#syntax#in_comment"]() == 1
end

-- generic environment detection
M.in_env = function(name)
	local is_inside = vim.fn["vimtex#env#is_inside"](name)
	return (is_inside[1] > 0 and is_inside[2] > 0)
end

-- generic environment detection
M.in_align = function(name)
	local is_inside = vim.fn["vimtex#env#is_inside"]("align")
	return (is_inside[1] > 0 and is_inside[2] > 0)
end

-- equation environment detection
M.in_equation = function()
	return M.in_env("equation")
end

-- itemize environment detection
M.in_itemize = function()
	return M.in_env("itemize")
end

-- itemize environment detection
M.in_enumerate = function()
	return M.in_env("enumerate")
end

-- TikZ picture environment detection
M.in_tikz = function()
	return M.in_env("tikzpicture")
end

return M
