-- CONFIGURATION --
local opts = { noremap = true, silent = true }
local term_opts = { silent = true }
local keymap = vim.api.nvim_set_keymap
local kmap = vim.keymap.set
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "
vim.g.maplocalleader = " "

-- NORMAL MODE --
-- Delete all but current buffer
keymap("n", "<leader><leader>i", ":%bd! | e# | bd#<cr>", opts)

-- Toggle quickfix
keymap("n", "<leader>;", ":lua ToggleQuickfix()<CR>", opts)

-- FZF
keymap("n", "<Tab>q", ":FzfLua helptags <cr>", opts)
keymap("n", "<Tab>b", ":FzfLua buffers <cr>", opts)
keymap("n", "<Tab>j", ":FzfLua files <cr>", opts)
keymap("n", "<Tab>k", ":FzfLua grep <cr>", opts)
keymap("n", "<Tab>l", ":FzfLua live_grep <cr>", opts)
keymap("n", "<Tab>m", ":FzfLua manpages <cr>", opts)

-- File manager
keymap("n", "<Tab>\\", "<cmd>Lf<cr>", opts)

-- Better substitution
keymap("n", "&", ":&&<CR>", opts)

-- Better movement
keymap("n", "$", "g$", opts)
keymap("n", "0", "g0", opts)
keymap("n", "b", "ge", opts)
keymap("n", "j", "gj", opts)
keymap("n", "k", "gk", opts)
keymap("n", "<leader>j", ":join<CR>", opts)
keymap("n", "<Up>", ":bnext<CR>", opts)
keymap("n", "<Down>", ":bprevious<CR>", opts)
keymap("n", "<S-Up>", "<cmd>cprev<CR>zz", opts)
keymap("n", "<S-Down>", "<cmd>cnext<CR>zz", opts)

-- Better copy
keymap("n", "yap", "myyap`y", opts)
keymap("n", "yip", "myyip`y", opts)
keymap("n", "yi\'", "myyi\'`y", opts)
keymap("n", "yi(", "myyi(`y", opts)
keymap("n", "yi[", "myyi[`y", opts)
keymap("n", "yi{", "myyi{`y", opts)
keymap("n", "yit", "myyit`y", opts)
keymap("n", "ya\"", "myya\"`y", opts)
keymap("n", "ya\'", "myya\'`y", opts)
keymap("n", "ya(", "myya(`y", opts)
keymap("n", "ya[", "myya[`y", opts)
keymap("n", "ya{", "myya{`y", opts)
keymap("n", "yat", "myyat`y", opts)
keymap("n", "yi)", "myyi)`y", opts)
keymap("n", "yi]", "myyi]`y", opts)
keymap("n", "yi}", "myyi}`y", opts)
keymap("n", "yit", "myyit`y", opts)
keymap("n", "ya\"", "myya\"`y", opts)
keymap("n", "ya\'", "myya\'`y", opts)
keymap("n", "ya)", "myya)`y", opts)
keymap("n", "ya]", "myya]`y", opts)
keymap("n", "ya}", "myya}`y", opts)
keymap("n", "yat", "myyat`y", opts)

-- Undo tree
vim.cmd("let g:undotree_DiffAutoOpen = 0")
vim.cmd("let g:undotree_SplitWidth = 44")
keymap("n", "<leader>u", ":UndotreeToggle <cr> | <C-w>w", opts)

-- Text correction
keymap("n", "<leader>\\", "[s1z=`]", term_opts)

-- Diagnostics
kmap("n", "[d", vim.diagnostic.goto_prev)
kmap("n", "]d", vim.diagnostic.goto_next)
kmap("n", "<leader>'", vim.diagnostic.setloclist)
kmap("n", "<Esc>", ":nohlsearch<CR>", opts)

-- VimTeX
keymap("n", "<leader>tt", "<cmd>VimtexTocToggle<cr>", opts)
keymap("n", "<leader>ll", "<cmd>VimtexCompile<cr>", opts)

-- Misc
keymap("n", "<C-d>", "<C-d>zz", opts)
keymap("n", "<C-u>", "<C-u>zz", opts)
keymap("n", "<leader>p", '"0p', opts)

-- VISUAL MODE --
keymap("v", "<Tab>", ">gv", opts)
keymap("v", "<S-Tab>", "<gv", opts)
keymap("v", "<leader>y", '"+y', opts)

-- Better movement
keymap("v", "j", "gj", opts)
keymap("v", "k", "gk", opts)
keymap("v", "$", "g$", opts)
keymap("v", "0", "g0", opts)
keymap("v", "b", "ge", opts)

-- Better substitution
keymap("x", "&", ":&&<CR>", opts)

-- Move text up and down
keymap("x", "<S-j>", ":move '>+1<CR>gv-gv", opts)
keymap("x", "<S-k>", ":move '<-2<CR>gv-gv", opts)
keymap("x", "<leader>p", '"_dp', opts)
keymap("v", "<leader>p", '"_dp', opts)

-- FUNCTIONS --
-- Anki
function anki_all()
	auto_count()
	local number_of_questions = tonumber(vim.fn.getreg("+"))
	for i = 1, number_of_questions do
		anki_automate()
	end
end

function anki_automate_single()
	vim.cmd([[/rule{]])
	vim.cmd([[normal! V]])
	vim.cmd([[exec '?\?']])
	vim.cmd([[normal! "ad]])
	vim.cmd([[normal! V]])
	vim.cmd([[exec '?Cloze']])
	vim.cmd([[normal! j]])
	vim.cmd([[exec '/Cloze']])
	vim.cmd([[normal! kd]])
	vim.cmd([[normal! "aP]])
	vim.cmd([[/rule{]])
	vim.cmd([[normal! k]])
	vim.cmd([[normal! A}}]])
	vim.cmd([[?\?]])
	vim.cmd([[normal! j]])
	vim.cmd([[normal! I{{c1::]])
	vim.cmd([[w!]])
	vim.cmd([[AnkiSend]])
	vim.cmd([[normal! u]])
	vim.cmd([[normal! gg]])
	vim.cmd([[exec '/rule{']])
	vim.cmd([[normal! V]])
	vim.cmd([[exec '?\?']])
	vim.cmd([[normal! ddd]])
end

function auto_count()
	vim.cmd([[let i=1 | .,$g/\[\$\]/s//\=i..'[$]'/ | let i+=1]])
	vim.cmd([[normal viw"+yu{]])
end

function real_count()
	vim.cmd([[let i=1 | g/\[\$\]/s//\=i..'[$]'/ | let i+=1]])
end

function anki_automate()
	vim.cmd([[/rule{]])
	vim.cmd([[normal k]])
	vim.cmd([[normal A}}]])
	vim.cmd([[?\?]])
	vim.cmd([[normal j]])
	vim.cmd([[normal I{{c1::]])
	vim.cmd([[w!]])
	vim.cmd([[AnkiSend]])
	vim.cmd([[%s/{{c1:://g]])
	vim.cmd([[%s/}}//g]])
	vim.cmd([[normal }]])
end

-- Misc
function ToggleQuickfix()
  local qf_open = false
  for _, win in ipairs(vim.api.nvim_list_wins()) do
	if vim.api.nvim_buf_get_option(vim.api.nvim_win_get_buf(win), "buftype") == "quickfix" then
	  qf_open = true
	  break
	end
  end
  if qf_open then
	vim.cmd("cclose")
  else
	vim.cmd("copen")
  end
end

