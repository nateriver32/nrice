local cmp = require("cmp")
local luasnip = require("luasnip")
require("luasnip/loaders/from_vscode").lazy_load()

cmp.setup({

	snippet = {
		expand = function(args)
			luasnip.lsp_expand(args.body)
		end,
	},

	completion = { completeopt = "menu,menuone,noinsert" },

	mapping = cmp.mapping.preset.insert({
		["<left>"] = cmp.mapping.scroll_docs(-4),
		["<right>"] = cmp.mapping.scroll_docs(4),
		["<Tab>"] = cmp.mapping.confirm({ select = true }),
		["<up>"] = cmp.mapping.select_prev_item(),
		["<down>"] = cmp.mapping.select_next_item(),
	}),

	sources = cmp.config.sources({
		-- { name = "nvim_lsp" },
		-- { name = "nvim_lsp_signature_help" },
		{ name = "luasnip" },
		{ name = "path" },
		{ name = "calc" },
		{ name = "buffer" },
		{ name = "look",
		keyword_length = 2,
		option = {
			convert_case = true,
			loud         = true,
			dict         = "/usr/share/dict/words",
		},
	},
}),

confirm_opts = {
	behavior = cmp.ConfirmBehavior.Replace,
	select   = false,
},

window = {
	completion = {
		border = "single",
		winhighlight = 'Normal:CmpPmenu,FloatBorder:FloatBorder,CursorLine:PmenuSel,Search:None',
	},
	documentation = {
		border = { "┌", "─", "┐", "│", "┘", "─", "└", "│" },
		completion = {
			border = "single",
		},
	},
},

experimental = {
	ghost_text = false,
	native_menu = false,
},
})
