local helpers = require("nate.luasnip-helper-funcs")
local get_visual = helpers.get_visual
local tikz = helpers.in_tikz
local math = helpers.in_mathzone
local equation = helpers.in_equation
local text = helpers.in_text
local align = helpers.in_align
local line_begin = require("luasnip.extras.expand_conditions").line_begin
local ls = require("luasnip")

return {

	s({ trig = ";c", snippetType = "autosnippet", regTrig = true, wordTrig = false }, fmta([[č]], {})),

	s({ trig = "`c", snippetType = "autosnippet", regTrig = true, wordTrig = false }, fmta([[ć]], {})),

	s({ trig = ";s", snippetType = "autosnippet", regTrig = true, wordTrig = false }, fmta([[š]], {})),

	s({ trig = ";z", snippetType = "autosnippet", regTrig = true, wordTrig = false }, fmta([[ž]], {})),

	s({ trig = ";d", snippetType = "autosnippet", regTrig = true, wordTrig = false }, fmta([[đ]], {})),

	s(
		{ trig = ";tt", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmta(
			[[
[latex]\texttt{<>}[/latex]
    ]],
			{
				i(1),
			}
		)
	),

	s(
		{ trig = "exa", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmta(
			[[
[latex]\textbf{EXAMPLES: begin}[/latex]

<>
[latex]\textbf{EXAMPLES: end}[/latex]
    ]],
			{
				i(1),
			}
		),
		{ condition = line_begin }
	),

	s(
		{ trig = "ččč ", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmta([[^^^ADDED^^^]], {}),
		{ condition = line_begin }
	),

	s(
		{ trig = "bold", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmta("[latex]\\textbf{<>}[/latex]", {
			d(1, get_visual),
		}),
		{ condition = line_begin }
	),

	s(
		{ trig = ";;; ", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmta([[^^^ADDED^^^]], {}),
		{ condition = line_begin }
	),

	s({ trig = "čč ", snippetType = "autosnippet", regTrig = true, wordTrig = false }, fmta([[?]], {})),

	s(
		{ trig = "(%a);;", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmt([[{}?{}]], {
			f(function(_, snip)
				return snip.captures[1]
			end),
			f(function(args)
				vim.fn.system("w<cr>")
			end),
		})
	),

	s(
		{ trig = "mg", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmt(
			[[<img src = "{}" />
]],
			{
				f(function(args)
					local clipboard_content = vim.fn.system("xclip -o -selection clipboard")
					clipboard_content = clipboard_content:gsub("\n$", "")
					local snippet_text = string.format("%s", clipboard_content)
					vim.fn.system("w<cr>")
					return snippet_text
				end),
			}
		),
		{ condition = line_begin }
	),

	s(
		{ trig = "gm", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmt(
			[[<img src = "{}" />
]],
			{
				f(function(args)
					local clipboard_content = vim.fn.system("xclip -o -selection clipboard")
					clipboard_content = clipboard_content:gsub("\n$", "")
					local snippet_text = string.format("%s", clipboard_content)
					vim.fn.system("w<cr>")
					return snippet_text
				end),
			}
		),
		{ condition = line_begin }
	),

	s(
		{ trig = "igm", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmt(
			[[<img src = "{}" />
]],
			{
				f(function(args)
					local clipboard_content = vim.fn.system("xclip -o -selection clipboard")
					clipboard_content = clipboard_content:gsub("\n$", "")
					local snippet_text = string.format("%s", clipboard_content)
					vim.fn.system("w<cr>")
					return snippet_text
				end),
			}
		),
		{ condition = line_begin }
	),

	s(
		{ trig = "mg", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmt(
			[[<img src = "{}" />
]],
			{
				f(function(args)
					local clipboard_content = vim.fn.system("xclip -o -selection clipboard")
					clipboard_content = clipboard_content:gsub("\n$", "")
					local snippet_text = string.format("%s", clipboard_content)
					vim.fn.system("w<cr>")
					return snippet_text
				end),
			}
		),
		{ condition = line_begin }
	),

	s(
		{ trig = "img", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmt(
			[[<img src = "{}" />
]],
			{
				f(function(args)
					local clipboard_content = vim.fn.system("xclip -o -selection clipboard")
					clipboard_content = clipboard_content:gsub("\n$", "")
					local snippet_text = string.format("%s", clipboard_content)
					vim.fn.system("w<cr>")
					return snippet_text
				end),
			}
		),
		{ condition = line_begin }
	),

	s(
		{ trig = "latex", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmta("[latex]<>[/latex]", {
			d(1, get_visual),
		})
	),

	s(
		{ trig = "en", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmta("[$]\\rule{\\textwidth}{1pt}[/$]<>", {
			f(function(args)
				vim.fn.system("w<cr>")
			end),
		}),
		{ condition = line_begin }
	),

	s(
		{ trig = "lien", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmta("[$]\\rule{\\textwidth}{1pt}[/$]<>", {
			f(function(args)
				vim.fn.system("w<cr>")
			end),
		}),
		{ condition = line_begin }
	),

	s(
		{ trig = "ien", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmta("[$]\\rule{\\textwidth}{1pt}[/$]<>", {
			f(function(args)
				vim.fn.system("w<cr>")
			end),
		}),
		{ condition = line_begin }
	),

	s(
		{ trig = "line", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmta("[$]\\rule{\\textwidth}{1pt}[/$]<>", {
			f(function(args)
				vim.fn.system("w<cr>")
			end),
		}),
		{ condition = line_begin }
	),

	s(
		{ trig = "lein", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmta("[$]\\rule{\\textwidth}{1pt}[/$]<>", {
			f(function(args)
				vim.fn.system("w<cr>")
			end),
		}),
		{ condition = line_begin }
	),

	s(
		{ trig = "ine", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmta("[$]\\rule{\\textwidth}{1pt}[/$]<>", {
			f(function(args)
				vim.fn.system("w<cr>")
			end),
		}),
		{ condition = line_begin }
	),

	s(
		{ trig = "ne", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmta("[$]\\rule{\\textwidth}{1pt}[/$]<>", {
			f(function(args)
				vim.fn.system("w<cr>")
			end),
		}),
		{ condition = line_begin }
	),

	s(
		{ trig = "cl", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmta([[{{c1::<>}}]], {
			d(1, get_visual),
		}),
		{ condition = line_begin }
	),

	s(
		{ trig = "qq", snippetType = "autosnippet", regTrig = true, wordTrig = false },
		fmta([[<>?]], {
			i(1, "question"),
		}),
		{ condition = line_begin }
	),
}
