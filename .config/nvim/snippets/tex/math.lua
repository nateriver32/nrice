local helpers = require('nate.luasnip-helper-funcs')
local get_visual = helpers.get_visual
local tikz = helpers.in_tikz
local math = helpers.in_mathzone
local equation = helpers.in_equation
local text = helpers.in_text
local align = helpers.in_align

return{

  s({trig = "nn", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "minus sign"},
    fmta("-",
		{
		}
	),
    { condition = math }
  ),

  s({trig = "pp", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "plus sign"},
    fmta("+",
		{
		}
	),
    { condition = math }
  ),

  s({trig = "ee", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "equals sign"},
    fmta("=",
		{
		}
	),
    { condition = math }
  ),

  s({trig = "amp", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "ampersand sign"},
    fmta("&",
		{
		}
	),
    { condition = math }
  ),

  s({trig = "mbb", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "black board font"},
    fmta([[\mathbb{<>}]],
      {
        i(1),
      }
    ),
    { condition = math }
  ),

  s({trig = ";in", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "element of"},
    fmta("\\in",
      {
      }
    ),
    { condition = math }
  ),

  -- statistics

  s({trig = "obar", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "overbar"},
    fmta("\\bar {<>}",
      {
        i (1),
      }
    ),
    { condition = math }
  ),

  -- algebra

  s({trig = "inv", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "one over something"},
    fmta("\\frac{1}{<>}",
      {
        i (1),
      }
    ),
    { condition = math }
  ),

  s({trig = "sab", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "super and sub script"},
    fmta("_{<>}^{<>}",
      {
        i (1),
        i (2),
      }
    ),
    { condition = math }
  ),

  s({trig = "([^%a])ae", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "align equals"},
    fmta("<>&=",
      {
        f( function(_, snip) return snip.captures[1] end ),
      }
    ),
    { condition = math }
  ),

  s({trig = "cds", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "center dots"},

    fmta("\\cdots",
      {
      }
    ),
    { condition = math }
  ),

  s({trig = "cd", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "center dot"},
    fmta("\\cdot",
      {
      }
    ),
    { condition = math }
  ),

  s({trig = "root", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "nth root"},
    fmta("\\sqrt[<>]{<>}",
      {
        i (1),
        i (2),
      }
    ),
    { condition = math }
  ),

  s({trig = "([%A])mm", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "math mode"},
    fmta("<>$ <> $",
      {
        f( function(_, snip) return snip.captures[1] end ),
        d(1, get_visual),
      }
    ),
    { }
  ),

  s({trig = "^([%s]*)mm", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "beginning of line math mode"},
    fmta("$ <> $ <>",
      {
        i(1),
        i(2),
      }
    )
  ),

  s({trig = "([^%a])dm", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "display math mode"},
    fmta([[
  <>\[
  <> 
  .\]
  ]],
      {
        f( function(_, snip) return snip.captures[1] end ),
        d(1, get_visual),
      }
    )
  ),

  s({trig = "^([%s]*)dm", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "beginning of line display math mode"},
    fmta([[
  \[
  <> 
  .\]
  <>
  ]],
      {
        i(1, "<>"),
        i(2, "<>"),
      }
    )
  ),

  s({trig = "([^%a])ff", snippetType = "autosnippet", wordTrig = false, regTrig = true, dscr = "fraction"},
    fmta("<>\\frac{<>}{<>}",
      {
        f( function(_, snip) return snip.captures[1] end ),
        i(1),
        i(2),
      }
    ),
    { condition = math }
  ),

  s({trig = '(%a)(%a+)bb', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "subscript"},
    fmta(
      "<>_{<>}",
      {
        f( function(_, snip) return snip.captures[1] end ),
        f( function(_, snip) return snip.captures[2] end ),
      }
    ),
    { condition = math }
  ),

  s({trig = '(%a)(%a+)aa', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "superscript"},
    fmta(
      "<>^{<>}",
      {
        f( function(_, snip) return snip.captures[1] end ),
        f( function(_, snip) return snip.captures[2] end ),
      }
    ),
    { condition = math }
  ),

  s({trig = '(%a)(%d+)bb', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "number subscript"},
    fmta(
      "<>_{<>}",
      {
        f( function(_, snip) return snip.captures[1] end ),
        f( function(_, snip) return snip.captures[2] end ),
      }
    ),
    { condition = math }
  ),

  s({trig = '(%a)(%d+)aa', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "number superscript"},
    fmta(
      "<>^{<>}",
      {
        f( function(_, snip) return snip.captures[1] end ),
        f( function(_, snip) return snip.captures[2] end ),
      }
    ),
    { condition = math }
  ),

  s({trig = '([%a])sa', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "general superscipt"},
    fmta(
      "<>^{<>}",
      {
        f( function(_, snip) return snip.captures[1] end ),
        i(1),
      }
    ),
    { condition = math }
  ),

  s({trig = '([%a])sb', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "general subscipt"},
    fmta(
      "<>_{<>}",
      {
        f( function(_, snip) return snip.captures[1] end ),
        i(1),
      }
    ),
    { condition = math }
  ),

  -- trigonometry

  s({trig = 'cos', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "cosine"},
    fmta(
      "\\cos<>",
      {
        i(1),
      }
    ),
    { condition = math }
  ),

  s({trig = 'sin', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "sine"},
    fmta(
      "\\sin<>",
      {
        i(1),
      }
    ),
    { condition = math }
  ),

  s({trig = 'tan', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "tangent"},
    fmta(
      "\\tan<>",
      {
        i(1),
      }
    ),
    { condition = math }
  ),

  s({trig = 'cot', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "cotangent"},
    fmta(
      "\\cot<>",
      {
        i(1),
      }
    ),
    { condition = math }
  ),

  --vector calculus

  s({trig = 'div', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "divergence"},
    fmta(
      [[\nabla\cdot]],
      {
      }
    ),
    { condition = math }
  ),

  s({trig = 'curl ', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "curl"},
    fmta(
      [[\nabla\times]],
      {
      }
    ),
    { condition = math }
  ),

  s({trig = 'vec', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "vector"},
    fmta(
      [[\vec{<>}]],
      {
        i(1),
      }
    ),
    { condition = math }
  ),

  s({trig = 'lap', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "laplacian"},
    fmta(
      [[\nabla^2 ]],
      {
      }
    ),
    { condition = math }
  ),

  s({trig = 'grad', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "gradient"},
    fmta(
      [[\nabla ]],
      {
      }
    ),
    { condition = math }
  ),

  s({trig = 'pd(.)', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "nth partial derivative"},
    fmta(
      [[\frac{\partial^{<>} <>}{\partial <>^{<>}}]],
      {
        f( function(_, snip) if snip.captures[1] == "1" then
          return print("")
        else
          return snip.captures[1] end 
        end),
        i(1),
        i(2),
        f( function(_, snip) if snip.captures[1] == "1" then
          return print("")
        else
          return snip.captures[1] end
        end),
      }
    ),
    { condition = math }
  ),

  s({trig = 'par', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "superscript"},
    fmta(
      "\\partial",
      {
      }
    ),
    { condition = math }
  ),

  -- calculus

  s({trig = 'od(.)', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "ordinary derivative"},
    fmta(
      [[\frac{\diff^{<>} <>}{\diff <>^{<>}}]],
      {
        f( function(_, snip) if snip.captures[1] == "1" then
          return print("")
        else
          return snip.captures[1] end 
        end),
        i(1),
        i(2),
        f( function(_, snip) if snip.captures[1] == "1" then
          return print("")
        else
          return snip.captures[1] end
        end),
      }
    ),
    { condition = math }
  ),

  s({trig = 'matd(.)', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "material derivative"},
    fmta(
      [[\frac{\matdiff^{<>} <>}{\matdiff <>^{<>}}]],
      {
        f( function(_, snip) if snip.captures[1] == "1" then
          return print("")
        else
          return snip.captures[1] end 
        end),
        i(1),
        i(2),
        f( function(_, snip) if snip.captures[1] == "1" then
          return print("")
        else
          return snip.captures[1] end
        end),
      }
    ),
    { condition = math }
  ),

  s({trig = 'dot', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "dot like times"},
    fmta(
      [[\dot]],
      {
      }
    ),
    { condition = math }
  ),

  s({trig = 'oo', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "infinity"},
    fmta(
      "\\infty",
      {
      }
    ),
    { condition = math }
  ),

  s({trig = 'iiint', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "triple integral"},
    fmta(
      "\\iiint\\limits_{<>}^{<>}{<>}",
      {
        i(1),
        i(2),
        i(3),
      }
    ),
    { condition = math }
  ),

  s({trig = 'iint', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "double integral"},
    fmta(
      "\\iint\\limits_{<>}^{<>}{<>}",
      {
        i(1),
        i(2),
        i(3),
      }
    ),
    { condition = math }
  ),

  s({trig = 'int', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "single integral"},
    fmta(
      "\\int\\limits_{<>}^{<>}{<>}",
      {
        i(1),
        i(2),
        i(3),
      }
    ),
    { condition = math }
  ),

  s({trig = 'sm', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "sum"},
    fmta(
      "\\sum\\limits_{<>}^{<>}",
      {
        i(1),
        i(2),
      }
    ),
    { condition = math }
  ),

  s({trig = "df", snippetType = "autosnippet", dscr = "differential"},
    { t("\\diff ") },
    { condition = math }
  ),

  s({trig = 'lm', snippetType = 'autosnippet', regTrig = true, wordTrig = false, dscr = "limit"},
    fmta(
      "\\lim\\limits_{<> \\rightarrow <>}",
      {
        i(1),
        i(2),
      }
    ),
    { condition = math }
  ),

  -- tikz

  s({trig = "dd", snippetType = 'autosnippet', dscr = "draw"},
    fmta(
      "\\draw [<>]",
      {
        i(1),
      }
    ),
    { condition = tikz }
  ),

  -- greek letters

  s({trig=";a", snippetType="autosnippet"},
    fmta ([[\alpha<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";b", snippetType="autosnippet"},
    fmta ([[\beta<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";g", snippetType="autosnippet"},
    fmta ([[\gamma<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";G", snippetType="autosnippet"},
    fmta ([[\Gamma<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";d", snippetType="autosnippet"},
    fmta ([[\delta<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";D", snippetType="autosnippet"},
    fmta ([[\Delta<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";ep", snippetType="autosnippet"},
    fmta ([[\epsilon<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";z", snippetType="autosnippet"},
    fmta ([[\zeta<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";et", snippetType="autosnippet"},
    fmta ([[\eta<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";the", snippetType="autosnippet"},
    fmta ([[\theta<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";The", snippetType="autosnippet"},
    fmta ([[\Theta<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";i", snippetType="autosnippet"},
    fmta ([[\iota<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";ka", snippetType="autosnippet"},
    fmta ([[\kappa<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";la", snippetType="autosnippet"},
    fmta ([[\lambda<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";L", snippetType="autosnippet"},
    fmta ([[\Lambda<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";m", snippetType="autosnippet"},
    fmta ([[\mu<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";n", snippetType="autosnippet"},
    fmta ([[\nu<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";x", snippetType="autosnippet"},
    fmta ([[\xi<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";X", snippetType="autosnippet"},
    fmta ([[\Xi<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";p", snippetType="autosnippet"},
    fmta ([[\pi<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";P", snippetType="autosnippet"},
    fmta ([[\Pi<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";r", snippetType="autosnippet"},
    fmta ([[\rho<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";s", snippetType="autosnippet"},
    fmta ([[\sigma<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";S", snippetType="autosnippet"},
    fmta ([[\Sigma<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";ta", snippetType="autosnippet"},
    fmta ([[\tau<>]],
    i (1)),
    { condition = math }
  ),

  s({trig=";u", snippetType="autosnippet"},
    fmta ([[\upsilon<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";U", snippetType="autosnippet"},
    fmta ([[\Upsilon<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";f", snippetType="autosnippet"},
    fmta ([[\varphi<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";F", snippetType="autosnippet"},
    fmta ([[\Phi<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";c", snippetType="autosnippet"},
    fmta ([[\chi<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";y", snippetType="autosnippet"},
    fmta ([[\psi<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";Y", snippetType="autosnippet"},
    fmta ([[\Psi<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";o", snippetType="autosnippet"},
    fmta ([[\omega<>]],
      i (1)),
    { condition = math }
  ),

  s({trig=";O", snippetType="autosnippet"},
    fmta ([[\Omega<>]],
      i (1)),
    { condition = math }
  ),

  s({trig="''", snippetType="autosnippet"},
    fmta ([[\label{eq:<>}]],
      i (1)),
    { condition = math }
  ),

}
