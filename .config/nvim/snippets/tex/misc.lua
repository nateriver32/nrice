local helpers = require("nate.luasnip-helper-funcs")
local get_visual = helpers.get_visual
local text = helpers.in_text
local line_begin = require("luasnip.extras.expand_conditions").line_begin
local math = helpers.in_mathzone
local align = helpers.in_align

return {

	s(
		{ trig = "pkg", snippetType = "autosnippet", wordTrig = false, regTrig = true, desc = "greek nomenclature" },
		fmta([[\usepackage[<>]{<>}]], {
			i(1),
			i(2),
		}),
		{ condition = line_begin }
	),

	s(
		{ trig = "gnm", snippetType = "autosnippet", wordTrig = false, regTrig = true, desc = "greek nomenclature" },
		fmta([[\nmg[<>]{<>}{<>}{<>}]], {
			i(1),
			i(2),
			i(3),
			i(4),
		}),
		{}
	),

	s(
		{ trig = "lnm", snippetType = "autosnippet", wordTrig = false, regTrig = true, desc = "latin nomenclature" },
		fmta([[\nml[<>]{<>}{<>}{<>}]], {
			i(1),
			i(2),
			i(3),
			i(4),
		}),
		{}
	),

	s(
		{ trig = ";lab", snippetType = "autosnippet", wordTrig = false, regTrig = true, desc = "label an object" },
		fmta([[\label{<>:<>}]], {
			i(1),
			i(2),
		}),
		{}
	),

	s(
		{ trig = ";ref", snippetType = "autosnippet", wordTrig = false, regTrig = true, desc = "insert reference" },
		fmta([[\ref{<>:<>}]], {
			i(1),
			i(2),
		}),
		{}
	),

	s(
		{ trig = ";nsi", snippetType = "autosnippet", wordTrig = false, regTrig = true, desc = "insert an si unit" },
		fmta([[\si{<>}]], {
			i(1),
		}),
		{}
	),

	s(
		{ trig = ";qsi", snippetType = "autosnippet", wordTrig = false, regTrig = true, desc = "insert an si unit" },
		fmta([[\qty{<>}{<>}]], {
			i(1),
			i(2),
		}),
		{}
	),

	s(
		{ trig = "cpg", snippetType = "autosnippet", wordTrig = false, regTrig = true, desc = "clear page" },
		fmta([[\clearpage]], {}),
		{ condition = line_begin }
	),

	s(
		{ trig = "pbr", snippetType = "autosnippet", wordTrig = false, regTrig = true, desc = "pagebreak" },
		fmta([[\pagebreak]], {}),
		{ condition = line_begin }
	),

	s(
		{
			trig = "lip",
			snippetType = "autosnippet",
			wordTrig = false,
			regTrig = true,
			desc = "chapter command for articles",
		},
		fmta([[\lipsum[<>] ]], {
			i(1),
		}),
		{ condition = line_begin }
	),

	s(
		{
			trig = "cha",
			snippetType = "autosnippet",
			wordTrig = false,
			regTrig = true,
			desc = "chapter command for articles",
		},
		fmta([[\chapter{<>}]], {
			i(1),
		}),
		{ condition = line_begin }
	),

	s(
		{
			trig = ";cha",
			snippetType = "autosnippet",
			wordTrig = false,
			regTrig = true,
			desc = "chapter command for articles",
		},
		fmta([[\chapter;{<>}]], {
			i(1),
		}),
		{ condition = line_begin }
	),

	s(
		{
			trig = "sssec",
			snippetType = "autosnippet",
			wordTrig = false,
			regTrig = true,
			desc = "subsubsection command for articles",
		},
		fmta([[\subsubsection{<>}]], {
			i(1),
		}),
		{ condition = line_begin }
	),

	s(
		{
			trig = ";sssec",
			snippetType = "autosnippet",
			wordTrig = false,
			regTrig = true,
			desc = "subsubsection command for articles",
		},
		fmta([[\subsubsection*{<>}]], {
			i(1),
		}),
		{ condition = line_begin }
	),

	s(
		{
			trig = "ssec",
			snippetType = "autosnippet",
			wordTrig = false,
			regTrig = true,
			desc = "subsection command for articles",
		},
		fmta([[\subsection{<>}]], {
			i(1),
		}),
		{ condition = line_begin }
	),

	s(
		{
			trig = ";ssec",
			snippetType = "autosnippet",
			wordTrig = false,
			regTrig = true,
			desc = "subsection command for articles",
		},
		fmta([[\subsection*{<>}]], {
			i(1),
		}),
		{ condition = line_begin }
	),

	s(
		{
			trig = ";sec",
			snippetType = "autosnippet",
			wordTrig = false,
			regTrig = true,
			desc = "section command for articles",
		},
		fmta([[\section*{<>}]], {
			i(1),
		}),
		{ condition = line_begin }
	),

	s(
		{
			trig = "sec",
			snippetType = "autosnippet",
			wordTrig = false,
			regTrig = true,
			desc = "section command for articles",
		},
		fmta([[\section{<>}]], {
			i(1),
		}),
		{ condition = line_begin }
	),

	s(
		{
			trig = "mbox",
			snippetType = "autosnippet",
			wordTrig = false,
			regTrig = true,
			desc = "makebox function for text alignment and manipulation",
		},
		fmta([[\makebox[<>][<>]{<>}]], {
			i(1, "width"),
			i(2, "pos (c, l, r, s)"),
			i(3, "text"),
		})
	),

	s(
		{
			trig = "pbox",
			snippetType = "autosnippet",
			wordTrig = false,
			regTrig = true,
			desc = "paragraph box for paragraph manipulation",
		},
		fmta([[\parbox[<>][<>][<>]{<>}{<>}]], {
			i(1, "pos"),
			i(2, "height"),
			i(3, "contentpos"),
			i(4, "width"),
			i(5, "text"),
		})
	),

	s(
		{
			trig = "dtx",
			snippetType = "autosnippet",
			wordTrig = false,
			regTrig = true,
			desc = "page text width and height variables",
		},
		fmta([[<>]], {
			c(1, { t("\\textwidth"), t("\\textheight") }),
		})
	),

	s(
		{
			trig = "dis",
			snippetType = "autosnippet",
			wordTrig = false,
			regTrig = true,
			desc = "display style for math mode",
		},
		fmta([[\displaystyle{<>}]], {
			d(1, get_visual),
		}),
		{ condition = math }
	),
}
