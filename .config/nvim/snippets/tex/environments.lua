local helpers = require('nate.luasnip-helper-funcs')
local line_begin = require('luasnip.extras.expand_conditions').line_begin
local get_visual = helpers.get_visual
local tikz = helpers.in_tikz
local math = helpers.in_mathzone
local text = helpers.in_text
local list = helpers.in_itemize
local nlist = helpers.in_enumerate

return {

 s({trig = "np", snippetType = "autosnippet", desc = "new page"},
    fmta(
      [[
    \newpage
    ]],
      {
      }
    ),
    {condition = line_begin * text }
  ),

 s({trig = "ifig", snippetType = "autosnippet", desc = "tikz figure"},
    fmta(
      [[
    \begin{figure}
        \centering
        \incfig{<>}
        \caption{<>.}
        \label{fig:<>}
    \end{figure}
    ]],
      {
        i(1),
        i(2),
        i(3),
      }
    ),
    {condition = line_begin * text }
  ),

 s({trig = "tfig", snippetType = "autosnippet", desc = "tikz figure"},
    fmta(
      [[
      \begin{nfigure}
      \centering
        \begin{tikzpicture}
            <>
        \end{tikzpicture}
        \caption{<>.}
        \label{fig:<>}
      \end{nfigure}
    ]],
      {
        i(1),
        i(2),
        i(3),
      }
    ),
    {condition = line_begin * text }
  ),

 s({trig = "nlist", snippetType = "autosnippet", desc = "enumerate environment"},
    fmta(
      [[
      \begin{enumerate}[label=\textbf{\arabic*}.]
          <>
      \end{enumerate}
    ]],
      {
        i(1),
      }
    ),
    {condition = line_begin * text }
  ),

  s({trig = "list", snippetType = "autosnippet", desc = "list environment"},
    fmta(
      [[
      \begin{itemize}[label=$\triangleright$]
          <>
      \end{itemize}
    ]],
      {
        i(1),
      }
    ),
    {condition = line_begin * text }
  ),

  s({trig = "item", snippetType = "autosnippet", desc = "itemize environment"},
    fmta(
      [[\item <>]],
      {
        i(1),
      }
    ),
    {condition = function() return list or nlist end * line_begin }
  ),

 s({trig = "beg", snippetType = "autosnippet", desc = "default begin end environment"},
    fmta(
      [[
      \begin{<>}
          <>
      \end{<>}
    ]],
      {
        i(1),
        i(2),
        rep(1),
      }
    ),
    {condition = line_begin * text }
  ),

  s({trig = "eq", snippetType = "autosnippet", desc = "equation environment"},
    fmta(
      [[
      \begin{<>}
          <>
      .\end{<>}
    ]],
      {
        c(1, { t("align"), t("align*"), t("equation"), t("equation*") }),
        i(2),
        rep(1),
      }
    ),
    { condition = line_begin }
  ),

  s({trig="fig", snippetType="autosnippet", dscr="figure environment"},
    fmta(
      [[
      \begin{nfigure}
          \centering
          \includegraphics[width=\textwidth]{<>}
          \caption{<>.}
          \label{fig:<>}
      \end{nfigure}
    ]],
      {
        i(1),
        i(2),
        i(3),
      }
    ),
    { condition = line_begin * text  }
  ),

  s({trig="tab", snippetType="autosnippet", dscr="table environment"},
    fmta(
      [[
      \begin{ntable}
          \centering
          \caption{<>.}
          \label{tab:<>}
          \begin{tabular}{<>}
            <>
          \end{tabular}
      \end{ntable}
    ]],
      {
        i(1),
        i(2),
        i(3),
        i(4),
      }
    ),
    { condition = line_begin * text  }
  ),

}
