(aas-set-snippets 'org-mode
    :cond #'texmathp
    "`a" "\\alpha "
    "`b" "\\beta "
    "`g" "\\gamma "
    "`d" "\\delta "
    "`e" "\\epsilon "
    "`z" "\\zeta "
    "`n" "\\eta "
    "``t" "\\theta "
    "`i" "\\iota "
    "`k" "\\kappa "
    "`l" "\\lambda "
    "`u" "\\mu "
    "`v" "\\nu "
    "`x" "\\xi "
    "`p" "\\pi "
    "`r" "\\rho "
    "`s" "\\sigma "
    "`t" "\\tau "
    "`f" "\\varphi "
    "`h" "\\chi "
    "`o" "\\omega "
    "`A" "\\Alpha "
    "`B" "\\Beta "
    "`G" "\\Gamma "
    "`D" "\\Delta "
    "`E" "\\Epsilon "
    "`Z" "\\Zeta "
    "`N" "\\Eta "
    "`T" "\\Theta "
    "`I" "\\Iota "
    "`K" "\\Kappa "
    "`L" "\\Lambda "
    "`U" "\\Mu "
    "`V" "\\Nu "
    "`X" "\\Xi "
    "`P" "\\Pi "
    "`R" "\\Rho "
    "`S" "\\Sigma "
    "`T" "\\Tau "
    "`F" "\\Phi "
    "`H" "\\Chi "
    "`O" "\\Omega "
    "ff" '(yas "\\frac{$1}{$2}")
    "sb" '(yas "_{$1} $0")
    "se" "se"
    "sa" '(yas "^{$1} $0")
    "mrm" '(yas "\\mathrm{$1}")
    "mbb" '(yas "\\mathbb{$1}")
    "mbf" '(yas "\\mathbf{$1}")
    "pp" "+"
    "mm" "-"
    "ee" "="
    ";in" "\\in"
    ";bar" '(yas "\\bar{$1}")
    ";." "\\cdot"
    "`." "\\cdots"
    "root" '(yas "\\sqrt{$2}")
    "cos" "\\cos"
    "sin" "\\sin"
    "tan" "\\tan"
    "cot" "\\cot"
    "par" "\\partial"
    "nab" "\\nabla"
    "div" "\\nabla\\cdot"
    "curl" "\\nabla\\times"
    "r`" "\\rightarrow"
    "l`" "\\leftarrow"
    "rr`" "\\Rightarrow"
    "ll`" "\\Leftarrow"
    "vec" '(yas "\\vec{$1}")
    "lrp" '(yas "\\left($1\\right)")
    "lrc" '(yas "\\left{$1\\right}")
    "lrs" '(yas "\\left[$1\\right]")
    "lap" '(yas "\\nabla^2 ")
    "pd" '(yas "\\frac{\\partial $1}{\\partial $2}")
    "od" '(yas "\\frac{\\diff $1}{\\diff $2}")
    "`8" '(yas "\\infty")
    "int" '(yas "\\int\\limits_{$1}^{$2}{$3} $0")
    "iint" '(yas "\\iint\\limits_{$1}^{$2}{$3} $0")
    "iiint" '(yas "\\iiint\\limits_{$1}^{$2}{$3} $0")
    "sum" '(yas "\\sum\\limits_{$1}^{$2} $0")
    "df" '(yas "\\diff ")
    "lim" '(yas "\\lim\\limits_{$1 \\rightarrow $2} $0")
    "text" '(yas "\\text{$1}")
    "amp" "&")

(aas-set-snippets 'org-mode
    ";c" "č"
    "`c" "ć"
    ";z" "ž"
    ";d" "đ"
    ";s" "š"
    ";--" "—")
