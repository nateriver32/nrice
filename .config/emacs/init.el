; Ensure all packages get installed automatically
(package-initialize)
(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")
                         ("nongnu" . "https://elpa.nongnu.org/nongnu/")))
(unless package-archive-contents
  (package-refresh-contents))
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

; THEME                                                                                                                                  
(use-package ef-themes)                                                                                                         ; Themes

; MISC
(setq custom-file "~/.config/emacs/custom.el")											; Stop emacs from throwing crap in init.el
(load-file custom-file)														; Load the custom file

; PACKAGES
; (package-vc-install '(org-mode :url "https://code.tecosaur.net/tec/org-mode"))  ; Fetch the package
(use-package org														; Org mode for optimized for LaTeX rendering
    :load-path "~/.config/emacs/elpa/org-mode/lisp/"
    :hook
    (org-mode . org-bullets-mode)
    (org-mode . org-indent-mode))

(use-package org-bullets
    :custom (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))							        ; Better bullet points

(use-package toc-org :hook (org-mode . toc-org-enable))										; Table of contents

(setq org-file-apps '(("\\.pdf\\'" . "zathura %s")))										; Default pdf viewer
(setq org-log-done 'time)													; Set closing time when completing a todo or similar item
(setq org-ellipsis " ▾")													; Better heading closing icon
(setq org-format-latex-options (plist-put org-format-latex-options :zoom 0.9))							; Change the size of previews
(setq org-latex-preview-numbered 0)												; Keep equation numbers consistent
(setq org-latex-preview-live t)													; Keep the previews live as you type
(setq org-latex-preview-live-debounce 0.1)											; Faster previews 
(setq org-startup-with-inline-images t)												; Render images on startup
(setq org-highlight-latex-and-related '(latex entities))									; Highlight LaTeX syntax when in org mode
(remove-hook 'org-mode-hook 'org-latex-preview-auto-mode)									        ; Automatic LaTeX previews
; (add-hook 'org-mode-hook 'org-toggle-pretty-entities)                                                                         ; Used for making math mode and similar better looking 

(setq evil-want-keybinding nil)													; Needed for the thing below to work properly
(use-package evil :config (evil-mode 1))											; Vim emulation

(use-package evil-collection :config (evil-collection-init))									; More vim emulation 

(use-package evil-multiedit :config (evil-multiedit-mode))                              					; Multiple cursors 

(use-package vertico														; Better minibuffers
    :custom
    (vertico-cycle nil)
    (vertico-count 10)
    :config
    (vertico-reverse-mode)
    (vertico-mode))

(use-package orderless														; Fuzzy finding ability in minibuffers
    :config
    (orderless-define-completion-style orderless+initialism
    (orderless-matching-styles '(orderless-initialism
                                 orderless-literal
                                 orderless-regexp)))
    (setq completion-styles '(orderless)
         completion-category-defaults nil
         orderless-matching-styles '(orderless-literal orderless-regexp)
         completion-category-overrides
         '((file (styles partial-completion)))))                                                                  

(use-package consult
    :config
    (setq consult-buffer-sources '(consult--source-hidden-buffer
                                  consult--source-modified-buffer
                                  consult--source-buffer
                                  consult--source-project-buffer-hidden
                                  consult--source-project-recent-file-hidden)))							; Better searching capabilities 

(use-package marginalia :config (marginalia-mode))										; Show information about items in minubuffer

(require 'saveplace)
(save-place-mode)                                                                                                               ; Save cursor position in file

; SETTINGS
(tool-bar-mode 0)														; Disable tool bar
(menu-bar-mode 0)														; Disable menu bar
(scroll-bar-mode 0)														; Disable scroll bar
(visual-line-mode 0)														; Enable soft wrap
(set-buffer-modified-p nil)													; Disable buffer modification prompts on exit
(global-display-line-numbers-mode 1)												; Enable line numbers
(add-to-list 'default-frame-alist `(font . "JetBrainsMono Nerd Font-14"))							; Set font
(global-visual-line-mode t)													; enable truncated lines
(delete-selection-mode 1)													; You can select text and delete it by typing.
(electric-indent-mode -1)													; Turn off the weird indenting that emacs does by default.
(electric-pair-mode 0)														; Turns off automatic parens pairing
(set-fringe-mode 10)														; Number of lines before scrolling
(tooltip-mode -1)														; Disable tooltips

; VARIABLES
(setq x-super-keysym 'meta)													; Set windows key as meta key
(setq confirm-kill-emacs nil)													; suppress the confirmation prompt when quitting Emacs
(setq confirm-kill-processes nil)												; Prevent prompts for saving modified buffers
(setq confirm-nonexistent-file-or-buffer nil)											; Disable annoying prompts
(setq confirm-kill-emacs nil)													; Don't prompt when exiting
(setq use-short-answers t)													; Use short answers for confirmation
(setq use-file-dialog nil)													; No file dialog
(setq pop-up-windows nil)													; No popup windows
(setq use-dialog-box nil)													; No dialog box
(setq inhibit-splash-screen t)													; Disable splash screen
(setq inhibit-startup-message t)												; Hide startup message
(setq inhibit-startup-echo-area-message t)											; Hide echo area message
(setq display-line-numbers-type 'relative)											; Enable relative linue numbers
(setq make-backup-files nil)													; Don't make backup files
(setq scroll-step 1
      scroll-conservatively 101
      scroll-margin 5
      scroll-preserve-screen-position nil)											; Better scrolling
(setq compile-command "")													; Remove the the default compile command
(setq org-return-follows-link t)												; Return will follow a link
(setq
   split-width-threshold nil
   split-height-threshold 0)

; KEYBINDS
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)										; Disable the escape-meta behaviour
(global-set-key (kbd "M-f") 'dired-other-window)										; Open dired in current dir
(evil-global-set-key 'motion "j" 'evil-next-visual-line)									; Make j go down a line normally when wrapped
(evil-global-set-key 'motion "k" 'evil-previous-visual-line)									; Make k go down a line normally when wrapped
(define-key evil-normal-state-map (kbd "RET") nil)										; Disable the return key in normal mode
(define-key evil-normal-state-map (kbd "SPC f") 'find-file)									; Find a file 
(define-key evil-normal-state-map (kbd "<up>") 'consult-buffer)									; Search buffers
(define-key evil-normal-state-map (kbd "SPC g") 'consult-ripgrep)								; Grep inside the current directory
(define-key evil-normal-state-map (kbd "SPC l") 'consult-line)									; Find locations inside current file based on line
(define-key evil-normal-state-map (kbd "SPC o") 'consult-outline)								; Find location inside current buffer based on headin
(define-key evil-normal-state-map (kbd "SPC q") 'org-latex-export-to-pdf)							; Make LaTeX file
(define-key evil-normal-state-map (kbd "SPC a") 'org-latex-preview-auto-mode)							; Automatically preview latex
(define-key evil-normal-state-map (kbd "SPC p") 'org-latex-preview)							        ; Preview latex
(define-key evil-motion-state-map (kbd "RET") nil)                                                                              ; Disable the enter key to allow links to work
(define-key evil-normal-state-map (kbd "SPC h") 'evil-split-buffer)                                                                ; Split a buffer horizontally and open another

(define-key evil-visual-state-map "R" 'evil-multiedit-match-all)				                                ; Highlights all matches of the selection in the buffer.
(define-key evil-normal-state-map (kbd "M-d") 'evil-multiedit-match-and-next)			                                ; incrementally add the next unmatched match.
(define-key evil-visual-state-map (kbd "M-d") 'evil-multiedit-match-and-next)			                                ; Match selected region.
(define-key evil-insert-state-map (kbd "M-d") 'evil-multiedit-toggle-marker-here)		                                ; Insert marker at point
(define-key evil-normal-state-map (kbd "M-D") 'evil-multiedit-match-and-prev)			                                ; Same as M-d but in reverse.
(define-key evil-visual-state-map (kbd "M-D") 'evil-multiedit-match-and-prev)
(define-key evil-visual-state-map (kbd "C-M-D") 'evil-multiedit-restore)			                                ; Restore the last group of multiedit regions.
(define-key evil-multiedit-mode-map (kbd "RET") 'evil-multiedit-toggle-or-restrict-region)	                                ; RET will toggle the region under the cursor
(define-key evil-motion-state-map (kbd "RET") 'evil-multiedit-toggle-or-restrict-region)	                                ; ...and in visual mode, RET will disable all fields outside the selected region
(define-key evil-multiedit-mode-map (kbd "C-n") 'evil-multiedit-next)
(define-key evil-multiedit-mode-map (kbd "C-p") 'evil-multiedit-prev)
(evil-ex-define-cmd "ie[dit]" 'evil-multiedit-ex-match)						                                ; Ex command that allows you to invoke evil-multiedit with a regular expression, e.g.

; LaTeX
(use-package auctex)														; More LaTeX flexibility
(with-eval-after-load 'ox-latex
    (add-to-list 'org-latex-classes
        '("myreport" 
        "\\documentclass{report}"
;	("\\part{%s}" . "\\part*{%s}")
        ("\\chapter{%s}" . "\\chapter*{%s}")
        ("\\section{%s}" . "\\section*{%s}")
        ("\\subsection{%s}" . "\\subsection*{%s}")
        ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))									; Define documents boilerplate
(setq org-latex-default-packages-alist '()
      org-latex-packages-alist '())												; Define the packages to load 
; (setq org-latex-pdf-process '("latexmk -pdflatex='pdflatex -interaction=nonstopmode -shell-escape' -pdf -bibtex -f %f"))	; pdf compile command
(setq org-latex-pdf-process '("latexmk -pdflatex='lualatex -interaction=nonstopmode -shell-escape' -pdf -bibtex -f %f"))	; pdf compile command
(customize-set-value 'org-latex-with-hyperref nil)										; Disable annoying warnings associated with hyperref

(use-package citar)														; Manage bibliography
(setq org-cite-global-bibliography '("~/Documents/Books/references.bib")
    org-cite-insert-processor 'citar
    org-cite-follow-processor 'citar
    org-cite-activate-processor 'citar
    org-cite-export-processors '(latex biblatex)
    citar-bibliography org-cite-global-bibliography)										; Define the references sources

(use-package valign :hook (org-mode . valign-mode))										; Align LaTeX tables appropriately 
(setq org-latex-tables-centered nil)												; To change the default centering behaviour org org-mode tables when exporting to tex
(setq org-cycle-include-plain-lists 'integrate)
(add-hook 'org-mode-hook 'auto-fill-mode)											; Enable auto-fill-mode in org-mode
(add-to-list 'org-latex-packages-alist '("" "listings" nil))                                                                    ; Add listings to syntax formatting with LaTeX previews
; (setq org-link-descriptive nil)							                                        ; Disable the [[]] reference hiding
(use-package org-contrib)
(require 'ox-extra)
(ox-extras-activate '(ignore-headlines))                                                                                        ; Gives org mode more tags useful for LaTeX

; SNIPPETS
(use-package yasnippet
    :config
    (setq yas-snippet-dirs '("~/.config/emacs/snippets"))
    (yas-global-mode 1))                                                                                                        ; Snippet engine
(defun nate-yas-try-expanding-auto-snippets ()
    (when yas-minor-mode
    (let ((yas-buffer-local-condition ''(require-snippet-condition . auto)))
	(yas-expand))))
(add-hook 'post-command-hook #'nate-yas-try-expanding-auto-snippets)							        ; Autoexpansion ability
(define-key yas-minor-mode-map (kbd "SPC") yas-maybe-expand)									; Expand appropriate snippet
(use-package aas :hook (org-mode . aas-activate-for-major-mode))
(aas-global-mode)
(setq snippets-file "~/.config/emacs/snippets.el")
(load-file snippets-file)													; Shorter snippets
(add-to-list 'default-frame-alist '(alpha-background . 100))

; CUSTOM FUNCTIONS
;; Define a custom face for the bold and green highlight
(defface bold-green
  '((t (:foreground "#CF0090" :weight bold)))
  "Face for highlighting the last typed character in green and bold.")

;; Function to highlight the last character typed
(defvar last-char-overlay nil)

(defun highlight-last-char ()
  "Highlight the last typed character in bold and green."
  (let ((point (point)))
    (when (and (not (bobp)) (> point 1))
      ;; Create an overlay for the last character
      (let ((overlay (make-overlay (1- point) point)))
        (overlay-put overlay 'face 'bold-green)
        ;; Remove the overlay after some ms if the cursor moves away
        (run-at-time 0.1 nil 'remove-last-char-overlay overlay)))))

(defun remove-last-char-overlay (overlay)
  "Remove the overlay after the cursor moves away."
  (when (and overlay (overlay-start overlay))
    (delete-overlay overlay)))

;; Add the function to the post-command-hook to highlight the last character
(add-hook 'post-command-hook 'highlight-last-char)
