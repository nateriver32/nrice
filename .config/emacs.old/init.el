 (use-package org
    :load-path "~/.config/emacs/elpa/org-mode/lisp/")

(set-face-attribute 'default nil
  :font "Iosevka Nerd Font"
  :height 120
  :weight 'medium)
(set-face-attribute 'variable-pitch nil
  :font "Iosevka Nerd Font"
  :height 120
  :weight 'medium)
(set-face-attribute 'fixed-pitch nil
  :font "Iosevka Nerd Font"
  :height 120
  :weight 'medium)
;; Makes commented text and keywords italics.
;; This is working in emacsclient but not emacs.
;; Your font must have an italic face available.
(set-face-attribute 'font-lock-comment-face nil
  :slant 'italic)
(set-face-attribute 'font-lock-keyword-face nil
  :slant 'italic)

;; Uncomment the following line if line spacing needs adjusting.
(setq-default line-spacing 0.12)

;; Needed if using emacsclient. Otherwise, your fonts will be smaller than expected.
;; (add-to-list 'default-frame-alist '(font . "Iosevka Nerd Font-11"))
(setq default-frame-alist '((font . "Iosevka Nerd Font-12")))

;; Uncomment the following line if line spacing needs adjusting.
(setq-default line-spacing 0.12)

(defun nate/org-mode-setup ()
    (org-indent-mode)
    (variable-pitch-mode 1)
    (auto-fill-mode 0)
    (visual-line-mode 1)
    (org-autolist-mode 1)
    (setq evil-auto-indent nil)
    (add-hook 'org-mode-hook 'org-latex-preview-auto-mode)
    ;; (add-hook 'org-mode-hook 'org-toggle-pretty-entities)
    ;; Enable auto-fill-mode in org-mode
    (add-hook 'org-mode-hook 'auto-fill-mode)

    ;; Set the default fill column to 80 characters in org-mode
    (add-hook 'org-mode-hook (lambda () (setq fill-column 100)))
)

(defun nate/org-font-setup ()
;; Set faces for heading levels
(dolist (face '((org-level-1 . 4.0)
                (org-level-2 . 3.8)
                (org-level-3 . 3.6)
                (org-level-4 . 3.4)
                (org-level-5 . 3.2)
                (org-level-6 . 3.0)
                (org-level-7 . 2.8)
                (org-level-8 . 2.6)))
(set-face-attribute (car face) nil :font "Iosevka Nerd Font" :weight 'bold :height (cdr face)))


;; Ensure that anything that should be fixed-pitch in Org files appears that way
(set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
(set-face-attribute 'org-code nil   :inherit 'fixed-pitch)
(set-face-attribute 'org-table nil   :inherit 'fixed-pitch)
(set-face-attribute 'org-verbatim nil :inherit 'fixed-pitch)
(set-face-attribute 'org-special-keyword nil :inherit 'fixed-pitch)
(set-face-attribute 'org-meta-line nil :inherit 'fixed-pitch)
(set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)

(font-lock-add-keywords 'org-mode
                        '(("^ *\\([-]\\) "
                            (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "")))))))

(use-package org
:load-path "~/.config/emacs/elpa/org-mode/lisp/"
:hook (org-mode . nate/org-mode-setup)
:config
(setq org-ellipsis " ▾")
(nate/org-font-setup))

(org-babel-load-file
 (expand-file-name
  "config.org"
  user-emacs-directory))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(corfu ox-latex faces yasnippet which-key visual-fill-column vertico valign undo-tree toc-org rainbow-mode projectile org-contrib org-bullets org-autolist orderless marginalia lsp-mode kind-icon helpful general evil-nerd-commenter evil-collection embark-consult doom-themes doom-modeline dired-open consult-dir citar cape auctex all-the-icons-dired all-the-icons-completion aas))
 '(warning-suppress-types '((org))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
