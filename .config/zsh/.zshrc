# Initialize zsh
source $ZDOTDIR/zshrc

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
# ZSH_HIGHLIGHT_STYLES[cursor]='bg=yellow'
ZSH_HIGHLIGHT_STYLES[arg0]='fg=green,bold'
