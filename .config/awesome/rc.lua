-- Libraries
pcall(require, "luarocks.loader")

local awful = require("awful")
local gears = require("gears")
local dpi = require("beautiful.xresources").apply_dpi
require("awful.autofocus")
local wibox = require("wibox")
local beautiful = require("beautiful")
local naughty = require("naughty")
local lain = require("lain")
local menubar_utils = require("menubar.utils")
--local menubar       = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
require("awful.hotkeys_popup.keys")
local mytable = awful.util.table or gears.table -- 4.{0,1} compatibility

-- Error handling
if awesome.startup_errors then
	naughty.notify({
		preset = naughty.config.presets.critical,
		title = "Oops, there were errors during startup!",
		text = awesome.startup_errors,
	})
end

-- Handle runtime errors after startup
do
	local in_error = false

	awesome.connect_signal("debug::error", function(err)
		if in_error then
			return
		end

		in_error = true

		naughty.notify({
			preset = naughty.config.presets.critical,
			title = "Oops, an error happened!",
			text = tostring(err),
		})

		in_error = false
	end)
end

-- Variables
local theme  = "nate"
local lalt   = "Mod1"
local ralt   = "Mod3"
local super  = "Mod4"
local ctrl   = "control"
local tag_overlay = true

-- Applications
local browser  = "brave"
local terminal = "st"

-- awful.util.tagnames = { "W1", "W2", "W3", "W4", "W5", "W6" }
-- awful.util.tagnames = { "TERM", "WWW", "CFD", "EDU", "INK", "RAN" }
awful.util.tagnames = { "1", "2", "3", "4", "5", "6" }
-- awful.util.tagnames = { " ", " ", "󰗚 ", " ", " ", " " }

-- Disable mouse snapping
awful.mouse.snap.default_distance = 1
awful.mouse.snap.client_enabled = false
awful.mouse.snap.edge_enabled = false

-- Layouts
awful.layout.layouts = {
	awful.layout.suit.max,
	awful.layout.suit.tile.right,
	awful.layout.suit.fair,
	awful.layout.suit.fair.horizontal,
	awful.layout.suit.floating,
}

-- Define theme
beautiful.init(string.format("%s/.config/awesome/theme/%s/theme.lua", os.getenv("HOME"), theme))

-- Tag popup effect, uncomment to enable
-- local Effects = require("effects")
-- local tagswitch = Effects.request_effect("tagswitch")

-- Create a wibox for each screen and add it
awful.screen.connect_for_each_screen(function(s)
	beautiful.at_screen_connect(s)
end)

-- Helper functions
local function get_screen_geometry(c)
    local screen = c and c.screen or awful.screen.focused()
    return screen.geometry
end

-- For the resizing and moving windows keydinds
screen_width, screen_height = get_screen_geometry().width, get_screen_geometry().height

-- Function to toggle floating and position the client
local function float_geometry(c)
    if c then
        local screen_geometry = get_screen_geometry(c)
        local screen_width = screen_geometry.width
        local screen_height = screen_geometry.height

        local width = screen_width * 0.8
        local height = screen_height * 0.8
        local x = screen_geometry.x + (screen_width - width) / 2
        local y = screen_geometry.y + (screen_height - height) / 2

        c.floating = not c.floating
        if c.floating then
            c:geometry({ x = x, y = y, width = width, height = height })
        end
    end
end


local function swap_master()
	if client.focus == awful.client.getmaster() then
		awful.client.swap.byidx(1)
		awful.client.focus.byidx(-1)
	else
		awful.client.setmaster(client.focus)
	end
end

function save_current_tag()
	local f = assert(io.open(os.getenv("HOME") .. "/.awesome-last-ws", "w"))
	local t = awful.screen.focused().selected_tag
	f:write(t.name, "\n")
	f:close()
end

function save_current_layout()
	local f = assert(io.open(os.getenv("HOME") .. "/.awesome-last-layout", "w"))
	local t = awful.screen.focused().selected_tag
	local save_layout = awful.layout.get_tag_layout_index(t)
	f:write(save_layout, "\n")
	f:close()
end

function load_last_active_tag()
	local f = assert(io.open(os.getenv("HOME") .. "/.awesome-last-ws", "r"))
	local tag_name = f:read("*line")
	f:close()
	local t = awful.tag.find_by_name(nil, tag_name)
	if t ~= nil then
		awful.tag.viewnone()
		awful.tag.viewtoggle(t)
	end
end

function load_last_active_layout()
	local f = assert(io.open(os.getenv("HOME") .. "/.awesome-last-layout", "r"))
	local layout_index = f:read("*line")
	f:close()
	for s in screen do
		awful.layout.set(awful.layout.layouts[tonumber(layout_index)], s)
	end
end

function sleep(n)
	os.execute("sleep " .. tonumber(n))
end

function toggle_always_on_top(c)
	local c = client.focus
	if c then
		c.ontop = not c.ontop
	end
end

function toggle_sticky(c)
	local c = client.focus
	if c then
		c.sticky = not c.sticky
	end
end

local function resetFloatingWindows()
	for _, c in ipairs(client.get()) do
		-- Check if the client is floating
		if c.fljating then
			-- Move the client to the master area and set it to tiling mode
			c:geometry(c.screen.geometry) -- Ensure window is within screen bounds
			awful.client.floating.toggle(c)
		end
	end
end

-- Variable to keep track of focus on click behavior
local focus_on_click = true

-- Function to toggle focus on click behavior
local function toggle_focus_on_click()
	focus_on_click = not focus_on_click
	naughty.notify({ text = "Focus on click: " .. (focus_on_click and "Enabled" or "Disabled") })
end

-- Keychords
local function keychord(chords)
	local g = awful.keygrabber({
		stop_key = "Escape",
		keypressed_callback = function(self, _, key)
			if chords[key] then
				chords[key]()
			end
			self:stop()
		end,
	})
	return function()
		g:start()
	end
end

local layout = "us"

-- Keybindings
local globalkeys = gears.table.join(

	awful.key(
		{ super }, "a",
		keychord({
			k = function()
				os.execute("sleep 1")
				awful.spawn("killall ffmpeg")
				awful.spawn("killall record.sh")
			end,
			m = function()
				awful.util.spawn("pactl set-source-mute @DEFAULT_SOURCE@ toggle")
			end,
		})
	),

	awful.key({ super }, "1", function()
		awful.util.spawn("mounter.sh")
	end, { description = "mount/unmount drives", group = "layout" }),

	awful.key({ super }, "2", function()
		awful.util.spawn("monitors.sh")
	end, { description = "switch monitors", group = "layout" }),

	awful.key({ super }, "3", function(c)
		awful.spawn("boomer")
	end, { description = "Launch boomer", group = "client" }),

	awful.key({ super }, "4", function(c)
		awful.spawn("youtube_search.sh")
	end, { description = "Search youtube", group = "client" }),

	awful.key({ super }, "5", function(c)
		awful.spawn("sudo_dmenu.sh")
	end, { description = "Run with sudo", group = "client" }),

	awful.key({ super }, "6", function(c)
		hide()
	end, { description = "Hide status bar", group = "client" }),

	awful.key({ super }, "7", function(c)
		awful.spawn("get_music.sh")
	end, { description = "Download music", group = "client" }),

	awful.key({ super }, "8", function(c)
		awful.spawn("get_video.sh")
	end, { description = "Stream video", group = "client" }),

	awful.key({ super }, "9", function(c)
		awful.spawn("notes.sh")
	end, { description = "Open notes", group = "client" }),

	awful.key({ super }, "`", function(c)
		if layout == "hr" then
			awful.spawn("setxkbmap -layout us")
			awesome.emit_signal("keyboard_layout_changed")
			layout = "us"
		elseif layout == "us" then
			awful.spawn("setxkbmap -layout hr")
			awesome.emit_signal("keyboard_layout_changed")
			layout = "hr"
		end
	end, { description = "change keyboard layout", group = "client" }),

	awful.key({ }, "XF86Tools", function(c)
		awful.spawn("st -c small -e sh -c -i 'nvim ~/.config/awesome/rc.lua'")
	end, { description = "edit wm config", group = "client" }),

	-- awful.key({ super }, "-", function(c)
	-- 	awful.spawn("camera_zoom.sh -")
	-- end, { description = "Reset camera zoom", group = "client" }),

	-- awful.key({ super }, "=", function(c)
	-- 	awful.spawn("camera_zoom.sh +")
	-- end, { description = "Zoom camera in", group = "client" }),

	-- awful.key({ super }, "0", function(c)
	-- 	awful.spawn("camera_zoom.sh =")
	-- end, { description = "Reset camera zoom", group = "client" }),

	awful.key({ super }, "Escape", function()
		awful.util.spawn("application_switcher.sh")
	end, { description = "switch to a window", group = "layout" }),

	awful.key({
		on_press = function(index)
			if tagswitch then
				local t = awful.screen.focused().selected_tag
				tagswitch.animate(tostring(t.index))
			end
		end,
	}),

	awful.key({ lalt }, "q", function(c)
		awful.spawn("pcmanfm")
	end, { description = "Open pcmanfm file explorer", group = "client" }),

	awful.key({ lalt }, "b", function(c)
		awful.tag.incnmaster(1, nil, true)
	end, { description = "Increase number of master windows", group = "client" }),

	awful.key({ lalt, "Shift" }, "b", function(c)
		awful.tag.incnmaster(-1, nil, true)
	end, { description = "Decrease number of master windows", group = "client" }),

	awful.key({ lalt }, "t", function(c)
		toggle_always_on_top(c)
	end, { description = "Toggle always on top", group = "client" }),

	awful.key({ lalt, "Shift" }, "t", function(c)
		toggle_sticky(c)
	end, { description = "Toggle sticky", group = "client" }),

	awful.key({ ralt }, "v", function()
		awful.spawn(terminal .. " -c small -e set_wallpaper.sh")
	end, { description = "pick wallpaper", group = "programs" }),

	awful.key({ lalt }, "-", function()
		awful.spawn(terminal .. " -c small -e pulsemixer")
	end, { description = "volume configuration", group = "programs" }),

	awful.key({ lalt }, "n", function()
		awful.layout.inc(1)
	end, { description = "select next layout", group = "layout" }),

	awful.key({ lalt, "Shift" }, "n", function()
		awful.layout.inc(-1)
	end, { description = "select previous layout", group = "layout" }),

	awful.key({ lalt, "Shift" }, "r", function()
		resetFloatingWindows()
		awesome.emit_signal("focus")
		awesome.restart()
	end, { description = "reload awesome", group = "awesome" }),

	awful.key({ ralt, "Shift" }, "r", function()
		reorder_reload = true
		awful.layout.set(awful.layout.layouts[1])
		resetFloatingWindows()
		awesome.restart()
	end, { description = "reload awesome in master tile mode", group = "awesome" }),

	awful.key({ super, "Shift" }, "k", function()
		awful.screen.focus_relative(1)
	end,
		{description = "focus the next screen", group = "screen"}),

	awful.key({ super, "Shift" }, "j", function ()
		if client.focus then
			client.focus:move_to_screen()
		end
	end, {description = "move focused client to the next screen", group = "client"}),

	awful.key({ lalt, "Shift" }, "j", function()
		awful.tag.viewprev()
		if tagswitch then
			local t = awful.screen.focused().selected_tag
			tagswitch.animate(tostring(t.name))
		end
	end, { description = "go to next workspace", group = "tag" }),

	awful.key({ lalt, "Shift" }, "k", function()
		awful.tag.viewnext()
		if tagswitch then
			local t = awful.screen.focused().selected_tag
			tagswitch.animate(tostring(t.name))
		end
	end, { description = "go to next workspace", group = "tag" }),

	awful.key({ lalt }, "h", swap_master),

	awful.key({ lalt }, "Return", function()
		-- local term_tag = awful.tag.find_by_name(nil, "TERM")
		-- awful.tag.viewonly(term_tag)
		awful.util.spawn(terminal)
	end, { description = "open a terminal", group = "launcher" }),

	awful.key({ }, "XF86Display", function()
		awful.util.spawn("monitors.sh")
	end, { description = "switch monitors", group = "layout" }),

	awful.key({ }, "XF86AudioPlay", function()
		awful.util.spawn("mpc toggle")
	end, { description = "pause/play music", group = "programs" }),

	awful.key({ }, "XF86AudioNext", function()
		awful.util.spawn("mpc next")
	end, { description = "next song", group = "programs" }),

	awful.key({ }, "XF86AudioPrev",  function()
		awful.util.spawn("mpc prev")
	end, { description = "previous song", group = "programs" }),

	awful.key({ lalt }, "XF86AudioNext", function()
		awful.util.spawn("mpc seek +5")
	end, { description = "seek forward in song", group = "programs" }),

	awful.key({ lalt }, "XF86AudioPrev", function()
		awful.util.spawn("mpc seek -5")
	end, { description = "seek backward in song", group = "programs" }),

	awful.key({ lalt }, "BackSpace", function()
		awful.util.spawn(terminal .. " -c non-focus")
	end, { description = "open a terminal that can only be focused with a mouse click", group = "launcher" }),

	awful.key({ lalt, "Shift" }, "e", function()
		awful.util.spawn("sh -c 'loginctl terminate-user $(whoami)'")
	end, { description = "logoff", group = "client" }),

	awful.key({ lalt, "Shift" }, "p", function()
		awful.util.spawn("reboot")
	end, { description = "reboot", group = "client" }),

	-- awful.key({ lalt, "Shift" }, "o", function()
	-- 	awful.util.spawn("poweroff")
	-- end, { description = "shutdown", group = "client" }),

	awful.key({ lalt, "Shift" }, "o", function()
		awful.util.spawn("shutdown now")
	end, { description = "shutdown", group = "client" }),

	awful.key({ lalt, "Shift" }, "[", function()
		awful.util.spawn("systemctl suspend")
	end, { description = "suspend", group = "client" }),

	awful.key({ lalt }, "`", function()
		awful.util.spawn("dmenu_run -p 'RUN '")
	end, { description = "dmenu", group = "programs" }),

	awful.key({ ralt }, "`", function()
		awful.util.spawn("rofi -show drun")
	end, { description = "rofi", group = "programs" }),

	-- awful.key({ lalt, "Shift" }, "f", function(c)
	-- 	awful.util.spawn_with_shell("pgrep " .. terminal .. " && (killall " .. terminal .. " && " .. terminal .. " -c file -e bash -i -c 'tmux_fzf_dir.sh || true') || " .. terminal .. " -c file -e bash -i -c 'tmux_fzf_dir.sh || true'")
	-- end, { description = "opens a directory", group = "programs" }),

	-- awful.key({ lalt, "Shift" }, "f", function(c)
	-- 	awful.util.spawn_with_shell(terminal .. " -c file -e bash -i -c 'tmux_fzf_dir.sh || true'")
	-- end, { description = "opens a directory", group = "programs" }),

	awful.key({ lalt, "Shift" }, "f", function(c)
		awful.util.spawn_with_shell(terminal .. " -c file -e bash -i -c 'fzf_dir_open.sh || true'")
	end, { description = "opens a directory", group = "programs" }),

	awful.key({ lalt }, "f", function()
		awful.util.spawn(terminal .. " -c file -e bash -i -c 'fzf_file_open.sh || true'")
	end, { description = "opens a file", group = "programs" }),

	awful.key({ lalt, "Shift" }, "m", function()
		awful.util.spawn("select_track.sh")
	end, { description = "music selector", group = "programs" }),

	awful.key({ ralt }, "b", function()
		awful.util.spawn(browser)
	end, { description = "open browser", group = "programs" }),

	awful.key({ ralt }, "d", function()
		awful.util.spawn("picom-trans -co +5")
	end, { description = "increase opacity", group = "layout" }),

	awful.key({ ralt }, "a", function()
		awful.util.spawn("picom-trans -co -5")
	end, { description = "decrease opacity", group = "layout" }),

	awful.key({ ralt }, "s", function()
		awful.util.spawn("picom-trans -co 80")
	end, { description = "reset opacity", group = "layout" }),

	awful.key({ ralt }, "w", function()
		awful.util.spawn("picom-trans -co 100")
	end, { description = "turn off opacity", group = "layout" }),

	awful.key({ ralt }, "e", function()
		lain.util.useless_gaps_resize(1)
	end, { description = "increase gaps", group = "customize" }),

	awful.key({ ralt }, "q", function()
		lain.util.useless_gaps_resize(-1)
	end, { description = "decrease gaps", group = "customize" }),

	awful.key({ lalt }, "m", function()
		awful.util.spawn("st -c small -e ncmpcpp")
	end, { description = "music player", group = "programs" }),

	awful.key({ ralt }, "Return", function()
		awful.util.spawn("st -c small")
	end, { description = "floating terminal", group = "programs" }),

	awful.key({ }, "XF86MonBrightnessDown", function()
		awesome.emit_signal("brightness")
		awful.util.spawn("brightnessctl set 1%-")
	end, { description = "lower brightness" }),

	awful.key({ }, "XF86MonBrightnessUp", function()
		awesome.emit_signal("brightness")
		awful.util.spawn("brightnessctl set +1%")
	end, { description = "raise brightness" }),

	awful.key({ }, "XF86AudioRaiseVolume", function()
		awesome.emit_signal("volume")
		awful.util.spawn("pactl set-sink-volume @DEFAULT_SINK@ +1%")
	end, { description = "raise volume" }),

	awful.key({ }, "XF86AudioLowerVolume", function()
		awesome.emit_signal("volume")
		awful.util.spawn("pactl set-sink-volume @DEFAULT_SINK@ -1%")
	end, { description = "lower volume" }),

	awful.key( { }, "XF86AudioMute", function()
		awesome.emit_signal("mute")
		awful.util.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle")
	end, { description = "mute volume" }),

	awful.key({ lalt }, "XF86AudioRaiseVolume", function()
		awful.util.spawn("mpc seek +5")
	end, { description = "raise volume" }),

	awful.key({ lalt }, "XF86AudioLowerVolume", function()
		awful.util.spawn("mpc seek -5")
	end, { description = "raise volume" }),

	awful.key({ ralt }, "h", function()
		-- awful.util.spawn("st -c small -e btop")
		awful.util.spawn("st -e btop")
	end, { description = "btop", group = "programs" }),

	awful.key({ super }, "Tab", function()
		awful.util.spawn("slock")
	end, { description = "lockscreen", group = "programs" }),

	awful.key({ ralt }, "z", function()
		awful.util.spawn("st -c small -f '" .. os.getenv("FONT_NAME") .. ":style=regular:pixelsize=24' -e hold_terminal.sh neofetch")
	end, { description = "system info", group = "programs" }),

	awful.key({ lalt }, "/", function()
		awful.util.spawn("timer.sh ")
	end, { description = "timer", group = "programs" }),

	awful.key({ lalt }, "Tab", function()
		awful.util.spawn("anki_screenshot.sh")
	end, { description = "anki screenshot", group = "programs" }),

	awful.key({ lalt }, "u", function()
		awful.util.spawn("sh -c 'flameshot; flameshot gui'")
	end, { description = "screenshot", group = "programs" }),

	awful.key({ ralt }, "k", function()
		awful.util.spawn("st -c small -f '" .. os.getenv("FONT_NAME") .. ":style=bold:pixelsize=38' -e hold_terminal.sh cal -3 --monday")
	end, { description = "calendar", group = "programs" }),

	-- awful.key({ ralt }, "c", function()
	-- 	awful.util.spawn("galculator")
	-- end, { description = "calculator", group = "programs" }),

	awful.key({ ralt }, "c", function()
		awful.util.spawn("st -c small sh -c -i 'python3'") 
	end, { description = "python repl", group = "programs" }),

	awful.key({ ralt }, "t", function()
		awful.util.spawn("st -c game -e tetris -nomenu")
	end, { description = "tetris", group = "programs" }),

	awful.key({ lalt, "Shift" }, "l", function()
		local c = awful.client.restore()
		if c then
			c:emit_signal("request::activate", "key.unminimize", { raise = true })
		end
	end, { description = "restore minimized", group = "client" }),

	awful.key({ lalt }, "k", function()
		awful.client.focus.byidx(1)
	end, { description = "focus next window", group = "client" }),

	awful.key({ lalt }, "j", function()
		awful.client.focus.byidx(-1)
	end, { description = "focus previous window", group = "client" })
)

local clientkeys = gears.table.join(

	awful.key({ lalt, "Shift" }, "h", function(c)
		c.minimized = true
	end, { description = "minimize", group = "client" }),

	awful.key({ lalt }, "Escape", function(c)
		c:kill()
	end, { description = "close", group = "client" }),

	awful.key({ lalt }, "space", function(c)
		float_geometry(c)
	end, { description = "toggle floating", group = "client" }),

	awful.key({ lalt }, "w", function(c)
		if c.height <= 100 then
			return
		end
		if c.floating then
			c:relative_move(0, 0, 0, -1 * screen_height * 0.05)
		else
			awful.client.incwfact(0.025)
		end
	end, { description = "Floating Resize Vertical -", group = "client" }),

	awful.key({ lalt }, "s", function(c)
		if c.floating then
			c:relative_move(0, 0, 0, screen_height * 0.05)
		else
			awful.client.incwfact(-0.025)
		end
	end, { description = "Floating Resize Vertical +", group = "client" }),

	awful.key({ lalt }, "a", function(c)
		if c.width <= 100 then
			return
		end
		if c.floating then
			c:relative_move(0, 0, -1 * screen_width * 0.025, 0)
		else
			awful.tag.incmwfact(-0.025)
		end
	end, { description = "Floating Resize Horizontal -", group = "client" }),

	awful.key({ lalt }, "d", function(c)
		if c.floating then
			c:relative_move(0, 0, screen_width * 0.025, 0)
		else
			awful.tag.incmwfact(0.025)
		end
	end, { description = "Floating Resize Horizontal +", group = "client" }),

	awful.key({ lalt }, "x", function(c)
		if c.floating == true then
			c:relative_move(0, 0, screen_width * 0.025, screen_height * 0.025)
			awful.placement.centered()
		else
			c.floating = true
			c:relative_move(0, 0, screen_width * 0.025, screen_height * 0.025)
			awful.placement.centered()
		end
	end, { description = "Floating resize increase full", group = "client" }),

	awful.key({ lalt }, "z", function(c)
		if c.width <= 100 or c.height <= 100 then
			return
		end
		if c.floating then
			c:relative_move(0, 0, -1 * screen_width * 0.025, -1 * screen_height * 0.025)
			awful.placement.centered()
		else
			c.floating = true
			c:relative_move(0, 0, screen_width * 0.025, screen_height * 0.025)
			awful.placement.centered()
		end
	end, { description = "Floating resize decrease full", group = "client" }),

	awful.key({ lalt, "Shift" }, "w", function(c)
		if c.floating then
			c:relative_move(0, -1 * screen_height * 0.03, 0, 0)
		else
			awful.client.swap.byidx(-1)
		end
	end, { description = "Floating Move Up", group = "client" }),

	awful.key({ lalt, "Shift" }, "s", function(c)
		if c.floating then
			c:relative_move(0, screen_height * 0.03, 0, 0)
		else
			awful.client.swap.byidx(1)
		end
	end, { description = "Floating Move Down", group = "client" }),

	awful.key({ lalt, "Shift" }, "a", function(c)
		if c.floating then
			c:relative_move(-1 * screen_width * 0.02, 0, 0, 0)
		else
			awful.client.swap.byidx(-1)
		end
	end, { description = "Floating Move Left", group = "client" }),

	awful.key({ lalt, "Shift" }, "d", function(c)
		if c.floating then
			c:relative_move(screen_width * 0.02, 0, 0, 0)
		else
			awful.client.swap.byidx(1)
		end
	end, { description = "Floating Move Right", group = "client" }),

	awful.key({ lalt }, "l", function(c)
		c.fullscreen = not c.fullscreen
		-- c:raise()
	end, { description = "toggle fullscreen", group = "client" }),

	awful.key({ ralt }, "f", function(c)
		c.maximized = not c.maximized
		c:raise()
	end, { description = "(un)maximize", group = "client" })
)

-- Default
for i = 1, 9 do
	-- Bind all keys to tags
	globalkeys = gears.table.join(
		globalkeys,
		-- View tag only.
		awful.key({ lalt }, "#" .. i + 9, function()
			local screen = awful.screen.focused()
			local tag = screen.tags[i]
			if tag then
				tag:view_only()
			end
			if tagswitch then
				local t = awful.screen.focused().selected_tag
				tagswitch.animate(tostring(t.name))
			end
		end, { description = "view tag #" .. i, group = "tag" }),

		-- Move client to tag.
		awful.key({ lalt, "Shift" }, "#" .. i + 9, function()
			if client.focus then
				local tag = client.focus.screen.tags[i]
				if tag then
					client.focus:move_to_tag(tag)
				end
			end
		end, { description = "move focused client to tag #" .. i, group = "tag" })
	)
end

-- local pressed = false

-- client.connect_signal("button::press", function(c, _, _, button)
-- 	if button == 3 then
-- 		pressed = true
-- 	elseif button == 1 and pressed then
-- 		c:kill()
-- 		pressed = false
-- 	elseif button == 1 then
-- 		pressed = false
-- 	end
-- end)

-- Mouse bindings
local clientbuttons = gears.table.join(

	awful.button({ lalt }, 1, function(c)
		if not c.floating then
			float_geometry(c)
		end
		awful.mouse.client.move(c)
	end),

	awful.button({ lalt, "Shift" }, 1, function(c)
		awful.mouse.client.move(c)
	end),

	awful.button({ lalt }, 2, function(c)
		awful.client.floating.set(c, true)
		awful.placement.centered()
	end),

	awful.button({ lalt }, 3, function(c)
		if not c.floating then
			float_geometry(c)
			pressed = false
		end
		awful.mouse.client.resize(c)
		pressed = false
		--[[ awful.placement.centered(c) ]]
	end),

	awful.button({ lalt, "Shift" }, 3, function(c)
		awful.mouse.client.resize(c)
	end),

	awful.button({}, 1, function(c)
		if c.class == "anki-timer" or c.class == "non-focus" or c.class == "ffplay" then
			client.focus = c
			c:raise()
		end
	end),

	awful.button({}, 10, function()
		awful.util.spawn("center-cursor.sh")
	end),

	awful.button({ ralt }, 4, function()
		awful.util.spawn("brightnessctl s +1%")
		awesome.emit_signal("brightness")
	end),

	awful.button({ ralt }, 5, function()
		awful.util.spawn("brightnessctl s 1%-")
		awesome.emit_signal("brightness")
	end),

	awful.button({ lalt }, 8, function()
		awful.client.focus.byidx(1)
		awful.client.focus.raise = true
	end),

	awful.button({ lalt }, 9, function()
		awful.client.focus.byidx(-1)
		awful.client.focus.raise = true
	end),

	awful.button({}, 13, function()
		awful.util.spawn("boomer")
	end)
)

-- Function to open rofi when right-clicking the top-left corner
local function open_rofi_on_corner_click(x, y, button)
    if button == 3 and y > screen_height - 200 and y < screen_height and x < 3 then
        awful.spawn("rofi -show drun")
    end
end

client.connect_signal("button::press", function(c, _, _, button)
    open_rofi_on_corner_click(mouse.coords().x, mouse.coords().y, button)
end)

root.buttons(gears.table.join(
    awful.button({}, 3, function()
        open_rofi_on_corner_click(mouse.coords().x, mouse.coords().y, 3)
    end)
))

-- Set keys
root.keys(globalkeys)

-- Rules
awful.rules.rules = {
	{
		rule = {},
		properties = {
			border_width = beautiful.border_width,
			border_color = beautiful.border_normal,
			focus = awful.client.focus.filter,
			keys = clientkeys,
			buttons = clientbuttons,
			-- screen = awful.screen.preferred,
			placement = awful.placement.centered,
			size_hints_honor = false,
			-- width = screen_width * 0.8,
			-- height = screen_height * 0.8,
			titlebars_enabled = true,
			callback = function(c)
				awful.placement.centered(c)
			end,
		},
	},

	{ rule = { class = "Brave-browser" }, properties = { tag = "2" } },

	{ rule = { class = "Anki" },          properties = { tag = "4" } },

	{ rule = { class = "Virt-manager" },  properties = { tag = "3" } },

	{
		rule = { class = "Inkscape" },
		properties = {
			screen = 1,
			tag = "5",
			-- maximized = false,
			-- floating = false,
		},
	},

	{
		rule = { class = "org.remmina.Remmina" },
		properties = {
			screen = 1,
			tag = "3",
			maximized = false,
			floating = false,
		},
	},

	{
		rule_any = {
			class = {
				"small",
				"Nsxiv",
				"mpv",
				"matplotlib",
			}
		},
		properties = {
			ontop = false,
			floating = true,
			width = screen_width * 0.8,
			height = screen_height * 0.8,
			callback = function(c)
				if awesome.startup then
					c.floating = false
				end
				awful.placement.centered(c)
			end,
		},
	},

	{
		rule = { class = "game" },
		properties = {
			floating = true,
			width = screen_width * 0.3,
			height = screen_height * 0.5,
			callback = function(c)
				if awesome.startup then
					c.floating = false
				end
				awful.placement.centered(c)
			end,
		},
	},

	{
		rule = { class = "non-focus" },
		properties = {
			floating = false,
			focusable = false,
		},
	},

	{
		rule = { class = "Galculator" },
		properties = {
			floating = true,
			width = screen_width * 0.4,
			height = screen_height * 0.5,
			callback = function(c)
				if awesome.startup then
					c.floating = false
				end
				awful.placement.centered(c)
			end,
		},
	},

	{
		rule = { class = "ffplay" },
		properties = {
			floating = true,
			raise = true,
			ontop = true,
			focusable = false,
			width = 300,
			height = 225,
			sticky = true,
			skip_taskbar = true,
			callback = function(c)
				awful.placement.bottom_right(c, { margins = { bottom = 5, right = 5 } })
			end,
		},
	},

	{
		rule = { instance = "mpv_camera" },
		properties = {
			floating = true,
			raise = true,
			ontop = true,
			focusable = false, -- Initially not focusable
			width = 300,
			height = 225,
			sticky = true,
			skip_taskbar = true,
			callback = function(c)
				awful.placement.bottom_right(c, { margins = { bottom = 26, right = 1 } })

				-- Enable focusing only on mouse click
				c:connect_signal("button::press", function()
					c.focusable = true
					client.focus = c
				end)

				-- Disable focusing again when it loses focus
				c:connect_signal("unfocus", function()
					c.focusable = false
				end)
			end,
		},
	},

	{
		rule = { instance = "radar" },
		properties = {
			floating = true,
			focusable = true,
			width = screen_width * 0.37,
			height = screen_height * 0.7,
			callback = function(c)
				awful.placement.centered(c)
			end,
		},
	},

	{
		rule = { class = "weather" },
		properties = {
			floating = true,
			focusable = true,
			width = screen_width * 0.9,
			height = screen_height * 0.9,
			callback = function(c)
				awful.placement.centered(c)
			end,
		},
	},

	{
		rule = { instance = "manim" },
		properties = {
			floating = true,
			focusable = true,
			ontop = true,
			width = screen_width * 0.45,
			height = screen_height * 0.45,
			callback = function(c)
				awful.placement.top_right(c)
			end,
		},
	},

	{
		rule = { class = "fullscreen" },
		properties = {
			fullscreen = true,
		}
	},

	{
		rule = { class = "App" },
		properties = {
			floating = true,
		}
	},

}

-- Signals
client.connect_signal("manage", function(c)
	if not awesome.startup then
		awful.client.setslave(c)
	end
	if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
		awful.placement.no_offscreen(c)
	end
end)

-- Focus follows mouse
client.connect_signal("button::press", function(c)
	c:emit_signal("request::activate", "mouse_enter", { raise = true })
end)
client.connect_signal("focus", function(c)
	c.border_color = beautiful.border_focus
end)
client.connect_signal("unfocus", function(c)
	c.border_color = beautiful.border_normal
end)

client.connect_signal("unmanage", function(c)
    -- Check if the closed window was a dialog
    if c.type == "dialog" then
        -- Focus the parent window or the previously focused window
        local parent = awful.client.focus.history.get(c.screen, -1)
        if parent then
            parent:emit_signal("request::activate", "dialog_closed", {raise = true})
        end
    end
end)

-- -- Add a titlebar if titlebars_enabled is set to true in the rules.
-- client.connect_signal("request::titlebars", function(c)
-- 	local buttons = gears.table.join(
-- 		awful.button({}, 1, function()
-- 			c:emit_signal("request::activate", "titlebar", { raise = true })
-- 			if not c.floating then
-- 				float_geometry(c)
-- 			end
-- 			awful.mouse.client.move(c)
-- 		end),
-- 		awful.button({}, 3, function()
-- 			c:emit_signal("request::activate", "titlebar", { raise = true })
-- 			awful.mouse.client.resize(c)
-- 		end)
-- 	)
--
-- 	awful.titlebar(c, { size = 20 }):setup({
-- 		{ -- Left
-- 			buttons = buttons,
-- 			layout  = wibox.layout.fixed.horizontal,
-- 			-- awful.titlebar.widget.closebutton(c),
-- 		},
-- 		{ -- Middle
-- 			buttons = buttons,
-- 			layout  = wibox.layout.flex.horizontal,
-- 			-- {
-- 			-- 	-- Add the title widget here
-- 			-- 	align = "center",
-- 			-- 	widget = awful.titlebar.widget.titlewidget(c)
-- 			-- },
-- 		},
-- 		{ -- Right
-- 			awful.titlebar.widget.floatingbutton(c),
-- 			awful.titlebar.widget.maximizedbutton(c),
-- 			awful.titlebar.widget.closebutton(c),
-- 			awful.titlebar.widget.stickybutton(c),
-- 			awful.titlebar.widget.ontopbutton(c),
-- 			layout = wibox.layout.align.horizontal
-- 		},
-- 		layout = wibox.layout.align.horizontal
-- 	})
-- end)

-- For restarting in the correct work space and layout
awesome.connect_signal("exit", function(c)
	save_current_tag()
	save_current_layout()
end)

-- When reloading, reset all windows to tiled layout
awesome.connect_signal("exit", function()
    for _, c in ipairs(client.get()) do
        c.floating = false
    end
end)

-- awesome.connect_signal("startup", function()
--     awful.screen.focus(1)
-- end)

awesome.connect_signal("startup", function()
    -- Ensure we focus on the first screen
    local first_screen = screen[1]
    awful.screen.focus(first_screen)

	-- If no client, switch tags to force a refresh
	local current_tag = first_screen.selected_tag
	if current_tag then
		local fallback_tag = first_screen.tags[1]
		if fallback_tag and fallback_tag ~= current_tag then
			fallback_tag:view_only()  -- Switch to another tag
			current_tag:view_only()   -- Switch back
		end
	end
end)

-- Auto start applications
awful.spawn.with_shell("reset_keys.sh", true)
-- awful.spawn.with_shell("sh -c 'killall nm-applet; nm-applet'", true)
-- awful.spawn.with_shell("sh -c 'killall vorta; vorta'", true)
awful.spawn.with_shell("sh -c 'wallpaper.sh main'", true)
-- awful.spawn.with_shell("sh -c 'emacs --daemon'", true)
-- awful.spawn.with_shell("picom", false)
-- awful.spawn.with_shell("sh -c 'killall qbittorrent; qbittorrent'", true)
-- awful.spawn.with_shell("sh -c 'kill all barrier; barrier'", true)
-- awful.spawn.with_shell("sh -c 'killall syncthing; syncthing --no-browser'", true)

-- Correctly restart wm
load_last_active_tag()
if reorder_reload then
	reorder_reload = false
else
	load_last_active_layout()
end
