local gears         = require("gears")
local lain          = require("lain")
local awful         = require("awful")
local wibox         = require("wibox")
local dpi           = require("beautiful.xresources").apply_dpi
local beautiful     = require("beautiful")
local theme_assets  = require("beautiful.theme_assets")
local naughty       = require("naughty")
local cairo         = require("lgi").cairo
local menubar       = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")

local awesome, client, os = awesome, client, os
local my_table            = awful.util.table or gears.table
local Font                = "JetBrainsMono Nerd Font Bold "
local handle              = io.popen("xdpyinfo | grep dimensions | awk '{print $2}'")
local resolution          = handle:read("*a")
local format              = resolution:gsub("[\n\r]", "")
local font_size           = os.getenv("FONT_SIZE")
local taskbar_heights     = 25
if format == "1920x1080" then
	font_size = font_size
else
	font_size = font_size
end

-- colors
local bg1    = os.getenv("BG0")
local bg2    = os.getenv("BG1")
local fg     = os.getenv("WHITE")
local red    = os.getenv("RED")
local blue   = os.getenv("BLUE")
local yellow = os.getenv("YELLOW")
local black  = "#000000"

-- colors for the status bars
local colors = {
	{ "#0000b3", "#ec4c2e", "#ffc700" },
	{ "#00ff00", "#ffff00", "#ff0000" },
	{ os.getenv("GREEN"), os.getenv("YELLOW"), os.getenv("RED") },
	{ os.getenv("GREEN"), os.getenv("ORANGE"), os.getenv("RED") },
	{ os.getenv("BLUE"), os.getenv("YELLOW"), os.getenv("RED") },
}

local bar_color = colors[3]

local theme                              = {}
theme.dir                                = os.getenv("HOME") .. "/.config/awesome/theme/nate"
theme.font                               = Font .. font_size
theme.fg_normal                          = fg
theme.fg_focus                           = fg
theme.bg_normal                          = bg1
theme.bg_focus                           = bg1
theme.fg_urgent                          = red
theme.bg_urgent                          = bg1
theme.border_width                       = os.getenv("BORDER_WIDTH")
theme.border_normal                      = bg2
theme.border_focus                       = blue
theme.taglist_bg_focus                   = bg1
theme.taglist_bg_normal                  = bg1
theme.taglist_fg_focus                   = red
theme.taglist_fg_normal                  = red
theme.taglist_fg_occupied                = yellow
theme.taglist_bg_occupied                = bg1
theme.titlebar_bg_normal                 = bg1
theme.titlebar_bg_focus                  = blue
theme.titlebar_fg_normal                 = bg1
theme.titlebar_fg_focus                  = black
theme.tasklist_bg_normal                 = bg1
theme.tasklist_bg_focus                  = blue
theme.tasklist_fg_focus                  = bg1
theme.tasklist_fg_normal                 = fg1
theme.tasklist_disable_icon              = false
theme.tasklist_plain_task_name           = true
theme.menu_height                        = dpi(16)
theme.menu_width                         = dpi(130)
beautiful.menu_font                      = os.getenv("FONT_NAME") .. " " .. os.getenv("FONT_SIZE")
theme.layout_tile                        = theme.dir .. "/icons/tile.svg"
theme.layout_tileleft                    = theme.dir .. "/icons/tileleft.svg"
theme.layout_tilebottom                  = theme.dir .. "/icons/tilebottom.svg"
theme.layout_tiletop                     = theme.dir .. "/icons/tiletop.svg"
theme.layout_fairv                       = theme.dir .. "/icons/fairv.svg"
theme.layout_fairh                       = theme.dir .. "/icons/fairh.svg"
theme.layout_spiral                      = theme.dir .. "/icons/spiral.svg"
theme.layout_dwindle                     = theme.dir .. "/icons/dwindle.svg"
theme.layout_max                         = theme.dir .. "/icons/max.svg"
theme.layout_fullscreen                  = theme.dir .. "/icons/fullscreen.svg"
theme.layout_magnifier                   = theme.dir .. "/icons/magnifier.svg"
theme.layout_floating                    = theme.dir .. "/icons/floating.svg"

-- theme.titlebar_close_button_focus        = theme.dir .. "/icons/floating.svg"
-- theme.titlebar_close_button_focus_hover  = theme.dir .. "/icons/floating.svg"
-- theme.titlebar_close_button_focus_press  = theme.dir .. "/icons/floating.svg"
-- theme.titlebar_close_button_normal       = theme.dir .. "/icons/floating.svg"
-- theme.titlebar_close_button_normal_hover = theme.dir .. "/icons/floating.svg"
-- theme.titlebar_close_button_normal_press = theme.dir .. "/icons/floating.svg"

-- beautiful.titlebar_floating_button_focus        = theme.dir .. "/icons/floating.svg"
-- beautiful.titlebar_floating_button_focus_hover  = theme.dir .. "/icons/floating.svg"
-- beautiful.titlebar_floating_button_focus_press  = theme.dir .. "/icons/floating.svg"
-- beautiful.titlebar_floating_button_normal       = theme.dir .. "/icons/floating.svg"
-- beautiful.titlebar_floating_button_normal_hover = theme.dir .. "/icons/floating.svg"
-- beautiful.titlebar_floating_button_normal_press = theme.dir .. "/icons/floating.svg"

-- theme.titlebar_maximized_button_focus        = theme.dir .. "/icons/floating.svg"
-- theme.titlebar_maximized_button_focus_hover  = theme.dir .. "/icons/floating.svg"
-- theme.titlebar_maximized_button_focus_press  = theme.dir .. "/icons/floating.svg"
-- theme.titlebar_maximized_button_normal       = theme.dir .. "/icons/floating.svg"
-- theme.titlebar_maximized_button_normal_hover = theme.dir .. "/icons/floating.svg"
-- theme.titlebar_maximized_button_normal_press = theme.dir .. "/icons/floating.svg"

theme.useless_gap                        = 0
awesome.set_preferred_icon_size(32)

-- Tagswitch
theme.bg_tagswitch     = bg1
theme.fg_tagswitch     = blue
theme.border_color     = blue
theme.tagswitch_height = dpi(100)
theme.tagswitch_width  = 2 * theme.tagswitch_height
theme.tagswitch_font   = Font .. font_size * 2
theme.tagswitch_speed  = 1000000
theme.tagswitch_delay  = 0.1

-- Notification design
local nconf = naughty.config
nconf.defaults.margin       = 16
nconf.defaults.text         = "Boo!"
nconf.defaults.timeout      = 2
nconf.padding               = 10
nconf.presets.critical.bg   = red
nconf.presets.critical.fg   = fg
nconf.presets.low.bg        = bg1
nconf.presets.low.fg        = fg
nconf.presets.normal.bg     = bg1
nconf.presets.normal.fg     = fg
nconf.defaults.icon_size    = 64
nconf.spacing               = 8
nconf.defaults.border_width = theme.border_width
nconf.defaults.border_color = blue
nconf.defaults.position     = "bottom_right"
theme.notification_font     = theme.font

-- lain related
theme.layout_centerfair = theme.dir .. "/icons/centerfair.svg"
theme.layout_termfair   = theme.dir .. "/icons/termfair.svg"
theme.layout_centerwork = theme.dir .. "/icons/centerwork.svg"

local markup = lain.util.markup

-- Textclock widget
local day        = wibox.widget.textclock("%a", 1)
local date       = wibox.widget.textclock("%d/%m/%Y", 1)
local clock      = wibox.widget.textclock("%H:%M", 1)
clock.font       = theme.font
clock.fill_space = true
day.font         = theme.font
day.fill_space   = true
date.font        = theme.font
date.fill_space  = true

local length  = 300
local height  = ( -1 / 3 ) * font_size + 8
local timeout = 1

-- Disk bar
disk_bar = wibox.widget({
	forced_height = dpi(8),
	forced_width = dpi(length),
	color = {
		type = "linear",
		from = { 0, 0 },
		to = { length, 0 },
		stops = {
			{ 1,   bar_color[3] },
			{ 0.5, bar_color[2] },
			{ 0,   bar_color[1] },
		},
	},
	background_color = bg2,
	margins = 1,
	paddings = 0,
	ticks = false,
	ticks_size = dpi(6),
	widget = wibox.widget.progressbar,
})

disk_margin = wibox.layout.margin(disk_bar, 1, 1)
disk_margin:set_top(1)
disk_margin:set_bottom(1)
disk_margin.color = fg

local disk_widget_settings = function()
	disk_bar:set_value(fs_now["/"].percentage / 100)
end

disk_widget = lain.widget.fs({
	settings = disk_widget_settings,
	timeout = 60,
})

disk_bar_widget = wibox.container.margin(disk_margin, dpi(height), dpi(height), dpi(height), dpi(height))

-- Volume bar
local volume_bar = wibox.widget({
	forced_height = dpi(1),
	forced_width = dpi(length),
	color = {
		type = "linear",
		from = { 0, 0 },
		to = { length, 0 },
		stops = {
			{ 0,   bar_color[1] },
			{ 0.5, bar_color[2] },
			{ 1,   bar_color[3] },
		},
	},
	background_color = bg2,
	margins = 1,
	paddings = 0,
	ticks = false,
	ticks_size = dpi(6),
	widget = wibox.widget.progressbar,
})

local volume_margin = wibox.layout.margin(volume_bar, 1, 1)
volume_margin:set_top(1)
volume_margin:set_bottom(1)
volume_margin.color = fg

local update_volume_bar = function()
	awful.spawn.easy_async("pamixer --get-volume", function(Volume)
		local volume = tonumber(Volume)
		volume_bar:set_value(volume / 100)
	end)
end

local volume_bar_widget = wibox.container.margin(volume_margin, dpi(height), dpi(height), dpi(height), dpi(height))

local volume_update_timer = gears.timer({
	timeout = 0.001,
	autostart = false,
	call_now = true,
	single_shot = true,
	callback = function()
		update_volume_bar()
	end,
})

awesome.connect_signal("volume", function()
	volume_update_timer:start()
end)

local mute = false
awesome.connect_signal("mute", function()
	if mute then
		volume_bar.background_color = bg2
		update_volume_bar()
		mute = not mute
	else
		volume_bar:set_value(0)
		volume_bar.background_color = os.getenv("RED")
		mute = not mute
	end
end)

local hoffset = length / 2.5

-- Add mouse scroll functionality to change volume
volume_bar_widget:buttons(gears.table.join(
	volume_bar_widget:buttons(),
	awful.button({}, 1, nil, function()
		if mute then
			awful.spawn("pamixer --toggle-mute")
			volume_bar.background_color = bg2
			update_volume_bar()
			mute = false
		else
			awful.spawn("pamixer --toggle-mute")
			volume_bar:set_value(0)
			volume_bar.background_color = os.getenv("RED")
			mute = true
		end
	end),

	awful.button({}, 4, nil, function()
		awful.spawn("pamixer --unmute")
		awful.spawn("pactl set-sink-volume @DEFAULT_SINK@ +1%")
		volume_bar.background_color = bg2
		update_volume_bar()
	end),

	awful.button({}, 5, nil, function()
		awful.spawn("pamixer --unmute")
		awful.spawn("pactl set-sink-volume @DEFAULT_SINK@ -1%")
		volume_bar.background_color = bg2
		update_volume_bar()
	end),

	awful.button({}, 2, nil, function()
		awful.spawn("st -c small -e ncmpcpp")
	end),

	-- awful.button({}, 3, nil, function()
	-- 	awful.spawn("mpc toggle")
	-- 	update_volume_bar()
	-- end),

	awful.button({}, 3, nil, function()
		if mouse.coords().x <= hoffset + length / 3  and mouse.coords().x >= hoffset + 0 * length / 3 and mouse.coords().y >= 0 and mouse.coords().y <= 8 * dpi(height) then
			awful.spawn("mpc prev")
		end
	end),

	awful.button({}, 3, nil, function()
		if mouse.coords().x <= hoffset + 2 * length / 3  and mouse.coords().x >= hoffset + 1 * length / 3 and mouse.coords().y >= 0 and mouse.coords().y <= 8 * dpi(height) then
			awful.spawn("mpc toggle")
		end
	end),

	awful.button({}, 3, nil, function()
		if mouse.coords().x <= hoffset + 3 * length / 3  and mouse.coords().x >= hoffset + 2 * length / 3 and mouse.coords().y >= 0 and mouse.coords().y <= 8 * dpi(height) then
			awful.spawn("mpc next")
		end
	end)

))

-- Other widgets
if false then
	-- Volume bar
	local volume_bar = wibox.widget({
		forced_height = dpi(1),
		forced_width = dpi(length),
		color = {
			type = "linear",
			from = { 0, 0 },
			to = { length, 0 },
			stops = {
				{ 1,   bar_color[1] },
				{ 0.5, bar_color[2] },
				{ 0,   bar_color[3] },
			},
		},
		background_color = bg2,
		margins = 1,
		paddings = 0,
		ticks = false,
		ticks_size = dpi(6),
		widget = wibox.widget.progressbar,
	})

	local volume_margin = wibox.layout.margin(volume_bar, 1, 1)
	volume_margin:set_top(1)
	volume_margin:set_bottom(1)
	volume_margin.color = fg

	local update_volume_bar = function()
		awful.spawn.easy_async("pamixer --get-volume", function(stdout)
			local volume = tonumber(stdout)
			volume_bar:set_value(volume / 100)
		end)
	end

	local volume_bar_widget = wibox.container.margin(volume_margin, dpi(height), dpi(height), dpi(height),
		dpi(height))

	local volume_update_timer = gears.timer({
		timeout = 0.001,
		autostart = false,
		call_now = true,
		single_shot = true,
		callback = function()
			update_volume_bar()
		end,
	})

	awesome.connect_signal("volume", function()
		volume_update_timer:start()
	end)

	-- Brightness bar
	local brightness_bar = wibox.widget({
		forced_height = dpi(1),
		forced_width = dpi(length),
		color = {
			type = "linear",
			from = { 0, 0 },
			to = { length, 0 },
			stops = {
				{ 1,   bar_color[1] },
				{ 0.5, bar_color[2] },
				{ 0,   bar_color[3] },
			},
		},
		background_color = bg2,
		margins = 1,
		paddings = 0,
		ticks = false,
		ticks_size = dpi(6),
		widget = wibox.widget.progressbar,
	})

	local brightness_margin = wibox.layout.margin(brightness_bar, 1, 1)
	brightness_margin:set_top(1)
	brightness_margin:set_bottom(1)
	brightness_margin.color = fg

	local function update_brightness_bar()
		awful.spawn.easy_async("xbacklight -get", function(stdout)
			local brightness = tonumber(stdout)
			brightness_bar:set_value(brightness / 100)
		end)
	end

	local brightness_update_timer = gears.timer({
		timeout = 0.001,
		autostart = false,
		call_now = true,
		single_shot = true,
		callback = function()
			update_brightness_bar()
		end,
	})

	local brightness_bar_widget =
	    wibox.container.margin(brightness_margin, dpi(height), dpi(height), dpi(height), dpi(height))

	awesome.connect_signal("brightness", function()
		brightness_update_timer:start()
	end)

	-- Battery bar
	local battery_bar = wibox.widget({
		forced_height = dpi(1),
		forced_width = dpi(length),
		color = {
			type = "linear",
			from = { 0, 0 },
			to = { length, 0 },
			stops = {
				{ 0,   bar_color[3] },
				{ 0.5, bar_color[2] },
				{ 1,   bar_color[1] },
			},
		},
		background_color = bg2,
		margins = 1,
		paddings = 0,
		ticks = false,
		ticks_size = dpi(6),
		widget = wibox.widget.progressbar,
	})

	battery_margin = wibox.layout.margin(battery_bar, 1, 1)
	battery_margin:set_top(1)
	battery_margin:set_bottom(1)
	battery_margin.color = fg

	local update_battery_bar = function()
		awful.spawn.easy_async("cat /sys/class/power_supply/BAT0/capacity", function(stdout)
			local battery = tonumber(stdout)
			battery_bar:set_value(battery / 100)
		end)
	end

	battery_bar_widget = wibox.container.margin(battery_margin, dpi(height), dpi(height), dpi(height), dpi(height))

	local battery_update_timer = gears.timer({
		timeout = 60,
		autostart = true,
		call_now = true,
		callback = function()
			update_battery_bar()
		end,
	})

	-- CPU bar
	cpu_bar = wibox.widget({
		forced_height = dpi(8),
		forced_width = dpi(length),
		color = {
			type = "linear",
			from = { 0, 0 },
			to = { length, 0 },
			stops = {
				{ 1,   bar_color[1] },
				{ 0.5, bar_color[2] },
				{ 0,   bar_color[3] },
			},
		},
		background_color = bg2,
		margins = 1,
		paddings = 0,
		ticks = false,
		ticks_size = dpi(6),
		widget = wibox.widget.progressbar,
	})

	cpu_margin = wibox.layout.margin(cpu_bar, 1, 1)
	cpu_margin:set_top(1)
	cpu_margin:set_bottom(1)
	cpu_margin.color = fg

	local cpu_widget_settings = function()
		cpu_bar:set_value(cpu_now.usage / 100)
	end

	cpu_widget = lain.widget.cpu({
		settings = cpu_widget_settings,
		timeout = timeout,
	})

	cpu_bar_widget = wibox.widget.background(cpu_margin)

	cpu_bar_widget = wibox.container.margin(cpu_margin, dpi(height), dpi(height), dpi(height), dpi(height))

	-- Memory bar
	mem_bar = wibox.widget({
		forced_height = dpi(8),
		forced_width = dpi(length),
		color = {
			type = "linear",
			from = { 0, 0 },
			to = { length, 0 },
			stops = {
				{ 1,   bar_color[1] },
				{ 0.5, bar_color[2] },
				{ 0,   bar_color[3] },
			},
		},
		background_color = bg2,
		margins = 1,
		paddings = 0,
		ticks = false,
		ticks_size = dpi(6),
		widget = wibox.widget.progressbar,
	})

	mem_margin = wibox.layout.margin(mem_bar, 1, 1)
	mem_margin:set_top(1)
	mem_margin:set_bottom(1)
	mem_margin.color = fg

	local mem_widget_settings = function()
		mem_bar:set_value(mem_now.perc / 100)
	end

	mem_widget = lain.widget.mem({
		settings = mem_widget_settings,
		timeout = timeout,
	})

	mem_bar_widget = wibox.container.margin(mem_margin, dpi(height), dpi(height), dpi(height), dpi(height))
end

-- Fading
if false then
	-- Fading effect for borders without transparency (compositor needed for consistency)
	function fading(col, delay)
		local colors = {}
		local color = {
			"#ff0000", -- Red (1)
			"#ffff00", -- Yellow (2)
			"#00ff00", -- Green (3)
			"#00ffff", -- Cyan (4)
			"#0000ff", -- Blue (5)
			"#ff00ff", -- Magenta (6)
			fg, -- White (7)
		}
		for i = 0, 255 do
			local fading_hex = string.format("%02x", i)
			table.insert(colors, string.format("%s", color[col]:gsub("%a%a", fading_hex)))
		end
		for i = 0, delay do
			local fading_hex = string.format("%02x", 255)
			table.insert(colors, string.format("%s", color[col]:gsub("%a%a", fading_hex)))
		end
		for i = 255, 0, -1 do
			local fading_hex = string.format("%02x", i)
			table.insert(colors, string.format("%s", color[col]:gsub("%a%a", fading_hex)))
		end
		return colors
	end

	-- Fading effect for border with builtin  transparency
	intervals = 250
	function createColor(index, baseColor)
		local red = baseColor[1] - math.floor(baseColor[1] * index / intervals)
		local green = baseColor[2] - math.floor(baseColor[2] * index / intervals)
		local blue = baseColor[3] - math.floor(baseColor[3] * index / intervals)
		local alpha = 0.8

		-- Convert decimal values to hexadecimal
		local hexRed = string.format("%02X", red)
		local hexGreen = string.format("%02X", green)
		local hexBlue = string.format("%02X", blue)
		local hexAlpha = string.format("%02X", alpha * 255)

		return "#" .. hexRed .. hexGreen .. hexBlue .. hexAlpha
	end

	local gradientTable = {}
	local color_table = {
		{ 255, 0,   0 }, -- Red (1)
		{ 255, 255, 0 }, -- Yellow (2)
		{ 0,   255, 0 }, -- Green (3)
		{ 0,   255, 255 }, -- Cyan (4)
		{ 0,   0,   255 }, -- Blue (5)
		{ 255, 0,   255 }, -- Magenta (6)
		{ 255, 255, 255 }, -- White (7)
	}
	local color = 6
	local baseColor = { 0, 0, 255 }

	local delay = 1500

	for i = intervals, 1, -1 do
		table.insert(gradientTable, createColor(i, color_table[color]))
	end
	for _ = 1, delay do
		table.insert(
			gradientTable,
			"#"
			.. string.format("%02x", color_table[color][1])
			.. string.format("%02x", color_table[color][2])
			.. string.format("%02x", color_table[color][3])
		)
	end
	for i = 1, intervals do
		table.insert(gradientTable, createColor(i, color_table[color]))
	end

	-- Turn on fading
	local method = "builtin"

	if method == "compositor" then
		border_focus_colors = fading(6, 750)
	elseif method == "builtin" then
		border_focus_colors = gradientTable
	end

	-- Counter to keep track of the current color index
	local color_index = 1

	-- Function to update the border color for a client
	local function update_border_color(c)
		c.border_color = border_focus_colors[color_index]
	end

	-- Connect the signal to update the border color on focus
	client.connect_signal("focus", function(c)
		update_border_color(c)

		-- Use a timer to continuously cycle through colors
		local color_timer = gears.timer({
			timeout = 0.001,
			autostart = true,
			call_now = false,
			callback = function()
				color_index = color_index % #border_focus_colors + 1
				update_border_color(c)
			end,
		})

		-- Start the timer when the client loses focus
		c:connect_signal("unfocus", function()
			color_timer:stop()
		end)

		-- Stop the timer when the client is closed
		c:connect_signal("unmanage", function()
			color_timer:stop()
		end)
	end)
end

-- Check for system updates
local pacupdates = awful.widget.watch('bash -c "/usr/bin/checkupdates | wc -l"', 600, function(widget, stdout) 
    widget:set_markup(markup.fontfg(theme.font, fg, "󰏗 " .. stdout))
end)

-- Keyboard layout widget
local keyboard_layout_widget = wibox.widget.textbox()
keyboard_layout_widget.font = theme.font

-- Function to update the keyboard layout widget
local function update_keyboard_layout_widget()
    local handle = io.popen("xkblayout-state print '%s'")
    local current_layout = handle:read("*a")
    handle:close()

    -- Trim trailing whitespace
    current_layout = current_layout:gsub("^%s*(.-)%s*$", "%1")

    if current_layout == "hr" then
        keyboard_layout_widget:set_markup_silently("HR")
    elseif current_layout == "us" then
        keyboard_layout_widget:set_markup_silently("US")
    end
end

local layout = "us"

-- Update the widget initially
update_keyboard_layout_widget()

-- Function to toggle keyboard layout
local function toggle_keyboard_layout()
	if layout == "hr" then
		awful.spawn("setxkbmap -layout us")
		awesome.emit_signal("keyboard_layout_changed")
		layout = "us"
	elseif layout == "us" then
		awful.spawn("setxkbmap -layout hr")
		awesome.emit_signal("keyboard_layout_changed")
		layout = "hr"
	end
end

-- Add a mouse click event to toggle the layout
keyboard_layout_widget:buttons(
    awful.util.table.join(
    awful.button({}, 1, function()
		toggle_keyboard_layout()
		awesome.emit_signal("keyboard_layout_changed")
	end)
    )
)

-- Signal to change layout
awesome.connect_signal("keyboard_layout_changed", function()
	update_keyboard_layout_widget()
end)

-- Make tabs clickable
local taglist_buttons = gears.table.join(awful.button({}, 1, function(t)
	-- Tag popup effect
	t:view_only()
	-- local Effects = require("effects")
	-- local tagswitch = Effects.request_effect("tagswitch")
	-- if tagswitch then
	-- 	local t = awful.screen.focused().selected_tag
	-- 	tagswitch.animate(tostring(t.name))
	-- end
end))

-- Make tasks clickable
local tasklist_buttons = gears.table.join(awful.button({}, 1, function(c)
	if c == client.focus then
		c.focus = true
	else
		c:emit_signal("request::activate", "tasklist", { raise = true })
	end
end))

-- Separators
local sep = wibox.widget.textbox(" ")
local big_sep =
    wibox.widget.textbox("                                                                                        ")
local empty_middle_widget = wibox.widget.textbox("")

function theme.at_screen_connect(s)
	-- Tags
	awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])

	-- if s.index == 1 then
	-- 	awful.tag(awful.util.tagnames, s, awful.layout.layouts[layout_index])
	-- elseif s.index == 2 then
	-- 	awful.tag(awful.util.tagnames, s, awful.layout.layouts[layout_index])
	-- end

	-- We need one layoutbox per screen.
	s.mylayoutbox = awful.widget.layoutbox(s)

	-- Create a taglist widget
	s.mytaglist = awful.widget.taglist({
		screen = s,
		filter = awful.widget.taglist.filter.all,
		buttons = taglist_buttons,
	})

	-- Timer setup for taskbar items (100ms)
	local tasklist_click_timer = gears.timer({ timeout = 0.1 })
	tasklist_click_timer:connect_signal("timeout", function()
		tasklist_click_timer:stop()
	end)

	-- Create a tasklist widget
	s.mytasklist = awful.widget.tasklist({
		screen = s,
		filter = awful.widget.tasklist.filter.currenttags,

		style = {
			align = "center",
		},

		layout = {
			layout  = wibox.layout.flex.horizontal
		},

		widget_template = {
			{
				nil,
				{
					{
						{ widget = wibox.widget.imagebox, id = "icon_role" },
						id     = "icon_margin_role",
						left   = 0,
						widget = wibox.container.margin
					},
					{
						{
							id     = "text_role",
							widget = wibox.widget.textbox,
						},
						id     = "text_margin_role",
						left   = 10,
						right  = 0,
						widget = wibox.container.margin
					},
					-- fill_space = true,
					layout     = wibox.layout.fixed.horizontal
				},
				layout = wibox.layout.align.horizontal,
				expand = "outside",
			},
			id     = "background_role",
			widget = wibox.container.background
		},

		buttons = gears.table.join(
			awful.button({}, 1, function(c)
				c:raise()
				client.focus = c
			end),
			-- awful.button({}, 1, function(c)
			-- 	if c.minimized then
			-- 		c.minimized = false
			-- 		c:raise()
			-- 	else
			-- 		c.minimized = true
			-- 	end
			-- end),
			awful.button({}, 3, function(c)
				c:kill()
			end)
		)
	})

	-- Function to show or hide the tasklist based on the layout
	local function toggle_tasklist_visibility(s)
		-- Replace "max" with the name of your maximized layout
		local maximized_layout = "max"

		-- Get the current layout name
		local current_layout = awful.layout.getname(awful.layout.get(s))

		-- Show or hide the tasklist based on the layout
		if current_layout == maximized_layout then
			s.mytasklist.visible = true
		else
			s.mytasklist.visible = false
		end
	end

	-- Connect the function to the layout change signal
	awful.tag.attached_connect_signal(s, "property::layout", function()
		toggle_tasklist_visibility(s)
	end)

	-- Update the tasklist visibility initially
	toggle_tasklist_visibility(s)


	s.topwibox = awful.wibar({
		position = "top",
		screen = s,
		height = dpi(taskbar_heights),
		bg = theme.bg_normal,
		fg = theme.fg_normal,
		border_width = 0,
		ontop = false,
	})

	-- Create the bottom wibox
	s.bottomwibox = awful.wibar({
		position = "bottom",
		screen = s,
		border_width = 0,
		height = dpi(taskbar_heights),
		bg = theme.bg_normal,
		fg = theme.fg_normal,
	})

	-- Add widgets to the bottom wibox
	s.bottomwibox:setup({
		layout = wibox.layout.flex.horizontal,
		s.mytasklist,
	})

	-- Show the number of hidden windows
	local hidden_windows_count = 0

	local hidden_windows = wibox.widget({
		align = "center",
		valign = "center",
		widget = wibox.widget.textbox,
		font = Font .. font_size,
	})
	hidden_windows:set_align("right")

	local function update_hidden_windows_count()
		hidden_windows_count = 0
		for _, c in ipairs(client.get()) do
			if c.minimized then
				hidden_windows_count = hidden_windows_count + 1
			end
		end
		if hidden_windows_count == 0 then
			hidden_windows:set_text("")
		else
			hidden_windows:set_text("HIDDEN: " .. hidden_windows_count)
		end
	end

	client.connect_signal("property::minimized", update_hidden_windows_count)
	client.connect_signal("unmanage", update_hidden_windows_count)

	update_hidden_windows_count()

	-- local hidden_windows = awful.widget.tasklist({
	-- 	screen = s,
	-- 	filter = awful.widget.tasklist.filter.currenttags,
	-- 	buttons = tasklist_buttons,
	-- 	style = {
	-- 		fg_normal = "#000000",
	-- 		bg_normal = "#000000",
	-- 		fg_focus = "#000000",
	-- 		bg_focus = "#000000",
	-- 		fg_minimize = "#000000",
	-- 		bg_minimize = "#ff00ff",
	-- 		shape = gears.shape.rectangle,
	-- 		align = "center",
	-- 	},
	-- })

	local systray = wibox.widget.systray()
	beautiful.systray_icon_spacing = 10
	systray:set_base_size(22)

	-- Add widgets to the wibox
	s.topwibox:setup({
		layout = wibox.layout.align.horizontal,
		expand = "inside",
		{
			layout = wibox.layout.flex.horizontal,
			s.mytaglist,
		},
		{
			layout = wibox.layout.fixed.horizontal,
			volume_bar_widget,
			disk_bar_widget,
			sep,
			hidden_windows,
		},
		{
			layout = wibox.layout.fixed.horizontal,
			systray,
			sep,
			sep,
			sep,
			sep,
			sep,
			sep,
			sep,
			pacupdates,
			sep,
			sep,
			keyboard_layout_widget,
			sep,
			sep,
			day,
			sep,
			sep,
			date,
			sep,
			sep,
			clock,
			sep,
			sep,
		},
	})

	-- Hide wibox
	hide = function()
		s.topwibox.visible = not s.topwibox.visible
	end

	-- Function to toggle the bottom wibox (taskbar) visibility based on layout
	local function toggle_bottomwibox_visibility(s)
		local maximized_layout = "max"
		local current_layout = awful.layout.getname(awful.layout.get(s))
		if current_layout == maximized_layout then
			s.bottomwibox.visible = true
		else
			s.bottomwibox.visible = false
		end
	end

	-- Connect the function to the layout change signal
	awful.tag.attached_connect_signal(s, "property::layout", function()
		toggle_bottomwibox_visibility(s)
	end)

	-- Update the bottom wibox visibility initially
	toggle_bottomwibox_visibility(s)
	-- s.bottomwibox.visible = true

end

return theme
