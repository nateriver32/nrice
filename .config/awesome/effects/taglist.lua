local beautiful = require("beautiful")

-- DPI
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local M = wibox({
	bg = beautiful.bg_tagswitch or beautiful.bg_normal,
	fg = beautiful.fg_tagswitch or beautiful.fg_normal,
	height = beautiful.tagswitch_height or dpi(90),
	width = beautiful.tagswitch_width or dpi(180),
})

M:setup({
	{
		id = "text",
		font = beautiful.tagswitch_font or beautiful.font,
	},
})

M.animate = function(text)
	M.changeText(text)
	fade(M, beautiful.tagswitch_speed or 200, beautiful.tagswitch_delay or 0.25)
end
