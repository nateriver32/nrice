#!/usr/bin/env bash
set -e

# file=$(find ~ -type f \
#   -not -path "*/.git*" \
#   -not -path "*/.gnupg*" \
#   -not -path "*/.rustup*" \
#   -not -path "*/.local/state*" \
#   -not -path "*/.cache*" \
#   -not -path "*/.librewolf*" \
#   -not -path "*/.cargo*" \
#   -print | fzf --prompt="FILE  " \
#   --preview-window=top:50% \
#   --preview 'bat --theme=base16 --style=numbers --color=always --line-range :500 {}' \
#   --bind J:preview-down,K:preview-up,U:preview-half-page-up,D:preview-half-page-down \
#   --pointer=" " \
#   --marker=" " \
#   --margin=0,0,0,0 \
#   --no-separator \
#   --no-scrollbar \
#   --ellipsis="..." \
#   --info=inline-right \
#   --multi \
#   --color=fg:$WHITE,hl:$RED \
#   --color=fg+:$RED,bg+:$BG0,hl+:$RED \
#   --color=info:$WHITE,prompt:$WHITE,pointer:$RED \
#   --color=marker:$RED,spinner:$RED,header:$WHITE)

file=$(find ~ -type f \
  -not -path "*/.git*" \
  -not -path "*/.gnupg*" \
  -not -path "*/.rustup*" \
  -not -path "*/.local/state*" \
  -not -path "*/.cache*" \
  -not -path "*/.librewolf*" \
  -not -path "*/.cargo*" \
  -print | fzf --prompt="FILE  " \
  --bind change:top \
  --pointer=" " \
  --marker=" " \
  --margin=0,0,0,0 \
  --no-separator \
  --no-scrollbar \
  --ellipsis="..." \
  --info=inline-right \
  --multi \
  --preview-window=top:75% \
  --preview 'bat --theme=base16 --style=numbers --color=always --line-range :500 {}' \
  --color=fg:$WHITE,hl:$RED \
  --color=fg+:$RED,bg+:$BG0,hl+:$RED \
  --color=info:$WHITE,prompt:$WHITE,pointer:$RED \
  --color=marker:$RED,spinner:$RED,header:$WHITE)

[ -z "$file" ] && return 1

filetype=$(file --brief --mime-type "$file")

case ${file##*.} in
  tex|py|ts|sh|lua|txt|h|mk|rasi|log|toml|status|nim|sub|yml|guess|anki|json|c|cpp|h|bib|conf|html|css|csv)
    cd "$(dirname "$file")"
    nvim "$file"
    exec $SHELL
    ;;
  org|el)
    nohup emacs "$file" > /dev/null 2>&1 &
    disown
    # Emacs "$file"
    ;;
  *)
    if [[ "$filetype" == "text/plain" ]]; then
      nvim "$file"
    else
      nohup xdg-open "$file" > /dev/null 2>&1 &
      disown
    fi
    ;;
esac
