#!/usr/bin/env bash
set -e

mpv --no-osc --no-osd-bar --video-unscaled=yes --x11-name=radar --loop=inf https://vrijeme.hr/anim_kompozit.gif
