#!/usr/bin/env bash
set -e

f_cache=$HOME/.file_cache

# Display the cached files using dmenu
path=$(cat $f_cache | dmenu -l 25 -p 'OPEN FILE ')

# Check if the selected path is empty
if [ -z "$path" ]; then
    exit 1
fi

cd "${path%/*}"

# Open file
case ${path##*.} in
  tex|py|ts|sh|lua|el|csv)
    st -e sh -c "nvim $path; exec $SHELL";;
  org)
    Emacs "$path";;
  *)
    xdg-open "$path";;
esac
