#!/usr/bin/env bash
set -e

time=$1

st -c anki-timer -f "CMU Sans Serif:style=bold:pixelsize=180" -e termdown -b --no-seconds --no-figlet $time
