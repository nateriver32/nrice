#!/usr/bin/env bash
set -e

dm=~/Programs/dmenu-flexipatch/config.h
stt=~/Programs/st-flexipatch/config.h
dwm=~/Programs/dwm-flexipatch/config.h
ro=$XDG_CONFIG_HOME/rofi/config.rasi
gtk=$XDG_CONFIG_HOME/gtk-3.0/settings.ini
qt5=$XDG_CONFIG_HOME/qt5ct/qt5ct.conf
qt6=$XDG_CONFIG_HOME/qt6ct/qt6ct.conf
awe=$XDG_CONFIG_HOME/awesome/theme/nate/theme.lua
flame=$XDG_CONFIG_HOME/flameshot/flameshot.ini
# dwm=$XDG_CONFIG_HOME/chadwm/chadwm/themes/ntheme.h
# dwm_bar=$XDG_CONFIG_HOME/chadwm/scripts/bar_themes/ntheme
# dwm_build=$XDG_CONFIG_HOME/chadwm/chadwm

xrdb ~/.Xresources
source $XDG_CONFIG_HOME/colors_and_fonts.sh
sed -i "/gtk-font-name/s/=.*/=$FONT_NAME $FONT_SIZE/" $gtk
sed -i "/gtk-theme-name/s/=.*/=Breeze-Dark/" $gtk
sed -i "/^style/s/=.*/=Breeze/" $qt5
# sed -i "/^fixed/s/=.*/=\"$FONT_NAME,$FONT_SIZE,-1,5,57,0,0,0,0,0\"/" $qt5
# sed -i "/^general/s/=.*/=\"$FONT_NAME,$FONT_SIZE,-1,5,57,0,0,0,0,0\"/" $qt5
sed -i "/^style/s/=.*/=Breeze/" $qt6
# sed -i "/^fixed/s/=.*/=\"$FONT_NAME,$FONT_SIZE,-1,5,57,0,0,0,0,0\"/" $qt6
# sed -i "/^general/s/=.*/=\"$FONT_NAME,$FONT_SIZE,-1,5,57,0,0,0,0,0\"/" $qt6
sed -i "/  fg: /s/:.*/: $RED;/" $ro
sed -i "/  border-color: /s/:.*/: $BLUE;/" $ro
sed -i "/  bg: /s/:.*/: $BG0;/" $ro
sed -i "/  bg-alt: /s/:.*/: $BG0;/" $ro
sed -i "/  bg-selected: /s/:.*/: $BG0;/" $ro
sed -i "/  fg-alt: /s/:.*/: $WHITE;/" $ro
sed -i "/  font: /s/:.*/: \"$FONT_NAME $FONT_STYLE $FONT_SIZE\";/" $ro
sed -i "/local Font/s/=.*/= \"$FONT_NAME $FONT_STYLE \"/" $awe
sed -i "/256 -> cursor/s/    \"[^\"]*\"/    \"$YELLOW\"/" $stt
sed -i "/257 -> rev cursor/s/    \"[^\"]*\"/    \"$YELLOW\"/" $stt
sed -i "/savePath/s/=.*/=\/home\/$(whoami)\/Pictures/" "$flame"

sed -i "/const char black/s/\"[^\"]*\"/\"$BG0\"/" $dm
sed -i "/const char red/s/\"[^\"]*\"/\"$RED\"/" $dm
sed -i "/const char selection/s/\"[^\"]*\"/\"$SELECTION\"/" $dm
sed -i "/const char cursearch/s/\"[^\"]*\"/\"$YELLOW\"/" $dm
sed -i "/const char green/s/\"[^\"]*\"/\"$GREEN\"/" $dm
sed -i "/const char yellow/s/\"[^\"]*\"/\"$YELLOW\"/" $dm
sed -i "/const char magenta/s/\"[^\"]*\"/\"$MAGENTA\"/" $dm
sed -i "/const char blue/s/\"[^\"]*\"/\"$BLUE\"/" $dm
sed -i "/const char purple/s/\"[^\"]*\"/\"$PURPLE\"/" $dm
sed -i "/const char cyan/s/\"[^\"]*\"/\"$CYAN\"/" $dm
sed -i "/const char white/s/\"[^\"]*\"/\"$WHITE\"/" $dm
sed -i "/const char border/s/\"[^\"]*\"/\"$BLUE\"/" $dm
sed -i "/static unsigned int border_width/s/=.*/= $BORDER_WIDTH;/" $dm

# sed -i "/const char black/s/\"[^\"]*\"/\"$BG0\"/" $dwm
# sed -i "/const char gray2/s/\"[^\"]*\"/\"$GRAY4\"/" $dwm
# sed -i "/const char gray3/s/\"[^\"]*\"/\"$GRAY3\"/" $dwm
# sed -i "/const char gray4/s/\"[^\"]*\"/\"$GRAY4\"/" $dwm
# sed -i "/const char red/s/\"[^\"]*\"/\"$RED\"/" $dwm
# sed -i "/const char green/s/\"[^\"]*\"/\"$GREEN\"/" $dwm
# sed -i "/const char yellow/s/\"[^\"]*\"/\"$YELLOW\"/" $dwm
# sed -i "/const char pink/s/\"[^\"]*\"/\"$MAGENTA\"/" $dwm
# sed -i "/const char orange/s/\"[^\"]*\"/\"$ORANGE\"/" $dwm
# sed -i "/const char blue/s/\"[^\"]*\"/\"$BLUE\"/" $dwm
# sed -i "/const char white/s/\"[^\"]*\"/\"$WHITE\"/" $dwm
# sed -i "/const char col_borderbar/s/\"[^\"]*\"/\"$BG0\"/" $dwm

# sed -i "/black/s/#.*/$BG0/" $dwm_bar
# sed -i "/gray2/s/#.*/$GRAY/" $dwm_bar
# sed -i "/gray3/s/#.*/$GRAY3/" $dwm_bar
# sed -i "/gray4/s/#.*/$GRAY4/" $dwm_bar
# sed -i "/red/s/#.*/$RED/" $dwm_bar
# sed -i "/green/s/#.*/$GREEN/" $dwm_bar
# sed -i "/yellow/s/#.*/$YELLOW/" $dwm_bar
# sed -i "/pink/s/#.*/$MAGENTA/" $dwm_bar
# sed -i "/orange/s/#.*/$ORANGE/" $dwm_bar
# sed -i "/blue/s/#.*/$BLUE/" $dwm_bar
# sed -i "/white/s/#.*/$WHITE/" $dwm_bar

xrdb ~/.Xresources

cd ${dm%/*}; sudo make clean install > /dev/null 2>&1
cd ${stt%/*}; sudo make clean install > /dev/null 2>&1
# cd $dwm_build; sudo make clean install > /dev/null 2>&1
# loginctl terminate-user $USER
# awesome-client 'awesome.restart()' > /dev/null 2>&1
