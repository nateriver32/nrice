#!/usr/bin/env bash
set -e

password=$(dmenu -P -p "PASSWORD " -nf "$BG0" -nb "$RED")

ssh node1 "echo $password | sudo -S shutdown now" 2>&1 /dev/null &
sleep 0.1
ssh node2 "echo $password | sudo -S shutdown now" 2>&1 /dev/null &
sleep 0.1
ssh node3 "echo $password | sudo -S shutdown now" 2>&1 /dev/null &
sleep 0.1
ssh node4 "echo $password | sudo -S shutdown now" 2>&1 /dev/null &
sleep 0.1
ssh node5 "echo $password | sudo -S shutdown now" 2>&1 /dev/null &
sleep 0.1
ssh node6 "echo $password | sudo -S shutdown now" 2>&1 /dev/null &
sleep 0.1
ssh node7 "echo $password | sudo -S shutdown now" 2>&1 /dev/null &
