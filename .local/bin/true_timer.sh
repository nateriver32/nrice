#!/usr/bin/env bash
set -e

# i3-msg floating enable, resize set 100px 1000px, move position 1910 30
# termdown -b $1

time=$(dmenu -p "TIMER ")

if [ "$time" = "" ]; then
  exit 1
fi

st -f "CMU Sans Serif:style=bold:pixelsize=180" -e termdown -b --no-figlet $time
