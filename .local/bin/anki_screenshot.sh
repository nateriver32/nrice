#!/usr/bin/env bash
set -e

name=$(date +%F-%T | sed 's/:/-/g').png
dir="$HOME/.local/share/Anki2/User_1/collection.media/$name"

width=$(xdpyinfo | grep dimensions | awk '{print $2}'| awk -F"x" '{print $1}')
height=$(xdpyinfo | grep dimensions | awk '{print $2}'| awk -F"x" '{print $2}')

# flameshot gui --region $(($width/2 + 124))x$(($height/2))+$(($width/4 - 62))+$(($height/4)) -p $dir
flameshot gui --region $(($width/2 + 124))x$(($height/2))+$(($width/4 - 62))+$(($height/4)) -p $dir || exit 1
echo $name | tr -d '\n' | xclip -selection clipboard

# awesome-client '
# local awful = require("awful")
# if awful.client.focus.class ~= "St" then
#   awful.client.focus.byidx(0)
# end
# '

# sleep 0.01
# xdotool type "img"
