#!/usr/bin/env bash
set -e

brave https://10.42.0.77:8006
brave https://10.42.0.31:8006
brave https://10.42.0.159:8006
brave https://10.42.0.200:8006
