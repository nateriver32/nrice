#!/usr/bin/env bash

video_dir=~/Videos/Recordings/
video_name=out.mkv
louder_video_name=louder.mkv
amplification="10"

selection=$(printf "RECORD AND AMPLIFY\nRECORD\nAMPLIFY" | dmenu -p "SELECT ")

[ -z $selection ] && exit 1

case $selection in
	"RECORD AND AMPLIFY")
		ffmpeg -y -f x11grab -r 30 -s 1920x1080 -i :0.0 -f pulse -i default \
			-filter_complex "[1:a]volume=$amplification dB[aout]" \
			-map 0:v -map "[aout]" \
			-vcodec libx264 -preset ultrafast -crf 18 -acodec aac $video_dir/$video_name;;
	"RECORD")
		ffmpeg -y -f x11grab -r 30 -s 1920x1080 -i :0.0 -f pulse -i default $video_dir/$video_name;;
	"AMPLIFY")
		ffmpeg -y -i $video_dir/$video_name -vcodec copy -af "volume=$amplification dB" $video_dir/$louder_video_name;;
esac
