#!/usr/bin/env bash
set -e

# path=$(find $HOME -type d -not -path "*/.git*" | fzf --prompt="DIRECTORY  ")
#
# if [ -z $path ]; then
#   exit 0
# fi
#
# cd $path; exec $SHELL

path=$(find ~ -type d \
  -not -path "*/.git*" \
  -not -path "*/.gnupg*" \
  -not -path "*/.rustup*" \
  -not -path "*/.local/state*" \
  -not -path "*/.cache*" \
  -not -path "*/.librewolf*" \
  -not -path "*/.cargo*" \
  -print | fzf --prompt="DIRECTORY  "\
  --bind change:top \
  --pointer=" " \
  --marker=" " \
  --margin=0,0,0,0 \
  --no-separator \
  --no-scrollbar \
  --ellipsis="..." \
  --info=inline-right \
  --preview-window=top:75% \
  --preview="tree -C {}" \
  --multi \
  --color=fg:$WHITE,hl:$RED \
  --color=fg+:$RED,bg+:$BG0,hl+:$RED \
  --color=info:$WHITE,prompt:$WHITE,pointer:$RED \
  --color=marker:$RED,spinner:$RED,header:$WHITE)

# if [ ! -z "$path" ]; then
#   cd "$path"
#   exec $SHELL
# fi

if [ ! -z "$path" ] && ! pgrep tmux; then
  cd "$path"
  nohup st -e bash -i -c 'tmux new-session -A -s MASTER' &
  disown
fi

if [ ! -z "$path" ]; then
  cd "$path"
  tmux neww
  exec $SHELL
fi
