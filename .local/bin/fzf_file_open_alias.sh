#!/usr/bin/env bash
set -e

# file=$(find ~ -type f \
#   -not -path "*/.git*" \
#   -not -path "*/.gnupg*" \
#   -not -path "*/.rustup*" \
#   -not -path "*/.local/state*" \
#   -not -path "*/.cache*" \
#   -not -path "*/.librewolf*" \
#   -not -path "*/.cargo*" \
#   -print | fzf --prompt="FILE  " \
#   --preview-window=top:50% \
#   --preview 'bat --theme=base16 --style=numbers --color=always --line-range :500 {}')

file=$(find ~ -type f \
  -not -path "*/.git*" \
  -not -path "*/.gnupg*" \
  -not -path "*/.rustup*" \
  -not -path "*/.local/state*" \
  -not -path "*/.cache*" \
  -not -path "*/.librewolf*" \
  -not -path "*/.cargo*" \
  -print | fzf --prompt="FILE  ")

export file

[ -z "$file" ] && return 1

filetype=$(file --brief --mime-type "$file")

case ${file##*.} in
  tex|py|ts|sh|lua|txt|h|mk|rasi|log|toml|status|nim|sub|yml|guess|anki|json|c|cpp|h|bib)
    cd "$(dirname "$file")"
    nvim "$file"
    exec $SHELL
    ;;
  org|el)
    nohup emacs "$file" > /dev/null 2>&1 &
    disown
    # Emacs "$file"
    ;;
  *)
    if [[ "$filetype" == "text/plain" ]]; then
      nvim "$file"
    else
      nohup xdg-open "$file" > /dev/null 2>&1 &
      disown
    fi
    ;;
esac
