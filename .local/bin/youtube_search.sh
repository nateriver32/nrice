#!/usr/bin/env bash
set -e

query=$(echo | dmenu -p "YOUTUBE SEARCH " -l 0)

if [ -z "$query" ]; then
  exit 1
fi

modified_query=$(echo "$query" | sed -e 's/ /+/g' -l 1)
echo $modified_query

url="https://www.youtube.com/results?search_query=$modified_query"
brave "$url"
