#!/usr/bin/env bash
set -e

choice=$(printf "MOUNT\nUNMOUNT" | dmenu -p "CHOOSE ")
mount_dir="/mnt/usb"

[ -z ${choice} ] && exit 1 

case $choice in
  MOUNT)
    sync
    part=$(lsblk | grep -i "part" | grep -ie " $" | sed "s/└─//g" | sed "s/├─//g" | awk '{print $1" "$4}' | dmenu -p "MOUNT " | cut -d" " -f1);
    [ -z ${part} ] && exit 1;
    password=$(dmenu -P -p "PASSWORD " -nf "$BG0" -nb "$RED");
    echo $password | sudo -S mount -o uid=1000,gid=1000 /dev/${part} $mount_dir;
    notification.sh Mounted;
    cd $mount_dir;
    st -c small;;
  UNMOUNT)
    sync
    part=$(lsblk | grep -i "part" | grep -v " $" | grep -v "sda" | sed "s/└─//g" | sed "s/├─//g" | awk '{print $1" "$4}' | dmenu -p "UNMOUNT " | cut -d" " -f1);
    [ -z ${part} ] && exit 1;
    password=$(dmenu -P -p "PASSWORD " -nf "$BG0" -nb "$RED");
    echo $password | sudo -S umount -l /dev/${part};
    notification.sh Unmounted;;
    *)
    exit 1;;
esac
