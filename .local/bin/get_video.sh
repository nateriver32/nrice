#!/usr/bin/env bash
set -e

sleep 1

# xdotool key "Ctrl+l"
xdotool type "yy"
# xdotool key "Ctrl+c"
xdotool key Escape

sleep 1

echo $(xsel -o) | xargs mpv
