#!/usr/bin/env bash
set -e

files_dir="~/Documents/"
choice=$(printf "NOTES\nTIPS" | dmenu -p "CHOOSE ")

[ -z ${choice} ] && exit 1 

case $choice in
  NOTES)
    st -c small sh -c "nvim $files_dir/notes.md";;
  TIPS)
    st -c small sh -c "nvim $files_dir/tips.md";;
    *)
    exit 1;;
esac
