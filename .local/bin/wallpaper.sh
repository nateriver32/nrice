#!/usr/bin/env bash
set -e

wallpaper_dir="$HOME/.local/share/wallpapers"
wallpapers=($wallpaper_dir/*)
current_wallpaper_file="$HOME/.current_wallpaper"
main_wallpaper="$HOME/.local/share/black.png"

# Get the index of the current wallpaper
if [[ -f "$current_wallpaper_file" ]]; then
  current_index=$(cat "$current_wallpaper_file")
else
  touch ~/.curent_wallpaper
  current_index=0
fi

# Calculate the next wallpaper index
next_index=$(( (current_index + 1) % ${#wallpapers[@]} ))

monitors=($(xrandr --listmonitors | awk '{if (NR>1) print $4}'))

# Set the next wallpaper for each monitor
if [ -z "$1" ]; then
  for ((i=0; i<${#monitors[@]}; i++)); do
    wallpaper="${wallpapers[next_index]}"
    nitrogen --head=$i --set-zoom-fill "$wallpaper" 2> /dev/null
  done
  echo "$next_index" > "$current_wallpaper_file"
elif [ "$1" == "main" ]; then
  for ((i=0; i<${#monitors[@]}; i++)); do
    nitrogen --head=$i --set-zoom-fill "$main_wallpaper" 2> /dev/null
  done
else
  for ((i=0; i<${#monitors[@]}; i++)); do
    wallpaper="${wallpaper_dir}/"$1".png"
    nitrogen --head=$i --set-zoom-fill "$wallpaper" 2> /dev/null
  done
fi
