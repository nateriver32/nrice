#!/usr/bin/env bash
set -e

if [ $# -eq 0 ]; then "${SHELL:-sh}"; else "$@"; fi
stty -icanon; dd ibs=1 count=1 >/dev/null 2>&1
