#!/usr/bin/env bash
set -e

file=$(find ~ -type f \
  -not -path "*/.git*" \
  -not -path "*/.gnupg*" \
  -not -path "*/.rustup*" \
  -not -path "*/.local/state*" \
  -not -path "*/.cache*" \
  -not -path "*/.librewolf*" \
  -not -path "*/.cargo*" \
  -print | fzf --prompt="FILE  " \
  --bind change:top \
  --pointer=" " \
  --marker=" " \
  --margin=0,0,0,0 \
  --no-separator \
  --no-scrollbar \
  --ellipsis="..." \
  --info=inline-right \
  --multi \
  --preview-window=top:75% \
  --preview 'bat --theme=base16 --style=numbers --color=always --line-range :500 {}' \
  --color=fg:$WHITE,hl:$RED \
  --color=fg+:$RED,bg+:$BG0,hl+:$RED \
  --color=info:$WHITE,prompt:$WHITE,pointer:$RED \
  --color=marker:$RED,spinner:$RED,header:$WHITE)

if [ -z $file ]; then
  exit 0
fi

cd ${file%/*}; nvim ${file}; exec $SHELL
