#!/usr/bin/env bash
set -e

current_zoom=$(v4l2-ctl -d /dev/video0 -l | awk -F= '/zoom_absolute/{print $NF}' | tr -d ' ')
default_zoom=200

if v4l2-ctl -d /dev/video0 -l > /dev/null 2>&1; then
	case $1 in
		"+")
			v4l2-ctl -d /dev/video0 -c zoom_absolute=$((current_zoom + 1))  > /dev/null 2>&1;;
		"-")
			v4l2-ctl -d /dev/video0 -c zoom_absolute=$((current_zoom - 1))  > /dev/null 2>&1;;
		"=")
			v4l2-ctl -d /dev/video0 -c zoom_absolute=$default_zoom  > /dev/null 2>&1;;
		*)
			v4l2-ctl -d /dev/video0 -c zoom_absolute=$1  > /dev/null 2>&1;;
	esac
else
	case $1 in
		"+")
			v4l2-ctl -d /dev/video1 -c zoom_absolute=$((current_zoom + 1))  > /dev/null 2>&1;;
		"-")
			v4l2-ctl -d /dev/video1 -c zoom_absolute=$((current_zoom - 1))  > /dev/null 2>&1;;
		"=")
			v4l2-ctl -d /dev/video1 -c zoom_absolute=$default_zoom  > /dev/null 2>&1;;
		*)
			v4l2-ctl -d /dev/video1 -c zoom_absolute=$1 > /dev/null 2>&1;;
	esac
fi

