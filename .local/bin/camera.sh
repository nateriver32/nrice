#!/usr/bin/env bash
set -e

if v4l2-ctl -d /dev/video0 -l; then
	ffplay -vf "hflip,vflip" /dev/video0
else
	ffplay -vf "hflip,vflip" /dev/video1
fi
