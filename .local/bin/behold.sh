#!/usr/bin/env bash
set -e

st -e hold_terminal.sh neofetch &
sleep 0.25
st -e hold_terminal.sh cmatrix &
sleep 0.25
st -e hold_terminal.sh lf &
sleep 0.25
st -e hold_terminal.sh cava &
sleep 0.25
st -e hold_terminal.sh pipes.sh &
sleep 0.25
st -f "CMU Sans Serif:style=bold:pixelsize=25" -e  hold_terminal.sh figlet hi
