#!/usr/bin/env bash
set -e

city=$(dmenu -p "CITY ")

if [ "$city" = "" ]; then
  exit 1
fi

st -f "Iosevka Nerd Font:pixelsize=18" -c weather -e hold_terminal.sh curl -s https://wttr.in/${city}
