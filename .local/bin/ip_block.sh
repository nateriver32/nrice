#!/usr/bin/env bash
set -e

# Flush existing rules to start with a clean slate
sudo iptables -F

# Allow RDP traffic to/from the Windows machine (assuming its IP is 10.42.0.38)
sudo iptables -A INPUT -p tcp -s 10.42.0.38 --dport 3389 -j ACCEPT
sudo iptables -A OUTPUT -p tcp -d 10.42.0.38 --sport 3389 -j ACCEPT

# Block internet sharing traffic (forwarding) to/from the Windows machine
sudo iptables -A FORWARD -s 10.42.0.38 -j DROP
sudo iptables -A FORWARD -d 10.42.0.38 -j DROP

