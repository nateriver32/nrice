#!/usr/bin/env bash
set -e

# file=$(find ~ -type f \
#   -not -path "*/.git*" \
#   -not -path "*/.gnupg*" \
#   -not -path "*/.rustup*" \
#   -not -path "*/.local/state*" \
#   -not -path "*/.cache*" \
#   -not -path "*/.librewolf*" \
#   -not -path "*/.cargo*" \
#   -print | fzf --prompt="FILE  " \
#   --preview-window=top:50% \
#   --preview 'bat --theme=base16 --style=numbers --color=always --line-range :500 {}' \
#   --bind J:preview-down,K:preview-up,U:preview-half-page-up,D:preview-half-page-down \
#   --pointer=" " \
#   --marker=" " \
#   --margin=0,0,0,0 \
#   --no-separator \
#   --no-scrollbar \
#   --ellipsis="..." \
#   --info=inline-right \
#   --multi \
#   --color=fg:$WHITE,hl:$RED \
#   --color=fg+:$RED,bg+:$BG0,hl+:$RED \
#   --color=info:$WHITE,prompt:$WHITE,pointer:$RED \
#   --color=marker:$RED,spinner:$RED,header:$WHITE)

# file=$(find ~ -type f \
#   -not -path "*/.git*" \
#   -not -path "*/.gnupg*" \
#   -not -path "*/.rustup*" \
#   -not -path "*/.local/state*" \
#   -not -path "*/.cache*" \
#   -not -path "*/.librewolf*" \
#   -not -path "*/.cargo*" \
#   -print | fzf --prompt="FILE  " \
#   --bind change:top \
#   --pointer=" " \
#   --marker=" " \
#   --margin=0,0,0,0 \
#   --no-separator \
#   --no-scrollbar \
#   --ellipsis="..." \
#   --info=inline-right \
#   --multi \
#   --color=fg:$WHITE,hl:$RED \
#   --color=fg+:$RED,bg+:$BG0,hl+:$RED \
#   --color=info:$WHITE,prompt:$WHITE,pointer:$RED \
#   --color=marker:$RED,spinner:$RED,header:$WHITE)
#
# [ -z "$file" ] && exit 1
#
# nohup echo $file | tr -d '\n' | sed -E 's/ /\\ /g' | sed -E 's/\(/\\(/g' | sed -E 's/\)/\\)/g' | xclip -selection clipboard

st -c file -e sh -c -i '
file=$(find ~ -type f \
  -not -path "*/.git*" \
  -not -path "*/.gnupg*" \
  -not -path "*/.rustup*" \
  -not -path "*/.local/state*" \
  -not -path "*/.cache*" \
  -not -path "*/.librewolf*" \
  -not -path "*/.cargo*" \
  -print | fzf --prompt="COPY FILE PATH  "\
  --bind change:top \
  --pointer=" " \
  --marker=" " \
  --margin=0,0,0,0 \
  --no-separator \
  --no-scrollbar \
  --ellipsis="..." \
  --info=inline-right \
  --multi \
  --color=fg:$WHITE,hl:$RED \
  --color=fg+:$RED,bg+:$BG0,hl+:$RED \
  --color=info:$WHITE,prompt:$WHITE,pointer:$RED \
  --color=marker:$RED,spinner:$RED,header:$WHITE)

[ -z $file ] && exit 1

echo $file | tr -d "\n" | sed -E "s/ /\\ /g" | sed -E "s/\(/\\(/g" | sed -E "s/\)/\\)/g" | xclip -selection clipboard
'
