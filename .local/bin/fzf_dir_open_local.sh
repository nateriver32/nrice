#!/usr/bin/env bash
set -e

# path=$(find . -type d \
#    -not -path "*/.git*" \
#    -not -path "*/.gnupg*" \
#    -not -path "*/.rustup*" \
#    -not -path "*/.local/state*" \
#    -not -path "*/.cache*" \
#    -not -path "*/.librewolf*" \
#    -not -path "*/.cargo*" \
#    -print | fzf --prompt="DIRECTORY  " \
#    --bind J:preview-down,K:preview-up,U:preview-half-page-up,D:preview-half-page-down \
#    --preview-window=top:50% \
#    --preview="tree -C {}")

path=$(find . -type d \
   -not -path "*/.git*" \
   -not -path "*/.gnupg*" \
   -not -path "*/.rustup*" \
   -not -path "*/.local/state*" \
   -not -path "*/.cache*" \
   -not -path "*/.librewolf*" \
   -not -path "*/.cargo*" \
   -print | fzf --prompt="DIRECTORY  " \
   --bind change:top)

cd "$path"
exec $SHELL
