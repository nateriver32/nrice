#!/usr/bin/env bash
set -e

# Requires the vimium extension to be installed in the browser
playlist=$HOME/Music/playlists/all.m3u

sleep 1
xdotool key "F6"
# xdotool type "yy"
xdotool key "Ctrl+c"
xdotool key Escape
sleep 1

notification.sh Downloading...
echo $(xsel -o) | xargs yt-dlp -f bestaudio -o "$HOME/Music/%(title)s.%(ext)s"

cd $HOME/Music

for i in *webm; do
  track=${i%%.webm}.mp3
  ffmpeg -i "$i" "${i%%.webm}.mp3"
  rm "$i"
done

mp3gain -r -k $HOME/Music/*.mp3
# sort alphabetically
# find . -type f -name "*.mp3" | sed 's/.\///' | sort > $playlist
# sort by time added
# ls -t *.mp3 | sed 's/.\///' > $playlist
tac $playlist > tmp.txt
echo "$track" >> tmp.txt
tac tmp.txt > $playlist
rm tmp.txt

mpc clear
mpc update
mpc load all
notification.sh Done
