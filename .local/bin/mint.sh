#!/usr/bin/env bash
set -e

# start the VM
virsh --connect qemu:///system start "mint"
# connect to the VM
virt-manager --connect qemu:///system --show-domain-console "mint"
