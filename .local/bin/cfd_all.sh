#!/usr/bin/env bash
set -e

# st -e ssh $USER@10.42.0.31 &
# sleep 0.1
# st -e ssh $USER@10.42.0.77 &
# sleep 0.1
# st -e ssh $USER@10.42.0.159 &
# sleep 0.1
# st -e ssh $USER@10.42.0.200 &
# sleep 0.1
# st -e ssh $USER@10.42.0.187 &

for i in {1,2,3,4,5,6,7}; do
	st -e ssh node$i -t 'btop; bash -l' &
    sleep 0.1
done
