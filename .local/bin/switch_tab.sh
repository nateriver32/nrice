#!/usr/bin/env bash
set -e

# Use xargs to trim whitespace from the selected tag
# tag=$(echo -e "TERM\nWWW\nEDU\nVM\nSIM\nINK\nRND" | dmenu -p "TAG " | xargs)
tag=$(echo -e "1\n2\n3\n4\n5\n6" | dmenu -p "TAG " | xargs)

# Pass the selected tag to awesome-client
awesome-client "
local awful = require('awful')
awful.tag.viewonly(awful.tag.find_by_name(nil, '$tag'))
"
