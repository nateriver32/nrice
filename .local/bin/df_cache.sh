#!/usr/bin/env bash
set -e

if [ ! -e "~/.directory_cache" ]; then
	touch ~/.directory_cache
fi

if [ ! -e "~/.file_cache" ]; then
	touch ~/.directory_cache
fi

d_cache=$HOME/.directory_cache
f_cache=$HOME/.file_cache

directories=$(find "$HOME" -type d -not -path '*/.git*')
files=$(find "$HOME" -type f -not -path '*/.git*')

echo "$directories" > "$d_cache"
sed -i '/.rustup/d' $d_cache
sed -i '/.cache/d' $d_cache
sed -i '/.local\/share/d' $d_cache
sed -i '/.local\/state/d' $d_cache
sed -i '/.cargo/d' $d_cache
sed -i '/.librewolf/d' $d_cache
sed -i '/.mozilla/d' $d_cache
sed -i '/x11-config-new/d' $d_cache

echo "$files" > "$f_cache"
sed -i '/x11-config-new/d' $f_cache
sed -i '/.rustup/d' $f_cache
sed -i '/.cache/d' $f_cache
sed -i '/.local\/share/d' $f_cache
sed -i '/.local\/state/d' $f_cache
sed -i '/.cargo/d' $f_cache
sed -i '/.librewolf/d' $f_cache
sed -i '/.mozilla/d' $f_cache
sed -i '/elpaca/d' $f_cache
