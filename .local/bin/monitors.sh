#!/usr/bin/env bash
set -e

IFS=''

primary_monitor=$(xrandr | grep "primary" | awk '{print $1}')
monitor_list=$(xrandr | grep " connected" | grep -v "primary" | awk '{print $1}')
# primary_monitor="DVI-D-0"
# monitor_list="HDMI-0"

[ -z ${monitor_list} ] && exit 1

selection=$(printf "$monitor_list\nDUPLICATE\nONLY\nOFF" | dmenu -p "MONITOR " )

[ -z ${selection} ] && exit 1

if [ "$selection" = "DUPLICATE" ]; then
  duplicate=$(echo "$monitor_list" | dmenu -p "MONITOR " )
elif [ "$selection" = "ONLY" ]; then
  only=$(echo "$monitor_list" | dmenu -p "MONITOR " )
elif [ "$selection" = "OFF" ]; then
  off=$(echo "$monitor_list" | dmenu -p "MONITOR " )
fi

case $selection in
  HDMI[0-9]|HDMI-[0-9])
    xrandr --output $primary_monitor --above $selection --output $selection --auto
	$HOME/.local/bin/wallpaper.sh main
	;;
  VGA[0-9]|VGA-[0-9]-[0-9])
    xrandr --output $primary_monitor --above $selection --output $selection --auto
	$HOME/.local/bin/wallpaper.sh main
	;;
  DVI[0-9]|DVI-[0-9])
    xrandr --output $primary_monitor --above $selection --output $selection --auto
	$HOME/.local/bin/wallpaper.sh main
	;;
  DP[0-9]-[0-9]|DP-[0-9]-[0-9])
    xrandr --output $primary_monitor --above $selection --output $selection --auto
	$HOME/.local/bin/wallpaper.sh main
	;;
  DUPLICATE)
    xrandr --output $primary_monitor --auto --same-as $duplicate --output $duplicate --auto
	;;
  ONLY)
    xrandr --output $only --primary --output $primary_monitor --off
	;;
  OFF)
    xrandr --output $primary_monitor --auto --output $off --off
	;;
  *)
    exit 1
	;;
esac
