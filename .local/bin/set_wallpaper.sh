#!/usr/bin/env bash
set -e

xl=$(echo "($x-500)/2" | bc)
yl=$(echo "($y-500)/2" | bc)
location=$(echo 500x500+$xl+$yl)
DIR=$(echo -e "DEFAULT (~/.local/share/wallpapers)\nCUSTOM" | dmenu -p "WALLPAPER DIRECTORY " )

[ "$DIR" = "" ] && exit 1

if [ "$DIR" = "DEFAULT (~/.local/share/wallpapers)" ]; then
  DIR="$HOME/.local/share/wallpapers/"
else
  DIR="$(command lf -print-last-dir "$@")"
fi

wall=$(nsxiv -t -o -r -b -g $location $DIR | xargs)

# Set the next wallpaper for each monitor
monitors=($(xrandr --listmonitors | awk '{if (NR>1) print $4}'))

for ((i=0; i<${#monitors[@]}; i++)); do
  monitor="${monitors[i]}"
  nitrogen --head=$i --set-zoom-fill "$wall" 2> /dev/null
done
