#!/usr/bin/env bash
set -e

# awesome-client "
# naughty = require('naughty')
# naughty.notify({
#     text =
#     title =
#     timeout =
#     hover_timeout =
#     screen =
#     position =
#     ontop =
#     height =
#     width =
#     max_height =
#     max_width =
#     font =
#     icon =
#     icon_size =
#     fg =
#     bg =
#     border_width =
#     border_color =
#     shape =
#     opacity =
#     margin =
#     run =
#     destroy =
#     preset =
#     replaces_id =
#     callback =
#     actions =
#     ignore_suspend =
# })
# "

awesome-client "
naughty = require('naughty')
naughty.notify({
    text = '$1',
})
"
