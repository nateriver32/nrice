#!/usr/bin/env bash
set -e

account=$(find .password-store  -mindepth 1 -maxdepth 2 -not -path '*/.*' -type f | awk -F/ '{print $2"/"$3}' | awk -F. '{print $1}')
password=$(echo "$account" | dmenu -p "ACCOUNT ")
[ -z $password ] && exit 1
pass -c $password
