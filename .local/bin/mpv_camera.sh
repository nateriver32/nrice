#!/usr/bin/env bash
set -e

# mpv --x11-name=mpv_camera --no-osc --no-osd-bar --profile=low-latency --untimed --no-cache --vf=hflip,vflip --panscan=1 /dev/video0
mpv --x11-name=mpv_camera --no-osc --no-osd-bar --profile=low-latency --untimed --no-cache --panscan=1 /dev/video0
