#!/usr/bin/env bash
set -e

command=$(ls /usr/bin | dmenu -p 'SUDO RUN ')
[ -z "$command" ] && exit 1
password=$(dmenu -P -p "PASSWORD " -nf "$BG0" -nb "$RED")
[ -z "$password" ] && exit 1
echo "$password" | sudo -S "$command"
