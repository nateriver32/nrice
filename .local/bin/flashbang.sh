#!/usr/bin/env bash
set -e

white=$HOME/.local/share/white.png
black=$HOME/.local/share/black.png

if [ "$1" != "" ]; then
  nitrogen --set-zoom-fill "$black" 2> /dev/null
else
  nitrogen --set-zoom-fill "$white" 2> /dev/null
fi
