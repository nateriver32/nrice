#!/usr/bin/env bash
set -e

x=$(($(xdpyinfo | grep dimensions | awk '{print $2}' | awk -F"x" '{print $1}')/2))
y=$(($(xdpyinfo | grep dimensions | awk '{print $2}' | awk -F"x" '{print $2}')/2))

xdotool mousemove $x $y
