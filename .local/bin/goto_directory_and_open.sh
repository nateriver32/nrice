#!/usr/bin/env bash
set -e

d_cache=$HOME/.directory_cache

# Display the cached directories using dmenu
path=$(cat $d_cache | dmenu -p 'DIRECTORY ')

# Check if the selected path is empty
if [ -z "$path" ]; then
    exit 1
fi

# Execute the shell in the selected directory using st
st -c small -e sh -c "cd '$path'; nvim .; exec $SHELL"
