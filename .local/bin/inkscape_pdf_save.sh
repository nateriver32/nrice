#!/usr/bin/env bash
set -e

inotifywait -m -e close_write -r $HOME/Documents/faks/**/*.svg |
while read -r filename event; do
    inkscape --export-page --export-pdf="${filename%.*}.pdf" "$filename" 
done
