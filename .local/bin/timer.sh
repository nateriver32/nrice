#!/usr/bin/env bash
set -e

# i3-msg floating enable, resize set 100px 1000px, move position 1910 30
# termdown -b $1

time=$(dmenu -p "TIMER ")

if [ "$time" = "" ]; then
  exit 1
fi

st -c anki-timer -f "Iosevka Nerd Font:style=Bold:pixelsize=180" -e termdown --no-seconds -b --no-figlet $time
