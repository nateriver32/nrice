#!/usr/bin/env bash
set -e

# path=$(find ~ -type d \
#   -not -path "*/.git*" \
#   -not -path "*/.gnupg*" \
#   -not -path "*/.rustup*" \
#   -not -path "*/.local/state*" \
#   -not -path "*/.cache*" \
#   -not -path "*/.librewolf*" \
#   -not -path "*/.cargo*" \
#   -print | fzf --prompt="DIRECTORY  "\
#   --bind change:top \
#   --preview-window=top:50% \
#   --preview="tree -C {}" \
#   --pointer=" " \
#   --marker=" " \
#   --margin=0,0,0,0 \
#   --no-separator \
#   --no-scrollbar \
#   --ellipsis="..." \
#   --info=inline-right \
#   --multi \
#   --color=fg:$WHITE,hl:$RED \
#   --color=fg+:$RED,bg+:$BG0,hl+:$RED \
#   --color=info:$WHITE,prompt:$WHITE,pointer:$RED \
#   --color=marker:$RED,spinner:$RED,header:$WHITE)

st -c file -e sh -c -i '
path=$(find ~ -type d \
  -not -path "*/.git*" \
  -not -path "*/.gnupg*" \
  -not -path "*/.rustup*" \
  -not -path "*/.local/state*" \
  -not -path "*/.cache*" \
  -not -path "*/.librewolf*" \
  -not -path "*/.cargo*" \
  -print | fzf --prompt="DIRECTORY  "\
  --bind change:top \
  --pointer=" " \
  --marker=" " \
  --margin=0,0,0,0 \
  --no-separator \
  --no-scrollbar \
  --ellipsis="..." \
  --info=inline-right \
  --multi \
  --color=fg:$WHITE,hl:$RED \
  --color=fg+:$RED,bg+:$BG0,hl+:$RED \
  --color=info:$WHITE,prompt:$WHITE,pointer:$RED \
  --color=marker:$RED,spinner:$RED,header:$WHITE)

if [ -n "$path" ]; then
  cd "$path"
  nohup pcmanfm >/dev/null 2>&1 & disown
fi
'
