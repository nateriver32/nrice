#!/usr/bin/env bash
set -e

windows=$(wmctrl -l | awk '{$3=""; $2=""; $1=""; print $0}' | sed 's/   //g')

selection=$(echo "$windows" | dmenu -l 10 -p "WINDOW ")

if [ -n "$selection" ]; then
    win_id=$(wmctrl -l | grep -F "$selection" | awk '{print $1}')
    wmctrl -i -a "$win_id"
fi

# # Get a list of all windows using xdotool
# windows=$(xdotool search --onlyvisible --name "." getwindowname %@)
#
# # Use dmenu to select a window
# selection=$(echo "$windows" | dmenu -l 10 -p "WINDOW ")
#
# if [ -n "$selection" ]; then
#     # Find the window ID of the selected window
#     win_id=$(xdotool search --all --name "$selection")
#     # Focus the selected window
#     # [ -n "$win_id" ] && xdotool windowactivate "$win_id"
#     [ -n "$win_id" ] && xdotool selectwindow $win_id set_window --urgency 1
# fi
