#!/usr/bin/env bash
set -e

# setxkbmap -option caps:escape
xset r rate 150 60
xmodmap $HOME/.Xmodmap
# xcape -e 'Shift_R=Super_L'
# xcape -e 'Mode_switch=Escape'
