#!/usr/bin/env bash
set -e

sleep 0.1
xdotool type "gg"
xdotool key space
xdotool key c
xdotool key colon
xdotool type "normal"
xdotool key ctrl+r
xdotool key plus
xdotool type "@o"
xdotool key Enter
