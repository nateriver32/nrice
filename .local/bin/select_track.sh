#!/usr/bin/env bash
set -e

# playlist=$(ls $HOME/Music/playlists | sed 's/\..*//g' | dmenu -p "PLAYLIST ")
# if [ "$playlist" = "" ]; then
#   exit 1
# fi
# mpc clear
# mpc load $playlist
# track=$(cat $HOME/Music/playlists/${playlist}.m3u | dmenu -p "PLAY ")
track=$(cat $HOME/Music/playlists/all.m3u | dmenu -p "PLAY ")
if [ "$track" = "" ]; then
  exit 1
fi
mpc searchplay filename "$track"
