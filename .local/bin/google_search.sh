#!/usr/bin/env bash
set -e

query=$(echo | dmenu -p "GOOGLE SEARCH " -l 0)

if [ -z "$query" ]; then
  exit 1
fi

brave "$query"
