-- vim tips --
-to search multiple times:
	-search the first time using `vimgrep /<search term>/ %` to populate the quickfix list.
	-open the quickfix list and search the second time.

-to populate the quickfix list with results from global command search:
	-:g/pattern/caddexpr getline(".")
	- we can now search inside the quickfix list and append the results of the new search inside the same list thus narrowing
          the search.

-to use vimgrep across all files use vimgrep /<search term>/ **/*

-to print use shell command to buffer using the buffer:
	- e.g. find ~ -type f
	- type !! then sh

-to record phone with no lag use scrcpy -m1024

-to fix the remmina connection to kubuntu:
	- run "update-alternatives --config x-session-manager" and select "/usr/bin/startplasma-x11"

-to replace every nth instance in vim:
    %s/\(.\{-}\zsPATTERN\)\{N}/REPLACE/

-to run matlab from terminal:
    matlab -nodisplay
    >>> run("file.m")

-to generate a password
    pwgen -yan <length>

-to use python virtual environment
    -source /path/to/venv/bin/activate -> to enter the virtual environment
    -now inside the virtual environment run the script that has pip packages

-to make pcmanfm see android
    -sudo pacman -S gvfs-mtp libmtp android-udev
    -sudo udevadm control --reload-rules
    -sudo udevadm trigger

-in flameshot to change the resize amount goto the widgets/capture/selectionwidget.cpp file and find the resize functions.
